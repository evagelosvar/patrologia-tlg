In illud: Qui dixerit verbum in filium
26.648 Περὶ δὲ οὗ γράφων ἐδήλωσας εὐαγγελικοῦ ῥητοῦ, συγγίνωσκε, ἀγαπητὲ,
συνείδησιν ἔχων ἀγα θήν. ∆έδια γὰρ κἂν ὅλως προσελθεῖν αὐτῷ· μὴ καὶ, δύνας εἰς
αὐτὸ τῷ λογισμῷ, καὶ ἀρξάμενος ἐρευνᾷν, ἀτονήσω τὴν ἐν αὐτῷ βαθεῖαν διάνοιαν
ἑλ κῦσαι. Σιωπῆσαι γοῦν περὶ τούτου τέλεον ἐβουλόμην, καὶ μόνον ἀρκεσθῆναι
τούτοις τοῖς ἔμπροσθεν γράμμασι. Στοχαζόμενος δὲ μὴ ἄρα καὶ δεύτερον περὶ τούτου
γράψαι φιλονεικήσῃς, ἐβιασάμην ἐμαυ τὸν, ἣν ἔχω μαθὼν, περὶ αὐτοῦ μετρίαν
διάνοιαν γράψαι· θαῤῥῶν, ὡς, τυχόντας μὲν ἡμᾶς τοῦ σκοποῦ, ἀποδέξῃ διὰ τὸν
διδάξαντα· μὴ τυχόντας δὲ, οὐ 26.649 μέμψῃ, γινώσκων τὴν ἡμετέραν προθυμίαν τε
καὶ ἀσθένειαν. Τὸ μὲν οὖν ῥητόν ἐστιν, ὅτε, Τοσούτων σημείων γινομένων ἐν τῷ
Εὐαγγελίῳ, οἱ μὲν Φα ρισαῖοι ἔλεγον· «Οὗτος οὐκ ἐκβάλλει τὰ δαιμόνια εἰ μὴ ἐν
Βεελζεβοὺλ τῷ ἄρχοντι τῶν δαιμονίων· ὁ δὲ Κύριος, εἰδὼς τὰς ἐνθυμήσεις αὐτῶν,
εἶπεν αὐτοῖς· Πᾶσα βασιλεία μερισθεῖσα καθ' ἑαυτὴν ἐρημοῦται.» Καὶ εἰρηκώς· «Εἰ δὲ
ἐν Πνεύματι Θεοῦ ἐγὼ ἐκβάλλω τὰ δαιμόνια, ἄρα ἔφθασεν ἐφ' ὑμᾶς ἡ βασιλεία τοῦ
Θεοῦ·» λοιπὸν ἐπιφέρει· «∆ιὰ τοῦτο λέγω ὑμῖν, Πᾶσα ἁμαρτία καὶ βλασφημία
ἀφεθήσεται ὑμῖν τοῖς ἀνθρώποις· ἡ δὲ εἰς τὸ Πνεῦμα βλασφημία οὐκ ἀφεθήσεται· καὶ
ὃς ἂν εἴπῃ λόγον κατὰ τοῦ Υἱοῦ τοῦ ἀνθρώπου, ἀφεθήσεται αὐτῷ· ὃς δ' ἂν εἴπῃ κατὰ
τοῦ Πνεύματος τοῦ ἁγίου, οὐκ ἀφεθήσεται αὐτῷ οὔτε ἐν τῷ αἰῶνι τούτῳ οὔτε ἐν τῷ
μέλλοντι.» Σὺ δὲ ἐζήτεις, τί δήποτε ἡ μὲν εἰς τὸν Υἱὸν βλασφη μία ἀφίεται, ἡ δὲ εἰς
τὸ Πνεῦμα τὸ ἅγιον οὐκ ἔχει ἄφεσιν οὔτε ἐν τῷ νῦν αἰῶνι, οὔτε ἐν τῷ μέλλον τι;
Παλαιοὶ μὲν οὖν ἄνδρες, Ὠριγένης ὁ πολυμαθὴς καὶ φιλόπονος, καὶ Θεόγνωστος ὁ
θαυμάσιος καὶ σπουδαῖος (τούτων γὰρ τοῖς περὶ τούτων συνταγματίοις ἐνέτυχον, ὅτε
τὴν ἐπιστολὴν ἔγραψας), ἀμφότεροι γὰρ περὶ τούτου γράφουσι, ταύτην εἶναι τὴν εἰς
τὸ ἅγιον Πνεῦμα βλασφημίαν λέγοντες, ὅταν οἱ καταξιωθέντες ἐν τῷ βαπτίσματι
τῆς δω ρεᾶς τοῦ ἁγίου Πνεύματος παλινδρομήσωσιν εἰς τὸ ἁμαρτάνειν. ∆ιὰ τοῦτο
γὰρ μηδὲ ἄφεσιν αὐτοὺς λήψεσθαί φασι· καθὰ καὶ ὁ Παῦλος ἐν τῇ πρὸς Ἑβραίους
λέγει· «Ἀδύνατον γὰρ τοὺς ἅπαξ φωτισθέντας, γευσαμένους τε τῆς δωρεᾶς τῆς
ἐπουρα νίου, καὶ μετόχους γενηθέντας Πνεύματος ἁγίου, καὶ καλὸν γευσαμένους
Θεοῦ ῥῆμα, δυνάμεις τε μέλ λοντος αἰῶνος, καὶ παραπεσόντας, πάλιν ἀνακαινί ζειν
εἰς μετάνοιαν.» Ταῦτα δὲ κοινῇ μὲν λέγουσι, καὶ ἰδίαν δὲ ἕκαστος προστίθησι
διάνοιαν. Ὁ μὲν γὰρ Ὠριγένης καὶ τὴν αἰτίαν τῆς κατὰ τῶν τοιούτων κρίσεως οὕτω
λέγει· «Ὁ μὲν Θεὸς καὶ Πατὴρ εἰς πάντα διήκει καὶ πάντα συνέχει, ἄψυχά τε καὶ
ἔμψυχα, λογικά τε καὶ ἄλογα· τοῦ δὲ Υἱοῦ ἡ δύναμις εἰς τὰ λογικὰ μόνα διατείνει, ἐν
οἷς εἰσι κατηχούμενοι καὶ Ἕλληνες, οἱ μηδέπω πιστεύσαντες· τὸ δὲ Πνεῦμα τὸ ἅγιον
εἰς μόνους ἐστὶ τοὺς 26.652 μεταλαβόντας αὐτοῦ ἐν τῇ τοῦ βαπτίσματος δό σει. Ὅταν
τοίνυν κατηχούμενοι καὶ Ἕλληνες ἁμαρτάνωσιν, εἰς μὲν τὸν Υἱὸν ἁμαρτάνουσιν,
ἐπεὶ ἐν αὐ τοῖς ἐστιν, ὥσπερ εἴρηται· δύνανται δὲ ὅμως λαμβά νειν ἄφεσιν, ὅταν
καταξιωθῶσι δωρεᾶς τῆς παλιγ γενεσίας. Ὅταν δὲ οἱ βαπτισθέντες ἁμαρτά νωσι, τὴν
τοιαύτην παρανομίαν εἰς τὸ Πνεῦμα τὸ ἅγιον φθάνειν φησίν· ἐπειδὴ ἐν αὐτῷ
γενόμενος ἥμαρτε· καὶ διὰ τοῦτο ἀσύγγνωστον εἶναι τὴν κατ' αὐτοῦ τιμωρίαν.» Ὁ δὲ
Θεόγνωστος καὶ αὐτὸς προστιθείς φησι ταῦτα· «Ὁ πρῶτον παραβεβηκὼς ὅρον καὶ
δεύτερον ἐλάττονος ἂν ἀξιοῖτο τιμωρίας· ὁ δὲ καὶ τὸν τρίτον ὑπεριδὼν οὐκέτι ἂν
συγγνώμης τυγχάνοι. Πρῶ τον δὲ ὅρον καὶ δεύτερόν φησι τὴν περὶ Πατρὸς καὶ Υἱοῦ
κατήχησιν· τὸν δὲ τρίτον τὸν ἐπὶ τῇ τελειώσει καὶ τῇ τοῦ Πνεύματος μετοχῇ
παραδιδόμε νον Λόγον· καὶ τοῦτο βεβαιῶσαι θέλων, ἐπάγει τὸ παρὰ τοῦ Σωτῆρος
 
 
 
 

1

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

εἰρημένον τοῖς μαθηταῖς· «Ἔτι «πολλὰ ἔχω ὑμῖν λέγειν, ἀλλ' οὔπω δύνασθε χωρεῖν.
«Ὅταν δὲ ἔλθῃ τὸ ἅγιον Πνεῦμα, διδάξει ὑμᾶς.» Εἶτά φησιν· «Ὥσπερ τοῖς μηδέπω
χωρεῖν δυναμένοις τὰ τέλεια διαλέγεται ὁ Σωτὴρ, συγκαταβαίνων αὐτῶν τῇ
σμικρότητι· τοῖς δὲ τελειουμένοις συγγίνεται τὸ Πνεῦμα τὸ ἅγιον, καὶ οὐ δήπου τις
ἐκ τούτων ἂν φαίη τὴν τοῦ Πνεύματος διδασκαλίαν ὑπερβάλλειν τῆς τοῦ Υἱοῦ
διδαχῆς· ἀλλ' ὅτι ὁ μὲν Υἱὸς συγκατα βαίνει τοῖς ἀτελέσι, τὸ δὲ Πνεῦμα σφραγίς ἐστι
τῶν τελειουμένων· οὕτως οὐ διὰ τὴν ὑπερβολὴν τοῦ Πνεύματος πρὸς τὸν Υἱὸν
ἄφυκτός ἐστι καὶ ἀσύγ γνωστος ἡ εἰς τὸ Πνεῦμα βλασφημία· ἀλλ' ὅτι ἐπὶ μὲν τοῖς
ἀτελέσιν ἐστὶ συγγνώμη· ἐπὶ δὲ τοῖς γευσα μένοις τῆς οὐρανίου δωρεᾶς καὶ
τελειωθεῖσιν οὐ δεμία περιλείπεται συγγνώμης ἀπολογία καὶ παρ αίτησις.» Ἐκεῖνοι
μὲν οὖν ταῦτα. Ἐγὼ δὲ ἀφ' ὧν ἔμαθον, νομίζω τὴν ἑκατέρου διάνοιαν μετρίας τινὸς
δοκιμασίας ἐπιδεῖσθαι καὶ κα τανοήσεως· μὴ ἄρα κεκρυμμένος ἐστί τις ἐν αὐ τοῖς
τοῖς ὑπ' αὐτῶν εἰρημένοις βαθύτερος νοῦς. ∆ῆλον γὰρ, ὡς, ὁ Υἱὸς ἐν τῷ Πατρὶ ὢν, ἐν
τούτοις ἐστὶν, ἐν οἷς καὶ ὁ Πατήρ ἐστι, καὶ τὸ Πνεῦμα οὐκ ἄπεστιν. Ἀδιαίρετος γάρ
ἐστιν ἡ ἁγία καὶ μακαρία, καὶ τε λεία Τριάς. Ἔτι δ' οὖν εἰ πάντα διὰ Υἱοῦ γέ γονε, καὶ
ἐν αὐτῷ τὰ πάντα συνέστηκε, πῶς ἂν ἐκτὸς εἴη τῶν δι' αὐτοῦ γενομένων; Μὴ ὄντων
δὲ ἐκείνων αὐτοῦ μακρὰν, ἐν πᾶσιν ἂν καὶ αὐτὸς εἰκό τως εἴη· ὡς ἐξ ἀνάγκης τὸν εἰς
τὸν Υἱὸν ἁμαρτάνοντα καὶ βλασφημοῦντα ἁμαρτάνειν καὶ εἰς τὸν Πατέρα 26.653 καὶ
εἰς τὸ ἅγιον Πνεῦμα. Καὶ τὸ ἅγιον δὲ λουτρὸν εἰ μὲν εἰς τὸ ἅγιον Πνεῦμα μόνον
ἐδίδοτο, εἰκότως ἂν ἐλέγετο τοὺς βαπτιζομένους εἰς τὸ Πνεῦμα μόνον ἁμαρτάνειν·
ἐπειδὴ δὲ εἰς Πατέρα, καὶ Υἱὸν, καὶ ἅγιον Πνεῦμα δίδοται, καὶ οὕτω τελειοῦται τῶν
βαπτιζομένων ἕκαστος, ἀνάγκη πάλιν τοὺς μετὰ τὸ βάπτισμα παραβαίνοντας εἰς τὴν
ἁγίαν καὶ ἀδιαίρετον Τριάδα τὴν βλασφημίαν ποιεῖσθαι. Καὶ τοῦτο δὲ δίκαιον ἂν εἴη
λογίζεσθαι καὶ νοεῖν. Εἰ μὲν πρὸς λαβόντας τὸ λουτρὸν τῆς παλιγγενεσίας, τοὺς
Φαρισαίους λέγω, καὶ μετασχόντας αὐτοὺς ἤδη τῆς δωρεᾶς τοῦ ἁγίου Πνεύματος,
διελέγετο ὁ Κύ ριος, πιθανή τις ἦν ἡ τοιαύτη διάνοια, ὡς παλινδρομήσαντας, καὶ
πλημμελήσαντας εἰς τὸ ἅγιον Πνεῦμα· εἰ δὲ μήτε τὸ λουτρὸν εἰλήφασιν, ἀλλὰ καὶ τὸ
τοῦ Ἰωάννου βάπτισμα ἐξουθενήσαντες ἦσαν, πῶς ᾐτιᾶτο τούτους ὡς
βλασφημήσαντας εἰς τὸ Πνεῦμα τὸ ἅγιον, οὗ μηδέπω μέτοχοι γεγόνεισαν; Καὶ γὰρ οὐ
διδάσκων ἁπλῶς ὁ Κύριος ἐλάλει ταῦτα, οὐδὲ ἐπὶ μέλλουσι τὴν τιμωρίαν ἠπείλει·
ἀλλ' εὐθὺς αἰτιώμε νος ἀληθῶς τοὺς Φαρισαίους, ὡς ἤδη γενομένους ὑπευθύνους
τῆς τηλικαύτης βλασφημίας, εἴρηκε τοῦτο τὸ ῥητὸν ὁ Κύριος. Τῶν δὲ Φαρισαίων
οὕτω κατηγο ρουμένων, καὶ πρὶν λαβεῖν αὐτοὺς τὸ βάπτισμα, οὐκ ἂν εἴη τοῦτο
ῥητὸν ἐπὶ τοὺς μετὰ τὸ λουτρὸν πα ραβαίνοντας· καὶ μάλιστα ὅτι οὐχ ἁπλῶς ἐπὶ
ἁμαρ τίαις, ἀλλ' ἐπὶ βλασφημίᾳ αὐτοὺς ᾐτιᾶτο. ∆ιαφέρει δὲ, ὅτι ὁ μὲν ἁμαρτάνων
παραβαίνει νόμον· ὁ δὲ βλασφημῶν εἰς αὐτὴν ἀσεβεῖ τὴν θεότητα. Ἐπὶ πολ λαῖς δ'
οὖν πρότερον πλημμελείαις αἰτιώμενος αὐ τοὺς ὁ Σωτὴρ, ὅτι τὴν ἐντολὴν τοῦ Θεοῦ
τὴν περὶ τῶν γονέων διὰ ἀργύριον παρέβαινον, καὶ τὰ τῶν προφητῶν
παρεκρούοντο, καὶ τὸν οἶκον τοῦ Θεοῦ ἐποίουν οἶκον ἐμπορίου, ὅμως παρῄνει καὶ
μετανοεῖν αὐτοῖς. Ἐπὶ δὲ τῷ λέγειν αὐτοὺς, Ἐν Βεελζεβοὺλ ἐκβάλλει τὰ δαιμόνια,
οὐκ ἔτι ταύτην ἁμαρτίαν ἁπλῶς, ἀλλὰ βλασφημίαν εἴρηκεν εἶναι τηλικαύτην, ὥστε
ἄφυκτον καὶ ἀσύγγνωστον εἶναι τὴν τιμωρίαν τοῖς τοιαῦτα τολμῶσιν. Ἄλλως τε, εἰ
τῶν μετὰ τὸ λουτρὸν ἁμαρτανόν των χάριν εἴρηται τοῦτο τὸ ῥητὸν, καὶ τούτοις ἀσύγ
γνωστός ἐστιν ἡ τῶν πλημμελημάτων δίκη· πῶς τῳ μὲν ἐν Κορίνθῳ μετανοοῦντι
κυροῖ τὴν αὐτὴν ἀγάπην ὁ Ἀπόστολος, τοὺς δὲ Γαλάτας παλινδρομήσαντας ὠδίνει,
ἄχρις οὗ πάλιν μορφωθῇ Χριστὸς ἐν αὐτοῖς; Ἐν δὲ τῷ λέγειν, πάλιν, δείκνυσιν
αὐτῶν καὶ τὴν προτέραν ἐν τῷ Πνεύματι τελειότητα. Τί δὲ καὶ Νουάτῳ μεμφόμεθα
 
 
 
 

2

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

ἀναιροῦντι μετάνοιαν, καὶ 26.656 φάσκοντι μηδεμίαν συγγνώμην ἔχειν τοὺς μετὰ τὸ
λουτρὸν ἁμαρτάνοντας, εἰ διὰ τοὺς μετὰ τὸ λουτρὸν ἁμαρτάνοντας εἴρηται τὸ
ῥητόν; Καὶ γὰρ τὸ ἐν τῇ πρὸς Ἑβραίους εἰρημένον οὐκ ἐκκλεῖόν ἐστι τῶν
ἁμαρτανόντων τὴν μετάνοιαν, ἀλλὰ δεικνύον ἓν εἶναι τὸ τῆς καθολικῆς Ἐκκλησίας
βάπτισμα καὶ μὴ δεύ τερον. Ἑβραίοις γὰρ ἔγραφε· καὶ ἵνα μὴ νομίσωσι, κατὰ τὴν ἐν
τῷ νόμῳ συνήθειαν, προφάσει μετανοίας εἶναι πολλὰ καὶ καθ' ἡμέραν βαπτίσματα,
διὰ τοῦτο μετανοεῖν μὲν παραινεῖ, μίαν δὲ εἶναι τὴν ἀνακαίνισιν διὰ τοῦ
βαπτίσματος, καὶ μὴ δευτέραν ἀποφαίνεται, ὡς καὶ ἐν ἑτέρᾳ Ἐπιστολῇ φησι· «Μία
πίστις, ἓν βάπτισμα.» Οὐδὲ γὰρ, εἶπεν, ἀδύνατον μετανοεῖν· ἀλλ' ἀδύνατον προφάσει
μετανοίας ἀνα καινίζειν ἡμᾶς. Ἔχει δὲ πολλὴν τὴν διαφοράν· ὁ μὲν γὰρ μετανοῶν
παύεται μὲν τοῦ ἁμαρτάνειν, ἔχει δὲ τῶν τραυμάτων τὰς οὐλάς· ὁ δὲ βαπτιζόμενος
τὸν μὲν παλαιὸν ἀπεκδιδύσκεται, ἀνακαινίζεται δὲ, ἄνωθεν γεννηθεὶς τῇ τοῦ
Πνεύματος χάριτι. Τοιαῦτα δὴ οὖν μοι λογιζομένῳ, μεῖζον ἀπαντᾷ τὸ βάθος τῆς ἐν
τῷ ῥητῷ διανοίας· καὶ διὰ τοῦτο πολλὰ πρότερον παρακαλέσας τὸν ἐπὶ τὸ φρέαρ καθ
εσθέντα, καὶ ἐπὶ τὴν θάλασσαν περιπατήσαντα Κύριον, ἐπὶ τὴν οἰκονομίαν λοιπὸν
τὴν ἐν αὐτῷ γενο μένην ὑπὲρ ἡμῶν ἀνέρχομαι, τάχα πως ἐξ αὐτῆς τὸν ἐν τῷ
ἀναγνώσματι νοῦν λαβεῖν δυνηθῶ. Πᾶσα τοίνυν ἡ θεία Γραφὴ ταύτην εὐαγγελίζεται
καὶ κη ρύττει, μάλιστα δὲ ὁ μὲν Ἰωάννης λέγων· «Ὁ Λόγος σὰρξ ἐγένετο, καὶ
ἐσκήνωσεν ἐν ἡμῖν·» ὁ δὲ Παῦλος γράφων· «Ὃς ἐν μορφῇ Θεοῦ ὑπάρχων, οὐχ
ἁρπαγμὸν ἡγήσατο τὸ εἶναι ἴσα Θεῷ· ἀλλ' ἑαυτὸν ἐκένωσε, μορφὴν δούλου λαβὼν,
καὶ σχήματι εὑρεθεὶς ὡς ἄνθρωπος. Ἐταπείνωσεν ἑαυτὸν, ὑπ ήκοος γενόμενος μέχρι
θανάτου, θανάτου δὲ σταυροῦ.» ∆ιὰ τοῦτο γὰρ Θεὸς ὢν καὶ ἄνθρωπος γενόμενος, ὡς
μὲν Θεὸς ἤγειρε νεκροὺς, καὶ λόγῳ πάντας θε ραπεύων, μετέβαλε καὶ τὸ ὕδωρ εἰς
οἶνον· οὐ γὰρ ἦν ἀνθρώπου ταῦτα ἔργα· ὡς δὲ σῶμα φορῶν, ἐδίψα καὶ ἐκοπία, καὶ
ἔπασχεν· οὐ γὰρ ἦν ἴδια ταῦτα τῆς θεότητος. Καὶ ὡς μὲν Θεὸς ἔλεγεν· «Ἐγὼ ἐν τῷ
Πατρὶ, καὶ ὁ Πατὴρ ἐν ἐμοί·» ὡς δὲ σῶμα φορῶν, 26.657 ἤλεγχε τοὺς Ἰουδαίους· «Τί
με ζητεῖτε ἀποκτεῖναι, ἄνθρωπον, ὃς τὴν ἀλήθειαν ὑμῖν λελάληκα, ἣν ἤκουσα παρὰ
τοῦ Πατρός;» Ἐγίνετο δὲ ταῦτα οὐ διῃρη μένως κατὰ τὴν τῶν γινομένων ποιότητα,
ὥστε τὰ μὲν τοῦ σώματος χωρὶς τῆς θεότητος, τὰ δὲ τῆς θεό τητος χωρὶς τοῦ
σώματος δείκνυσθαι· συνημμένως δὲ πάντα ἐγίνετο, καὶ εἷς ἦν ὁ ταῦτα ποιῶν Κύριος
παραδόξως τῇ ἑαυτοῦ χάριτι. Ἔπτυε γὰρ ἀνθρω πίνως, καὶ τὸ πτύσμα ἦν ἔνθεον· ἐν
αὐτῷ γὰρ ἐποίει τοὺς ὀφθαλμοὺς τοῦ ἐκ γενετῆς τυφλοῦ ἀναβλέπειν. Καὶ δεικνύναι
δὲ θέλων ἑαυτὸν Θεὸν, ἀνθρωπίνῃ γλώττῃ τοῦτο σημαίνων ἔλεγεν· «Ἐγὼ καὶ ὁ Πα
τὴρ ἕν ἐσμεν·» καὶ μόνον δὲ θέλων ἐθεράπευε. Τὴν δὲ ἀνθρωπίνην ἐκτείνων χεῖρα,
ἤγειρε τὴν πεν θερὰν Πέτρου πυρέσσουσαν, καὶ τὴν τοῦ ἀρχισυναγώγου θυγατέρα
ἤδη τελευτήσασαν ἐκ τῶν νεκρῶν ἀνήγειρεν. Αἱρετικοὶ μὲν οὖν κατὰ τὴν ἰδίαν
ἀπαιδευσίαν ἐμάνησαν· καὶ οἱ μὲν τὰ σωματικὰ βλέποντες τοῦ Σωτῆρος ἠρνήσαντο
τὸ, «Ἐν ἀρχῇ ἦν ὁ Λόγος·» οἱ δὲ τὰ τῆς θεότητος θεωροῦντες ἠγνόησαν τὸ, «Ὁ Λόγος
σὰρξ ἐγένετο.» Ὁ δὲ πιστὸς καὶ ἀποστολικὸς ἀνὴρ, εἰδὼς τὴν τοῦ Κυρίου
φιλανθρωπίαν, βλέπων μὲν τὰ τῆς θεότητος σημεῖα, θαυμάζει τὸν ἐν τῷ σώματι
Κύριον· θεωρῶν δὲ πάλιν καὶ τὰ τοῦ σώματος ἴδια, ὑπερεκπλήσσεται, κατανοῶν τὴν
ἐν αὐτοῖς τῆς θεότητος ἐνέργειαν. Τοιαύτης δὲ οὔσης τῆς ἐκκλησιαστικῆς πίστεως,
ὅταν τινὲς εἰς τὰ ἀν θρώπινα βλέποντες, ἴδωσι τὸν Κύριον διψῶντα, ἢ κοπιῶντα, ἢ
πάσχοντα, καὶ μόνον φλυαρήσωσιν ὡς κατὰ ἀνθρώπου τοῦ Σωτῆρος, ἁμαρτάνουσι
μὲν με γάλως, δύνανται δὲ ὅμως, ταχέως μεταγινώσκοντες, λαμβάνειν συγγνώμην,
ἔχοντες πρόφασιν τὴν τοῦ σώματος ἀσθένειαν (ἔχουσι γὰρ καὶ τὸν Ἀπό στολον
συγγνώμην αὐτοῖς νέμοντα, καὶ οἱονεὶ χεῖρα αὐτοῖς ἐν τῷ λέγειν ἐκτείνοντα, ὅτι
 
 
 
 

3

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

«Καὶ ὁμολο γουμένως μέγα ἐστὶ τὸ τῆς εὐσεβείας μυστήριον, Θεὸς ἐφανερώθη ἐν
σαρκί»). Ὅταν δὲ πάλιν τὰ τῆς θεότητος ἔργα θεωροῦντές τινες, διστάζωσι περὶ τῆς
τοῦ σώματος φύσεως, καὶ αὐτοὶ μὲν ἁμαρτάνουσι τὰ μέγιστα, ὅτι, καίτοι βλέποντες
ἐσθίοντα καὶ πάσχοντα, φαντάζονται φαντασίαν· συγγινώσκειν δὲ ὅμως καὶ τούτοις
ταχέως μεταγινώσκουσιν ὁ Χριστὸς δύναται, διὰ τὸ καὶ αὐτοὺς ἔχειν πρόφασιν τὴν
ἐκ τῶν ἔργων ὑπὲρ ἄνθρωπον μεγαλειότητα. Ἐπειδὰν 26.660 δὲ τὴν ἀμφοτέραν
τούτων ἄγνοιαν καὶ πήρωσιν ὑπερβάντες οἱ δοκοῦντες ἔχειν τοῦ νόμου τὴν γνῶσιν
(οἷοι ἦσαν οἱ τότε Φαρισαῖοι) εἰς μανίαν τραπῶσι, καὶ αὐτὸν τὸν ἐν τῷ σώματι ὄντα
Λόγον τέλεον ἀρνήσωνται, ἢ καὶ τὰ τῆς θεότητος ἔργα τῷ διαβόλῳ καὶ τοῖς τούτου
δαίμοσιν ἀναφέρωσιν· εἰκό τως ἀσύγγνωστον ἔχουσι τὴν ἐκ τῆς τοιαύτης ἀσεβείας
ἐπιτιμίαν. Τόν τε γὰρ διάβολον εἰς Θεὸν ἐλογίσαντο, καὶ τὸν ἀληθινὸν καὶ ὄντως
ὄντα Θεὸν, οὐδὲν πλέον ἐν τοῖς ἔργοις ἔχειν τῶν δαιμόνων ἡγήσαντο. Εἰς τοιαύτην
τοίνυν ἀσέβειαν πεπτωκότες ἦσαν οἱ τότε Ἰουδαῖοι, καὶ τῶν Ἰουδαίων οἱ Φαρισαῖοι.
Τοῦ γὰρ Σωτῆρος δεικνύοντος τὰ τοῦ Πατρὸς ἔργα, (νεκροὺς γὰρ ἤγειρε, τυφλοῖς
ἐχαρίζετο τὸ βλέπειν, χωλοὺς ἐποίει περιπατεῖν, κωφῶν ἤνοιγε τὴν ἀκοὴν, ἀλάλους
ἐποίει λαλεῖν, καὶ τὴν κτίσιν ὑποτεταγμένην ἐδείκνυεν, ἐπιτάσσων τοῖς ἀνέμοις, καὶ
περιπατῶν ἐπ' αὐτῆς τῆς θαλάττης), οἱ μὲν ὄχλοι ἐξεπλήσσοντο καὶ ἐδόξαζον τὸν
Θεόν· οἱ δὲ θαυμαστοὶ Φαρισαῖοι ἔλεγον τοῦ Βεελζεβοὺλ εἶναι τὰ τοιαῦτα ἔργα· καὶ
οὐκ ᾐσχύνοντο οἱ ἄφρονες τὴν τοῦ Θεοῦ δύναμιν ἐπὶ τὸν διάβολον μεταφέροντες.
Ὅθεν ἀκολούθως ὁ Σωτὴρ ἀσύγγνωστα καὶ ἄφυκτα βλασφημεῖν αὐτοὺς ἀπεφή νατο.
Ἕως μὲν γὰρ εἰς τὰ ἀνθρώπινα βλέποντες ἐχώ λευον τῇ διανοίᾳ, λέγοντες· «Οὐχ
οὗτός ἐστιν ὁ τοῦ τέκτονος υἱός;» καὶ, «Πῶς οὗτος οἶδε γράμματα, μὴ μεμαθηκώς;»
καὶ, «Τί σὺ ποιεῖς περὶ σεαυτοῦ σημεῖον;» καὶ, «Καταβήτω νῦν ἀπὸ τοῦ σταυροῦ, καὶ
πιστεύσομεν αὐτῷ·» ἔφερεν αὐ τοὺς, καὶ, ὡς εἰς Υἱὸν ἀνθρώπου ἁμαρτανόντων αὐ
τῶν, λυπούμενος ἐπὶ τῇ πωρώσει αὐτῶν ἔλεγεν· «Εἰ ἔγνωτε καὶ ὑμεῖς τὰ εἰς
εἰρήνην.» Καὶ γὰρ καὶ τῷ μεγάλῳ Πέτρῳ ὡς περὶ ἀνθρώπου λαλησάσης τῆς
θυρωροῦ, καὶ οὕτως ἀποκρινομένῳ, συνέγνω κλαύ σαντι ὁ Κύριος. Ὅτε δὲ πεσόντες
ἐπιπεπτώκασι, καὶ πλέον ἐμάνησαν, ὥστε τὰ τοῦ Θεοῦ ἔργα λέγειν εἶναι τοῦ
Βεελζεβοὺλ, οὐκέτι λοιπὸν αὐτοὺς ἤνεγκεν· εἰς γὰρ τὸ Πνεῦμα αὐτοῦ ἐβλασφήμουν,
λέγοντες τὸν ταῦτα ποιοῦντα μὴ εἶναι Θεὸν, ἀλλὰ Βεελζεβούλ. Καὶ διὰ τοῦτο, ὡς
ἀφόρητα τολμῶντας, αἰωνίως ἐτιμω ρήσατο. Ἴσον γὰρ ἦν αὐτοὺς τολμᾷν καὶ εἰπεῖν,
26.661 ὁρῶντας τὴν τοῦ κόσμου τάξιν, καὶ τὴν εἰς αὐτὸν πρόνοιαν, ὅτι καὶ ἡ κτίσις
ὑπὸ τοῦ Βεελζεβοὺλ γέ γονε· καὶ ὁ ἥλιος ὑπακούων τῷ διαβόλῳ ἀνατέλλει, καὶ δι'
αὐτὸν περιπολεῖ τὰ ἄστρα ἐν τῷ οὐρανῷ. Καὶ γὰρ ὥσπερ ταῦτα τοῦ Θεοῦ, οὕτω
κἀκεῖνα τοῦ Πατρὸς ἦν ἔργα· καὶ εἰ ἐκεῖνα τοῦ Βεελζεβοὺλ, ἐξ ἀνάγ κης καὶ ταῦτα
τοῦ Βεελζεβούλ. Καὶ ποῦ λοιπὸν αὐτοῖς τό· «Ἐν ἀρχῇ ἐποίησεν ὁ Θεὸς τὸν οὐρανὸν
καὶ τὴν γῆν;» Ἀλλ' οὐδὲν ξένον τῆς τοιαύτης αὐτῶν μανίας· ἐπεὶ καὶ οἱ τῆς τοιαύτης
αὐτὸν γνώμης πατέρες, ἐν τῇ ἐρήμῳ πρὸ ὀλίγου ἐξελθόντες ἀπὸ τῆς Αἰγύπτου,
μόσχον πλάσαντες, αὐτῷ τὰς εἰς αὐτοὺς εὐεργεσίας παρὰ Θεοῦ γενομένας
ἀνατιθέντες, ἔλεγον· «Οὗτοι οἱ θεοί σου, Ἰσραὴλ, οἱ ἀναγαγόντες σε ἐκ γῆς
Αἰγύπτου.» ∆ιὰ γοῦν ταύτην τὴν βλασφημίαν καὶ τότε τῶν τοιαῦτα τολμησάντων
τὴν μὲν ἀρχὴν οὐκ ὀλίγοι, πολλοὶ δὲ ἀνῃρέθησαν· ἐπηγγείλατο δὲ ὁ Θεὸς λέγων· «Ἧ
δ' ἂν ἡμέρᾳ ἐπισκέψωμαι, ἐπάξω ἐπ' αὐτοὺς τὴν ἁμαρτίαν αὐτῶν.» Καὶ γὰρ ἕως μὲν
κἀκεῖνοι δι' ἄρτους καὶ ὕδωρ ἐγόγγυζον, ἐπὶ τοσοῦτον αὐτοὺς οὕτως ἔφερεν, ὡς εἴ
τις τροφὸς τροφοφορήσειε τὸν ἑαυτῆς υἱόν· ὅτε δὲ πλέον ἐμάνη σαν, καὶ ὡς ψάλλει
κατ' αὐτῶν τὸ Πνεῦμα· «Ἠλλά ξαντο τὴν δόξαν αὐτῶν ἐν ὁμοιώματι μόσχου
ἐσθίοντος χόρτον·» τὸ τηνικαῦτα λοιπὸν ἀσύγγνωστα τολμήσαντες, ἐπατάχθησαν,
 
 
 
 

4

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

ὥς φησιν ἡ Γραφὴ, περὶ τῆς ποιήσεως τοῦ μόσχου, οὗ ἐποίησεν Ἀαρών. Τοιαῦτα καὶ
νῦν τολμῶντες οἱ Φαρισαῖοι, τοι αύτην ἐσχήκασι παρὰ τοῦ Σωτῆρος τὴν ἀπόφα σιν,
οἵαν καὶ ὁ παρ' αὐτῶν ὑπονοηθεὶς Βεελζεβοὺλ ἔσχεν ἤδη καὶ ἔχει· ὥστε αὐτοὺς εἰς
τὸ ἡτοιμασμέ νον πῦρ ἐκείνῳ αἰωνίως συγκατεσθίεσθαι μετ' αὐτοῦ. Οὐ γὰρ δὴ
σύγκρισίν τινα ποιούμενος τῆς εἰς ἑαυ τὸν καὶ τῆς εἰς τὸ Πνεῦμα τὸ ἅγιον λεγομένης
βλασφημίας, καὶ ὡς μείζονος ὄντος τοῦ Πνεύμα τος, καὶ διὰ τοῦτο μείζονα τὴν
αἰτίαν ἐχούσης τῆς εἰς τὸ Πνεῦμα γινομένης, ταῦτ' εἴρηκε· μὴ γένοιτο! Φθάσας γὰρ
ἐδίδαξεν, ὅτι πάντα ὅσα ἔχει ὁ Πατὴρ τοῦ Υἱοῦ ἐστι, καὶ ἐκ τοῦ Υἱοῦ τὸ Πνεῦμα
λήψεται, καὶ αὐτὸ δοξάσει τὸν Υἱόν. Καὶ οὐχὶ τὸ Πνεῦμα τὸν Υἱὸν δίδωσιν, ἀλλ' ὁ
Υἱὸς παρέχει τὸ Πνεῦμα τοῖς μα θηταῖς, καὶ δι' αὐτῶν τοῖς εἰς αὐτὸν πιστεύουσιν.
Οὐχ οὕτως οὖν εἴρηκεν ὁ Σωτήρ· ἀλλ' ὡς εἰς αὐτὸν ἐπ' ἀμφοτέροις γινομένης τῆς
βλασφημίας, καὶ τῆς 26.664 μὲν ἐλάττονος, τῆς δὲ ὑπερβαλλούσης ταῦτα λελάλη κεν
ὁ Κύριος. Καὶ γὰρ καὶ αὐτοὶ οἱ Φαρισαῖοι ἀμφότερα ἦσαν λέγοντες· καὶ ἄνθρωπον
βλέποντες αὐτὸν ἐλοιδόρουν· «Πόθεν τούτῳ ἡ σοφία αὕτη;» καὶ, «Πεντήκοντα ἔτη
οὔπω ἔχεις, καὶ Ἀβραὰμ ἑώρακας;» Καὶ τὰ τοῦ Πατρὸς ἔργα θεωροῦντες, οὐ μόνον
ἠρνοῦντο τὴν θεότητα αὐτοῦ, ἀλλὰ καὶ ἀντὶ ταύτης ἔλεγον ἐν αὐτῷ εἶναι τὸν
Βεελζεβοὺλ, καὶ αὐ τοῦ εἶναι τὰ ἔργα. ∆ιὰ τοῦτο, ὡς εἰς αὐτὸν ἀμφοτέρων
λεγομένων, καὶ τῆς μὲν ἐλάττονος βλασφη μίας οὔσης διὰ τὸ ἀνθρώπινον, τῆς δὲ
μείζονος διὰ τὴν θεότητα, ἐπὶ τῇ μείζονι κατ' αὐτῶν τὸ ἄφυ κτον τῆς τιμωρίας
ἔλεγεν. Ἀμέλει τοὺς μαθητὰς πα ραθαρσύνων καὶ λέγων, ὅτι «Εἰ τὸν οἰκοδεσπότην
Βεελζεβοὺλ ἐπεκάλεσαν,» ἑαυτὸν εἶναι τὸν οἰκο δεσπότην ἔλεγε, τὸν καὶ ὑπὸ τῶν
Ἰουδαίων οὕτω βλασφημούμενον. Εἰ δὲ οἱ Ἰουδαῖοι λέγοντες. «ἐν Βεελζε βοὺλ,»
οὐδένα ἕτερον ἢ τὸν Κύριον ἐλοιδόρουν, δῆ λον τοίνυν ἐστὶν, ὡς τὸ λεγόμενον, «Ἡ
δὲ τοῦ Πνεύ ματος βλασφημία,» εἰς αὐτόν ἐστι τὸν Κύριον λεγο μένη, καὶ τὸ ῥητὸν
ὅλον περὶ ἑαυτοῦ σημαίνων ἔλε γεν ὁ Σωτήρ. Αὐτὸς γάρ ἐστιν ὁ τοῦ παντὸς οἰκοδε
σπότης· πάλιν γὰρ ἀσφαλείας ἕνεκα τὸ αὐτὸ εἰπεῖν οὐκ ὀκνητέον. Τὸ διψᾷν, τὸ
κοπιᾷν, τὸ κοιμᾶ σθαι, τὸ ῥαπίζεσθαι, τὸ ἐσθίειν, ἀνθρώπων ἐστὶν ἴδια· τὰ δὲ ἔργα ἃ
ἐποίει ὁ Κύριος, οὐκ ἔτι ἀνθρώπων, ἀλλὰ Θεοῦ ἦν αὐτὰ ποιεῖν. Ὅταν οὖν τινες, καθὰ
πρότερον εἶπον, ταῦτα βλέποντες ὡς ἀνθρώπῳ λοιδορῶνται τῷ Κυρίῳ, ἐλάττονος
ἂν ἀξιοῖντο τιμω ρίας τῶν τὰ τοῦ Θεοῦ ἔργα μεταφερόντων εἰς τὸν διάβολον. Οἱ γὰρ
τοιοῦτοι, οὐ μόνον τὰ ἅγια τοῖς κυσὶ ῥίπτουσιν, ἀλλὰ καὶ τὸν Θεὸν τῷ διαβόλῳ συγ
κρίνουσι, καὶ τὸ φῶς λέγουσιν εἶναι σκότος. Ταύτην γοῦν εἶναι τῶν Φαρισαίων τὴν
ἀσύγγνωστον βλα σφημίαν ὁ Μάρκος παρετηρήσατο λέγων· «Ὃς δ' ἂν βλασφημήσῃ
εἰς τὸ Πνεῦμα τὸ ἅγιον, οὐκ ἔχει ἄφεσιν, ἀλλ' ἔνοχός ἐστιν αἰωνίου ἁμαρτίας· ὅτι
ἔλεγον, Πνεῦμα ἀκάθαρτον ἔχει.» Καὶ ὁ μὲν ἐκ γενετῆς τυφλὸς ἀναβλέψας
ἐμαρτύρει· «Ἐκ τοῦ αἰῶνος οὐκ ἠκούσθη, ὅτι ἤνοιξέ τις ὀφθαλμοὺς τυφλοῦ γεγεννη
μένου. Εἰ μὴ ἦν οὗτος παρὰ Θεοῦ, οὐκ ἠδύνατο ποιεῖν οὐδέν·» οἵ τε ὄχλοι,
θαυμάζοντες ἐπὶ τοῖς γινομέ νοις ὑπὸ τοῦ Κυρίου, ἔλεγον· «Ταῦτα τὰ ῥή 26.665 ματα
οὐκ ἔστι δαιμονιζομένου· μὴ δαιμόνιον δύναται τυφλῶν ὀφθαλμοὺς ἀνοίγειν;» οἱ δὲ
νομιζόμενοι ἁδροὶ τοῦ νόμου, οἱ Φαρισαῖοι, οἱ τὰ κράσπεδα πλατύνοντες, καὶ
κομπάζοντες ὡς πλέον τι τῶν ἄλλων γινώσκοντες, οὐδ' οὕτως ᾐσχύνοντο· κατὰ δὲ
τὸ γε γραμμένον, ἔθυον οἱ τάλανες δαιμονίῳ καὶ οὐ Θεῷ, λέγοντες δαιμόνιον ἔχειν
τὸν Κύριον, καὶ δαιμόνων εἶναι τὰ τοῦ Θεοῦ ἔργα. Ἔπασχον δὲ τοῦτο δι' οὐδὲν
ἕτερον, ἢ ἵνα μόνον ἀρνήσωνται τὸν ταῦτα ποιοῦντα Θεὸν εἶναι, καὶ τοῦ Θεοῦ Υἱόν.
Εἰ γὰρ τὸ ἐσθίειν καὶ τοῦ σώματος ἡ ὄψις ἐδείκνυεν αὐτὸν καὶ ἄνθρωπον, διὰ τί μὴ
ἐκ τῶν ἔργων ἐθεώρουν αὐτὸν εἶναι ἐν τῷ Πατρὶ, καὶ τὸν Πατέρα ἐν αὐτῷ; Ἀλλ' οὐκ
ἤθελον· μᾶλλον δὲ αὐτοὶ τὸν Βεελζεβοὺλ εἶχον ἐν ἑαυτοῖς φθεγγόμενον, ὡς ἐκ μὲν
 
 
 
 

5

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

τῶν ἀνθρωπίνων ἄνθρωπον αὐτὸν μόνον ὀνομάζειν· ἐκ δὲ τῶν ἔργων, ἃ ἦν ἴδια
Θεοῦ, μηκέτι Θεὸν αὐτὸν ὁμολογεῖν, ἀλλὰ τὸν ἐν αὐ τοῖς Βεελζεβοὺλ ἀντ' ἐκείνου
θεοποιεῖν, ἵνα σὺν αὐτῷ λοιπὸν αἰωνίως ἐν τῷ πυρὶ κολάζωνται. Καὶ αὐτὴ δὲ ἡ τοῦ
ἀναγνώσματος παρατήρησις τοιαύτην ἔμφασίν τινα παρέχειν μοι δοκεῖ, καὶ
δεικνύειν εἰς αὐτὸν φθάνειν ἀμφοτέρας τὰς βλασφη μίας, καὶ περὶ ἑαυτοῦ εἰρηκέναι
τὸ, «Υἱὸς ἀνθρώ που,» καὶ τὸ, «Πνεῦμα·» ἵν' ἐκ τούτου μὲν τὸ σω ματικὸν ἑαυτοῦ
δείξῃ, ἐκ δὲ τοῦ πνεύματος τὴν πνευματικὴν ἑαυτοῦ καὶ νοητὴν καὶ ἀληθεστάτην
θεότητα δηλώσῃ. Καὶ γὰρ τὸ μὲν λαβεῖν δυνάμενον συγγνώμην ἐπὶ τοῦ Υἱοῦ τοῦ
ἀνθρώπου συνέταξεν, ἵνα τὸ σωματικὸν ἑαυτοῦ σημάνῃ· τὴν δὲ ἀσύγγνωστον
βλασφημίαν εἰς τὸ Πνεῦμα φθάνειν δεδήλωκεν· ἵνα, πρὸς ἀντιδιαστολὴν τοῦ
σωματικοῦ τοῦτο λέγων, τὴν ἑαυτοῦ θεότητα δείξῃ. Τοῦτον δὲ τὸν χαρακτῆρα καὶ ἐν
τῷ κατὰ Ἰωάννην Εὐαγγελίῳ ἑώρακα· ὁπη νίκα, περὶ τῆς τοῦ σώματος βρώσεως
διαλεγόμενος, καὶ διὰ τοῦτο πολλοὺς ἑωρακὼς τοὺς σκανδαλισθέντας, φησὶν ὁ
Κύριος· «Τοῦτο ὑμᾶς σκανδαλίζει; Ἐὰν οὖν θεωρῆτε τὸν Υἱὸν τοῦ ἀνθρώπου ἀναβαί
νοντα, ὅπου ἦν τὸ πρότερον; Τὸ Πνεῦμά ἐστι τὸ ζωοποιοῦν· ἡ σὰρξ οὐκ ὠφελεῖ
οὐδέν. Τὰ ῥήματα ἃ ἐγὼ λελάληκα ὑμῖν, πνεῦμά ἐστι καὶ ζωή.» Καὶ ἐν ταῦθα γὰρ
ἀμφότερα περὶ ἑαυτοῦ εἴρηκε, σάρκα καὶ πνεῦμα· καὶ τὸ πνεῦμα πρὸς τὸ κατὰ σάρκα
δι έστειλεν, ἵνα μὴ μόνον τὸ φαινόμενον, ἀλλὰ καὶ τὸ ἀόρατον αὐτοῦ πιστεύσαντες
μάθωσιν, ὅτι καὶ ἃ λέγει οὐκ ἔστι σαρκικὰ, ἀλλὰ πνευματικά. Πόσοις γὰρ ἤρκει τὸ
σῶμα πρὸς βρῶσιν, ἵνα καὶ τοῦ κόσμου παντὸς τοῦτο τροφὴ γένηται; Ἀλλὰ διὰ τοῦτο
τῆς εἰς 26.668 οὐρανοὺς ἀναβάσεως ἐμνημόνευσε τοῦ Υἱοῦ τοῦ ἀνθρώπου, ἵνα τῆς
σωματικῆς ἐννοίας αὐτοὺς ἀφελ κύσῃ, καὶ λοιπὸν τὴν εἰρημένην σάρκα βρῶσιν ἄνω
θεν οὐράνιον, καὶ πνευματικὴν τροφὴν παρ' αὐτοῦ διδομένην μάθωσιν. «Ἃ γὰρ
λελάληκα, φησὶν, ὑμῖν, πνεῦμά ἐστι καὶ ζωή·» ἴσον τῷ εἰπεῖν· Τὸ μὲν δει κνύμενον
καὶ διδόμενον ὑπὲρ τῆς τοῦ κόσμου σωτη ρίας ἐστὶν ἡ σὰρξ ἣν ἐγὼ φορῶ· ἀλλ' αὕτη
ὑμῖν καὶ τὸ ταύτης αἷμα παρ' ἐμοῦ πνευματικῶς δοθή σεται τροφή· ὥστε
πνευματικῶς ἐν ἑκάστῳ ταύτην ἀναδίδοσθαι, καὶ γίνεσθαι πᾶσι φυλακτήριον εἰς
ἀνάστασιν ζωῆς αἰωνίου. Οὕτω καὶ τὴν Σαμα ρεῖτιν ἀφέλκων ὁ Κύριος ἀπὸ τῶν
αἰσθητῶν, Πνεῦμα εἴρηκε τὸν Θεὸν, ὑπὲρ τοῦ μηκέτι σωματικῶς αὐτὴν, ἀλλὰ
πνευματικῶς διανοεῖσθαι περὶ τοῦ Θεοῦ· οὕτω καὶ ὁ προφήτης θεωρῶν τὸν Λόγον
τὸν γενόμενον σάρκα εἴρηκε· «Πνεῦμα προσώπου ἡμῶν Χριστὸς Κύριος·» ἵνα μὴ ἐκ
τοῦ φαινομένου νομίσῃ τις ἄνθρωπον ψιλὸν εἶναι τὸν Κύριον, ἀλλὰ καὶ Πνεῦμα
ἀκούων, γινώσκῃ Θεὸν εἶναι τὸν ἐν σώματι ὄντα. Ἑκάτερον οὖν δῆλον ἂν εἴη
λοιπὸν, ὅτι ὁ τὸν Κύριον ὁρῶν τὰ περὶ ἑαυτοῦ διαλεγόμενον, ἐὰν μὲν τὰ σωματικὰ
αὐτοῦ μόνον βλέπων, καὶ μὴ πιστεύων λέγοι· «Πόθεν τούτῳ ἡ σοφία αὕτη;»
ἁμαρτάνει μὲν, καὶ ὡς εἰς Υἱὸν ἀνθρώπου βλασφημεῖ· ἀλλ' ὁ 26.669 ὁρῶν αὐτοῦ τὰ
ἔργα τὰ Πνεύματι ἁγίῳ γινό μενα, καὶ λέγων τὸν τοιαῦτα ποιοῦντα μὴ εἶναι Θεὸν
καὶ τοῦ Θεοῦ Υἱὸν, ἀλλὰ τῷ Βεελζεβοὺλ αὐτὰ ἐπιγράφων, φανερῶς βλασφημεῖ,
ἀρνούμενος αὐτοῦ τὴν θεό τητα. Καὶ γὰρ καὶ ἐν τῷ εὐαγγελικῷ ῥητῷ, καθὰ
πολλάκις εἴρηται, λέγων, «Υἱὸν ἀνθρώπου,» τὸ κατὰ σάρκα καὶ ἀνθρώπινον αὐτοῦ
δείκνυσιν· ἵνα, «τὸ Πνεῦμα» λέγων, δηλώσῃ αὐτοῦ εἶναι τὸ ἅγιον Πνεῦμα, ἐν ᾧ τὰ
πάντα εἰργάζετο. Ὅθεν τὰ μὲν ἔργα ποιῶν ἔλεγεν· «Εἰ ἐμοὶ μὴ πιστεύετε, κἂν τοῖς
ἔργοις μου πιστεύετε, ἵνα γνῶτε, ὅτι ἐγὼ ἐν τῷ Πατρὶ, καὶ ὁ Πατὴρ ἐν ἐμοί.» Μέλλων
δὲ ἑαυτὸν σωματικῶς προσφέρειν ὑπὲρ ἡμῶν, ὁπηνίκα δι' αὐτὸ τοῦτο ἀνέβη εἰς
Ἱερουσαλὴμ, ἔλεγε τοῖς μαθηταῖς αὐτοῦ· «Καθ εύδετε τὸ λοιπὸν, καὶ ἀναπαύεσθε·
ἰδοὺ γὰρ ἤγγικεν ἡ ὥρα, καὶ ὁ Υἱὸς τοῦ ἀνθρώπου παραδίδοται εἰς χεῖρας
ἁμαρτωλῶν·» τὰ μὲν γὰρ ἔργα Θεὸν ἀληθι νὸν αὐτὸν ἐποίει πιστεύεσθαι· ὁ δὲ
 
 
 
 

6

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

θάνατος ἐδείκνυεν αὐτὸν ἀληθῶς ἔχοντα σῶμα. ∆ιὸ εἰκότως Υἱὸν ἀνθρώ που ἔλεγε
τὸν παραδιδόμενον· ὁ Λόγος γὰρ, ἀθάνατος καὶ ἄψαυστός ἐστι, καὶ αὐτοζωὴ
τυγχάνει ὤν. Ἀλλ' οὔτε τούτοις ἐπίστευον οἱ Φαρισαῖοι, οὔτε ἂ ἐποίουν τούτων οἱ
υἱοὶ, βλέπειν ἐβούλοντο· ὅθεν ἠρέμα πάλιν αὐτοὺς ὁ Κύριος ἐλέγχει λέγων· «Εἰ ἐν
Βεελ ζεβοὺλ ἐγὼ ἐκβάλλω τὰ δαιμόνια, οἱ υἱοὶ ὑμῶν ἐν τίνι ἐκβάλλουσι; ∆ιὰ τοῦτο
αὐτοὶ ὑμῶν κριταὶ ἔσον ται· εἰ δὲ ἐν Πνεύματι Θεοῦ ἐγὼ ἐκβάλλω τὰ δαιμό νια, ἄρα
ἔφθασεν ἐφ' ὑμᾶς ἡ βασιλεία τοῦ Θεοῦ.» Καὶ ὧδε δὲ «ἐν Πνεύματι Θεοῦ» ἔλεγεν, οὐχ
ὡς ἐλάττων ὢν τοῦ Πνεύματος, οὐδὲ ὡς τοῦ Πνεύματος αὐτὰ ἐνεργοῦντος ἐν αὐτῷ,
ἀλλ' ἵνα πάλιν δη λώσῃ, ὅτι αὐτὸς, Λόγος ὢν τοῦ Θεοῦ, αὐτὸς διὰ τοῦ Πνεύματος
ἐνεργεῖ τὰ πάντα, καὶ διδάξῃ τοὺς ἀκούοντας, ὅτι ὅσῳ τὰ τοῦ Πνεύματος ἔργα τῷ
Βεελ ζεβοὺλ ἀνατιθέασι, τοσούτῳ τὸν χορηγοῦντα τὸ Πνεῦμα δυσφημοῦσι· καὶ γὰρ
καὶ τοῦτο λέγων, δεί κνυσιν, ὅτι μὴ ἀγνοοῦντες, ἀλλ' ἑκόντες εἰς τὴν τοι αύτην
ἀναπόδραστον βλασφημίαν ἑαυτοὺς περιπείρουσι. Καὶ οὐκ αἰσχύνονται· καίτοι
γινώσκοντες τὰ τοιαῦτα ἔργα ὄντα τοῦ Θεοῦ, ὅτι τῷ Βεελζεβοὺλ πνεύματος ἔλεγον
γίνεσθαι ταῦτα. 26.672 Πῶς οὖν, ταῦτα τολμῶντες, ἔτι δύνανται τοὺς Ἕλληνας
αἰτιᾶσθαι, πλάττοντας εἴδωλα, καὶ θεοὺς ὀνομάζοντας αὐτά; Ὅμοια γὰρ καὶ τὰ
αὐτῶν τῆς ἐκείνων μανίας· ἢ τάχα καὶ μείζονα τὰ τούτων τολμήματα τυγχάνει ὄντα·
ὅτι, καίπερ νόμον περὶ τούτου λαβόντες, διὰ τῆς παραβάσεως τοῦ νόμου τὸν Θεὸν
ἠτίμασαν. Τί δὲ τοιαῦτα βλασφημοῦντες ποιήσουσιν, ὅταν ἀναγινώσκωσι τὸν
προφήτην Ἡσαΐαν, καὶ ἀκούσωσι, τὰ σημεῖα τῆς τοῦ Χριστοῦ παρου σίας εἶναι
τυφλῶν ἀνάβλεψιν, χωλῶν περίπατον, ἀλάλων λαλιὰν, νεκρῶν ἀνάστασιν, λεπρῶν
θερα πείαν, καὶ κωφῶν ἀκοῆς ἄνοιξιν; ἄρα γὰρ καὶ τούτων τίνα θελήσουσιν εἶναι
τὸν ποιητήν; Τὸν μὲν γὰρ Θεὸν λέγοντες, ἑαυτοὺς διελέγξουσιν ἀσεβήσαν τας εἰς
τὸν Κύριον· ἃ γὰρ ὁ προφήτης προβλέ πων εἴρηκε, ταῦτα παρὼν αὐτὸς ὁ Κύριος πε
ποίηκεν· ἐὰν δὲ, ὡς προπετείᾳ προληφθέντες, τολ μήσωσιν εἰπεῖν κἀκεῖνα ἐν τῷ
Βεελζεβοὺλ γίνε σθαι, φοβοῦμαι μὴ, κατ' ὀλίγον ἐν ἀσεβείᾳ προκόπτοντες, καὶ
ἀναγινώσκοντες· «Τίς ἔδωκε στόμα ἀνθρώπῳ; Καὶ τίς ἐποίησε δύσκωφον καὶ κωφὸν,
βλέποντα καὶ τυφλόν;» καὶ τὰ ὅμοια τούτοις, μανέντες πάλιν εἴπωσι τοῦ Βεελζεβοὺλ
εἶναι καὶ ταύτας τὰς φωνάς. Ὧ γὰρ τὴν τοῦ ἀναβλέπειν χάριν διδόασι, τούτῳ καὶ τὴν
τοῦ μὴ βλέπειν αἰ τίαν αὐτοὺς ἀνάγκη διδόναι· ἀμφότερα γὰρ τὸν αὐτὸν ποιεῖν
εἴρηκεν ὁ Λόγος. Πάντως δὲ τοῦτο λέγον τες, φρονήσουσί ποτε καὶ δημιουργὸν εἶναι
τὸν Βεελ ζεβοὺλ τῆς ἀνθρωπίνης φύσεως· τοῦ γὰρ ποιοῦν τός ἐστιν ἔχειν καὶ τὴν
τῶν γινομένων ἐξουσίαν. Τοῦ γὰρ Μωϋσέως εἰρηκότος· «Ἐν ἀρχῇ ἐποίησεν ὁ Θεὸς
τὸν οὐρανὸν καὶ τὴν γῆν·» καὶ, «ἐν εἰκόνι Θεοῦ ἐποίησεν ὁ Θεὸς τὸν ἄνθρωπον·» καὶ
∆ανιὴλ παῤ ῥησιάζεται τῷ ∆αρείῳ λέγων· «Οὐ σέβομαι εἴ δωλα χειροποίητα, ἀλλὰ
ζῶντα Θεὸν τὸν κτίσαντα τὸν οὐρανὸν καὶ τὴν γῆν, καὶ ἔχοντα πάσης σαρκὸς
κυρείαν·» εἰ μὴ ἄρα πάλιν μεταβαλλόμενοι λέγοιεν τὸ μὲν μὴ βλέπειν καὶ χωλεύειν,
καὶ τὰ ἄλλα πάθη ἐξ ἐπιτιμίας τοῦ δημιουργοῦ γίνεσθαι· τὴν δὲ λύσιν τούτων καὶ
τὴν εἰς τοὺς πάσχοντας εὐεργεσίαν τὸν Βεελζεβοὺλ ἐργάζεσθαι. Ἀλλὰ τοῦτο κἂν
ἐξετάζειν, λίαν εὔηθές ἐστι. Μαινομένων γὰρ καὶ πλέον παρεξεστηκότων ἐστὶν ἡ
τοιαύτη μετὰ ἀσεβείας μωρολογία· ὅτι μηδὲ τὰ κρείττονα κατὰ σύγκρισιν τῷ Θεῷ,
ἀλλὰ τῷ Βεελζεβοὺλ διδόασιν οἱ παράφρονες. Καὶ γὰρ οὐ μέλει τούτοις λυμαίνεσθαι
τῶν θείων Γραφῶν τὰ δόγματα, ἵνα μόνον ἀρνήσωνται τὴν τοῦ Χριστοῦ παρουσίαν.
26.673 Ἔδει δὲ τοὺς πονηροὺς ἢ μηδὲ ἐκ τῶν σωμα τικῶν ἐξουθενεῖν αὐτοὺς ὡς
ἄνθρωπον τὸν Κύ ριον, ἢ δηλονότι καὶ ἐκ τῶν ἔργων Θεὸν αὐτὸν ὁμολογεῖν
ἀληθινόν. Ἀλλὰ πάντα διεστραμμένως ἔπρατ τον· ἄνθρωπον μὲν βλέποντες, ὡς
ἀνθρώπῳ ἐλοιδο ροῦντο· ἔργα δὲ θεϊκὰ θεωροῦντες, ἠρνοῦντο τὴν θεότητα, καὶ
 
 
 
 

7

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

προσέτρεχον τῷ διαβόλῳ, οἰόμενοι, ὅτι, ταῦτα τολμῶντες, διαδράναι δυνήσονται,
ὥστε μὴ κριθῆναι παρ' αὐτοῦ τοῦ ὑβριζομένου παρ' αὐτῶν Λόγου. Οἱ μὲν οὖν
ἐπαοιδοὶ, καὶ μάγοι, καὶ φαρμα κοὶ τοῦ Φαραὼ, καίτοι πλεῖστα ἐπιχειρήσαντες, ὅμως
βλέποντες τὰ διὰ Μωσέως σημεῖα, καθυφῆκαν, καὶ νῶτα δεδώκασι, λέγοντες
δάκτυλον εἶναι Θεοῦ τὸν ταῦτα ποιοῦντα· οἱ δὲ Φαρισαῖοι καὶ οἱ γραμ ματεῖς, ὅλην
τὴν χεῖρα τοῦ Θεοῦ βλέποντες ἐργαζο μένην, καὶ πλείονα καὶ μείζονα θεωροῦντες
ἐκείνων τὰ δι' αὐτοῦ τοῦ Σωτῆρος γινόμενα, τὸν Βεελζε βοὺλ ἔλεγον ταῦτα ποιεῖν,
ὃν καὶ οἱ μάγοι, καίτοι ἴδιον αὐτῶν ὄντα, ὅμως ὡμολόγουν μὴ δύνασθαι μηδὲ τὰ
τούτων ἐλάττονα ποιεῖν. Τίς οὖν ὑπερβολὴ τῆς τούτων μανίας εἰπεῖν; ἢ τίς, ὡς
εἴρηκεν ὁ προφήτης, ὁμοιῶσαί τι ταῖς τούτων ἀσεβείαις δυνήσεται; Ἐδικαίωσαν γὰρ
ἐξ ἑαυτῶν καὶ Σοδομίτας· καὶ νενικήκασι μὲν τὴν Ἑλλήνων ἀγνωσίαν,
ὑπερβάλλουσι δὲ καὶ τῶν μάγων τοῦ Φαραὼ τὴν ἀλογίαν, καὶ πρὸς μόνους τοὺς
Ἀρειομανίτας ἔχουσι τὴν σύγκρισιν, ἀλλήλοις εἰς τὴν αὐτὴν ἀσέβειαν συμπίπτοντες.
Ἰουδαῖοι μὲν γὰρ βλέποντες διὰ τοῦ Υἱοῦ τὰ τοῦ Πατρὸς ἔργα, τῷ Βεελζεβοὺλ
ἀνατιθέασιν αὐτά· Ἀρειανοὶ δὲ, καὶ αὐτοὶ τὰ αὐτὰ βλέποντες ἔργα, τοῖς κτίσμασι
συναριθμοῦσι τὸν ταῦτα ποιοῦντα Κύριον, φάσκοντες ἐξ οὐκ ὄντων αὐτὸν εἶναι· καὶ,
οὐκ ἦν, πρὶν γένηται· Καὶ οἱ μὲν Φαρισαῖοι θεωροῦντες τὸν Κύριον ἐν σώματι
διεγόγγυζον λέγοντες· «∆ιὰ τί σὺ, ἄνθρωπος ὢν, ποιεῖς σεαυτὸν Θεόν;» οἱ δὲ
Χριστομάχοι, βλέποντες αὐτὸν πάσχοντα καὶ κοιμώμενον, βλασφημοῦσι λέγοντες· Ὁ
ταῦτα ὑπομένων οὐ δύναται Θεὸς ἀληθινὸς καὶ ὁμοούσιος εἶναι τοῦ Πατρός. Καὶ
ὅλως ἄν τις ἐξετάζειν ἐκ παραλλήλου βούλοιτο τὰς ἀμφοτέρων κακονοίας, εὑρήσει,
καθὰ προεῖπον, εἰς τὴν κοιλάδα τῆς ἁλυκῆς παροινίας συμπίπτοντας αὐτοὺς
ἀλλήλοις. 26.676 ∆ιὰ τοῦτο γὰρ κἀκείνοις καὶ τούτοις ἀσύγγνωστον ἔσεσθαι τὴν ἐπὶ
τοῖς τοιούτοις τιμωρίαν ὁ Κύριος ἀπεφήνατο λέγων· «Ὃς δ' ἂν εἴπῃ κατὰ τοῦ
Πνεύματος τοῦ ἁγίου, οὐκ ἀφεθήσεται αὐτῷ οὔτε ἐν τῷ αἰῶνι τούτῳ, οὔτε ἐν τῷ
μέλλοντι.» Καὶ εἰκότως γε· ὁ γὰρ ἀρνούμενος τὸν Υἱὸν, τίνα παρακαλῶν, ἱλασμὸν
δυνήσεται ἔχειν; ἢ ποίαν ζωὴν, ἢ ἀνάπαυσιν προσδοκήσει, ὁ ἀπωσάμενος τὸν
λέγοντα· «Ἐγώ εἰμι ἡ ζωή;» καὶ, «∆εῦτε πρὸς μὲ, πάντες οἱ κοπιῶντες καὶ
πεφορτισμένοι, κἀγὼ ἀναπαύσω ὑμᾶς;» Τούτων δὲ οὕτω διδόντων δίκας, δῆλον ἂν
εἴη, ὡς οἱ εὐσεβοῦντες εἰς τὸν Χριστὸν, καὶ τὸ κατὰ σάρκα καὶ τὸ κατὰ Πνεῦμα
προσκυνοῦντες αὐτοῦ, καὶ μήτε Υἱὸν Θεοῦ αὐτὸν ἀγνοοῦντες, μήτε ὅτι καὶ Υἱὸς
ἀνθρώπου γέγονεν ἀρνούμενοι· ἀλλὰ καὶ πιστεύοντες, ὅτι «Ἐν ἀρχῇ ἦν ὁ Λόγος,»
καὶ, «ὁ Λόγος σὰρξ ἐγένετο,» βασιλεύσουσιν αἰωνίως ἐν τοῖς οὐρανοῖς κατὰ τὰς
ἁγίας ἐπαγγελίας αὐτοῦ τοῦ Κυρίου καὶ Σωτῆρος ἡμῶν Ἰησοῦ Χριστοῦ λέγοντος·
«Ἀπελεύσονται μὲν ἐκεῖνοι εἰς κόλασιν αἰώνιον, οἱ δὲ δίκαιοι εἰς ζωὴν αἰώνιον.»
Ταῦτά σοι συντόμως, ἐξ ὧν ἔμαθον, ἔγραψα. Σὺ δὲ μὴ ὡς τελείαν διδασκαλίαν, ἀλλὰ
μόνην ἀφορμὴν ταῦτα παρ' ἐμοῦ λάμβανε· καὶ λοιπὸν σαυτῷ τὴν ἀκριβεστέραν
διάνοιαν ἔκ τε τοῦ εὐαγγελικοῦ ῥητοῦ καὶ τῶν Ψαλμῶν ἀναλαμβάνων, δέσμευε τὰ
τῆς ἀληθείας δράγματα· ἵνα καὶ, σοῦ ταῦτα φέροντος, εἴπωσιν οἱ λέγοντες·
«Ἐρχόμενοι δὲ ἥξουσιν ἐν ἀγαλλιάσει, αἴροντες τὰ δράγματα αὐτῶν,» ἐν Χριστῷ
Ἰησοῦ τῷ Κυρίῳ ἡμῶν, δι' οὗ καὶ μεθ' οὗ τῷ Πατρὶ ἅμα τῷ ἁγίῳ Πνεύματι ἡ δόξα καὶ
τὸ κράτος, εἰς τοὺς αἰῶνας τῶν αἰώνων. Ἀμήν.

 
 
 
 

8

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

