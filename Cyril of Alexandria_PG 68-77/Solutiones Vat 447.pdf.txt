Solutiones (fragmentum e tractatu de dogmatum solutione) (e cod. Vat.
gr. 447)
Ηʹ. Ἄνθρωπός τις ἦν πλούσιος, εὐφραινόμενος καθ' ἡμέραν λαμπρῶς·
πτωχὸς δέ τις Λάζαρος ἐπὶ τὸν τούτου πυλῶνα ἐβέβλητο ἡλκωμένος κατὰ τὴν
εὐαγγελικὴν ἱστορίαν. ἐγένετο οὖν ἀμφοτέροις ἀποθανεῖν καὶ τοῦτον μὲν τὸν
πτωχὸν εἰς τὴν ἀνάπαυσιν ἀπελθεῖν, τὸν δὲ εἰς τὴν κόλασιν. ταῦτα ἤδη γέγονε 86
καὶ ἀνταπόδοσις ἀξία ἐκληρώθη ἑκάστῳ, ἢ τῆς μελλούσης κρίσεως ἀνατυποῖ τὴν
εἰκόνα ἐν τούτοις; ἀλλά, φησίν, ὁπότε ὀνομάζει Λαζάρου προσηγορίαν, ἀληθῶς
γέγονε καὶ ἐπράχθη. διὰ τί γὰρ μὴ εἶπε Πτωχὸς δέ τις ἄνθρωπος, ἀλλὰ Λάζαρος; ἵνα
τῇ προσηγορίᾳ δείξῃ πείρᾳ καὶ ἀληθείᾳ ταῦτα πεπράχθαι. Ἐπίλυσις Τὴν κρίσιν κτλ.
Θʹ. Εἰ ἔλαβεν Ὠσηὲ ὁ προφήτης γυναῖκα πόρνην καὶ ἐτέκνωσεν ἐξ αὐτῆς
πράξει καὶ ἐνεργείᾳ, ἢ προφητικῶς νοούμενα λέγει. Εἰ ὁ Μελχισεδὲκ οὐκ ἄνθρωπος
ἁπλῶς οὐδὲ πνεῦμα, ἀλλ' ἄνθρωπος ἀρχὴν γενέσεως οὐκ ἐξ ἀνθρώπων ἔχων ἀλλὰ
προσφάτως δημιουργηθεὶς ὑπὸ τοῦ θεοῦ.
CYR.Περὶ τούτων τῶν κεφαλαίων μακρὸς ἡμῖν πεποίηται λόγος, ὅτε
ἐγράφομεν εἰς τὸν Ὠσηὲ τὸν προφήτην, καὶ ἐν τῷ βιβλίῳ δὲ τῷ περὶ τῆς Γενέσεως
πολλὴ βάσανος εὑρίσκεται περὶ τοῦ Μελχισεδέκ, καὶ ἔξεστι τῇ εὐλαβείᾳ σου ταῖς
βίβλοις ἐντυχεῖν κἀκεῖθεν λαβεῖν τῶν εἰρημένων ἐφ' ἑκάστῳ τὸν νοῦν.
 

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

