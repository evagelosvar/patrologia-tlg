Epistula ad Constantium imperatorem
Ἐ ΠΙΣΤΟΛΗ ΚΥΡΙΛΛΟΥ ἘΠΙΣΚΟΠΟΥ}1 ἹΕΡΟΣΟΛΥΜΩΝ ΠΡΟΣ
ΚΩΝΣΤΑΝΤΙΟΝ ΤΟΝ ΒΑΣΙΛΕΑ ΠΕΡΙ ΤΟΥ ἘΝ ΟΥΡΑΝΩ ΦΑΝΕΝΤΟΣ
ΣΗΜΕΙΟΥ Βασιλεῖ θεοφιλεστάτῳ καὶ εὐσεβεστάτῳ Κωνσταντίῳ
Αὐγούστῳ, Κύριλλος ὁ ἐν Ἱεροσολύμοις ἐπίσκοπος ἐν κυρίῳ χαίρειν.
1 Πρώτας ταύτας ἐξ Ἱεροσολύμων πρὸς τὴν θεοφιλῆ σου βασιλείαν ἀποστέλλω
γραμμάτων ἀπαρχάς, σοί τε πρεπούσας ὑποδέξασθαι κἀμοὶ παρασχεῖν· οὐ λόγων
κολακείας πεπληρωμένας, ἀλλ' ἐπουρανίων μηνυτικὰς θεοπτίας· οὐδ' εὐσυνθέτους
λόγων ῥητορικὰς πιθανότητας περιεχούσας, ἀλλ' ἁγίων εὐαγγελίων προρρήσεσι διὰ
τῆς τῶν πραγμάτων ἐκβάσεως μαρτυρούσας τὴν ἀλήθειαν. 2 Ἕτεροι μὲν γὰρ ἀφ' ὧν
ἔχουσι τὴν τιμίαν σου πολλάκις στεφανοῦσι κεφαλήν, χρυσοκολλήτους στεφάνους
λίθοις διαυγεστάτοις πεποικιλμένους προσκομίζοντες. Ἡμεῖς δὲ οὐ τοῖς ἀπὸ γῆς σε
στεφανοῦμεν (τὰ γὰρ ἀπὸ γῆς δωρούμενα τέλος ἔχει τὴν γῆν)· ἀλλ' ἐπουρανίων
πραγμάτων θεϊκὴν ἐνέργειαν ἐν τοῖς τῆς θεοφιλοῦς σου βασιλείας καιροῖς ἐν
Ἱεροσολύμοις τελεσθεῖσαν τῆς σῆς εὐσεβείας μετὰ σπουδῆς ἄγομεν εἰς γνῶσιν· οὐχ
ἵνα νῦν ἐξ ἀγνοίας ἔλθῃς εἰς θεογνωσίαν (φθάνεις γὰρ καὶ ἑτέρους διδάσκων δι' ὧν
εὐσεβεῖς· ἀλλ' ἵνα, ἅπερ ᾔδεις, ταῦτα καὶ βεβαιωθῇς, καὶ τὸν τῆς βασιλείας
προγονικὸν ὑποδεξάμενος κλῆρον, μείζοσι θεόθεν στεφάνοις ἐπουρανίοις
τετιμῆσθαι μαθών, θεῷ μὲν τῷ παμβασιλεῖ καὶ νῦν τὴν πρέπουσαν ἀποδῷς
εὐχαριστίαν, μείζονος δὲ τοῦ κατ' ἐχθρῶν θάρσους πληρωθῇς, ἔργῳ τὴν σὴν
βασιλείαν ἀγαπᾶσθαι πρὸς θεοῦ, δι' ὧν ἐπὶ σοῦ θαυματουργεῖ, καταλαβών. 3 Ἐπὶ μὲν
γὰρ τοῦ θεοφιλεστάτου καὶ τῆς μακαρίας μνήμης Κωνσταντίνου τοῦ σοῦ πατρός, τὸ
σωτήριον τοῦ σταυροῦ ξύλον ἐν Ἱεροσολύμοις ηὕρηται, τῆς θείας χάριτος τῷ καλῶς
ζητοῦντι τὴν εὐσέβειαν τῶν ἀποκεκρυμμένων ἁγίων τόπων παρασχούσης τὴν
εὕρεσιν. Ἐπὶ δὲ σοῦ, δέσποτα πανευσεβέστατε βασιλεῦ, προγονικὴν εὐσέβειαν
μείζονι τῇ πρὸς τὸ θεῖον εὐλαβείᾳ νικῶντος, οὐκ ἀπὸ γῆς λοιπόν, ἀλλ' ἐξ οὐρανῶν
τὰ θαυματουργήματα· καὶ τοῦ κυρίου καὶ σωτῆρος ἡμῶν Ἰησοῦ Χριστοῦ, τοῦ
μονογενοῦς υἱοῦ τοῦ θεοῦ, τὸ κατὰ τῆς τοῦ θανάτου νικῆς τρόπαιον, ὁ μακάριος
λέγω σταυρός, φωτὸς μαρμαρυγαῖς ἀπαστράπτων ἐν Ἱεροσολύμοις ὤφθη. 4 Ἐν γὰρ
ταῖς ἁγίαις ἡμέραις ταύταις τῆς ἁγίας πεντηκοστῆς, νόνναις μαΐαις, περὶ τρίτην
ὥραν, παμμεγέθης ὁ σταυρός, ἐκ φωτὸς κατεσκευασμένος, ἐν οὐρανῷ, ὑπεράνω τοῦ
ἁγίου Γολγοθᾶ μέχρι τοῦ ἁγίου Ὄρους τῶν ἐλαιῶν ἐκτεταμένος ἐφαίνετο· οὐχ ἑνὶ
καὶ δευτέρῳ μόνον φανείς, ἀλλὰ παντὶ τῷ τῆς πόλεως πλήθει φανερώτατα δειχθείς·
οὐδ' ὡς ἄν τις νομίσειεν ὀξέως κατὰ φαντασίαν παραδραμών, ἀλλ' ἐπὶ πλείοσιν
ὥραις ὑπὲρ γῆν ὀφθαλμοφανῶς θεωρούμενος, καὶ ταῖς ἀπαστραπτούσαις
μαρμαρυγαῖς τὰς ἡλιακὰς ἀκτῖνας νικήσας (ἦ γὰρ ἂν ὑπ' αὐτῶν νικώμενος
ἐκαλύπτετο, εἰ μὴ δυνατωτέρας ἡλίου τοῖς ὁρῶσι παρεῖχε τὰς λαμπηδόνας)· ὡς ἅπαν
μὲν ἀθρόως ἐξαυτῆς εἰς τὴν ἁγίαν ἐκκλησίαν συνδραμεῖν τῆς πόλεως τὸ πλῆθος, τῷ
τῆς θεοπτίας φόβῳ μετ' εὐφροσύνης κατασχεθέν, νέων ἅμα καὶ πρεσβύτων ἀνδρῶν
τε καὶ γυναικῶν, καὶ πάσης ἡλικίας καὶ μέχρις αὐτῶν ἤδη τῶν κατ' οἴκους
θαλαμευομένων κορῶν, ἐντοπίων τε καὶ ξένων, χριστιανῶν τε ἅμα καὶ τῶν
ἀλλαχόθεν ἐπιδημούντων ἐθνικῶν· ὁμοθυμαδὸν δὲ πάντων ὡς ἐξ ἑνὸς στόματος
Χριστὸν Ἰησοῦν τὸν κύριον ἡμῶν, τὸν υἱὸν τοῦ θεοῦ τὸν μονογενῆ, τὸν
θαυματοποιὸν ἀνυμνούντων, ἔργῳ τε καὶ πείρᾳ παραλαβόντων· ὅτι χριστιανῶν τὸ
δόγμα τὸ πανευσεβὲς οὐκ ἐν πειθοῖς σοφίας ἐστι λόγοις, ἀλλ' ἐν ἀποδείξει
πνεύματος καὶ δυνάμεως, οὐχ ὑπ' ἀνθρώπων μόνον καταγγελλόμενον, ἀλλ' ἐξ
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

οὐρανῶν θεόθεν μαρτυρούμενον. 5 Ἐπεὶ τοίνυν ἡμεῖς, οἱ τῆς Ἱερουσαλὴμ οἰκήτορες,
τὸ τοῦ θαύματος παράδοξον αὐταῖς ὄψεσι παραλαβόντες, θεῷ μὲν τῷ παμβασιλεῖ καὶ
υἱῷ τῷ μονογενεῖ τοῦ θεοῦ τὴν πρέπουσαν μετ' εὐχαριστίας προσκύνησιν
ἀπεδώκαμεν τε καὶ ἀποδώσομεν, εὐχὰς ἐκτενεῖς καὶ ὑπὲρ τοῦ θεοφιλοῦς σου
κράτους ἐν τοῖς ἁγίοις τόποις πεποιημένοι τε καὶ ποιησόμενοι, ἔδει δὲ τὰς
ἐπουρανίους ταύτας θεοπτίας μὴ παραδοῦναι σιωπῇ, ἀλλ' εὐαγγελίσασθαι σου τὴν
ἔνθεον εὐσέβειαν ἐξαυτῆς διὰ τούτων ἐσπούδασα τῶν γραμμάτων· ἵν' ἀγαθῷ
θεμελίῳ τῆς παρὰ σοὶ προϋπαρχούσης πίστεως τὴν ἐκ τῶν προσφάτως θεόθεν
ἐπιδειχθέντων γνῶσιν ἐποικοδομήσας, βεβαιότερον μὲν τὴν εἰς τὸν κύριον ἡμῶν
Ἰησοῦν Χριστὸν ἀναλάβῃς τὴν ἐλπίδα, θαρσῶν δὲ καὶ πασῆς ἀνδρείας συνήθως
πεπληρωμένος, ὡς ἂν αὐτὸν θεὸν ἔχων σύμμαχον, τὸ τοῦ σταυροῦ τρόπαιον
ἑτοιμότατα προφέρῃς, καύχημα καυχημάτων, τὸ δειχθὲν ἐν οὐρανῷ σημεῖον
ἐπαγόμενος· οὗ καὶ τὸ σχῆμα δείξας ἀνθρώποις οὐρανὸς μειζόνως ἐκαυχήσατο. 6 Τὸ
μέντοι θαυματούργημα τοῦτο, βασιλεῦ θεοφιλέστατε, προφητῶν μαρτυρίαις καὶ
Χριστοῦ φωναῖς ἁγίοις εὐαγγελίοις ἐγκειμέναις ἀκολούθως, ἐτελέσθη τε νῦν καὶ
πάλιν μειζόνως τελεσθήσεται· ἐν γὰρ τῷ κατὰ Ματθαῖον εὐαγγελίῳ μελλόντων
πραγμάτων γνῶσιν ὁ σωτὴρ τοῖς μακαρίοις ἀποστόλοις αὐτοῦ δωρούμενος καὶ δι'
ἐκείνων τοῖς μετ' αὐτοὺς ὁμιλῶν λευκότατα προαναφωνῶν ἔλεγε· "Καὶ τότε
φανήσεται τὸ σημεῖον τοῦ υἱοῦ τοῦ ἀνθρώπου ἐν οὐρανῷ". Ἥντινα τοῦ εὐαγγελίου
θείαν βίβλον συνηθῶς εἰς χεῖρας ἀναλαβών, τὰς περὶ τούτου κειμένας
προαγγελτικὰς μαρτυρίας ἐγγράφως εὑρήσεις· ὧν μαλιστά σε, δέσποτα, πυκνοτέρᾳ
τῇ μελέτῃ προσέχειν παρακαλῶ, διὰ τὰ λοιπὰ τῶν κατὰ τὴν αὐτόθι γεγραμμένων
ἀκολουθίαν, τῶν ὑπὸ τοῦ σωτῆρος ἡμῶν προαναφωνηθέντων, πολλῆς μετ'
εὐλαβείας δεομένων τῆς παρατηρήσεως, πρὸς τὸ μηδεμίαν ἐξ ἀντικειμένης
ἐνεργείας ὑπομεῖναι τὴν βλάβην. 7 Ταύτας σοὶ τῶν ἡμετέρων λόγων, βασιλεῦ
θεοφιλέστατε, προσφέρω τὰς ἀπαρχάς· ταύτας ἐξ Ἱεροσολύμων πρώτας
ἀποφθέγγομαι τὰς φωνάς, σοὶ τῷ γνησιωτάτῳ καὶ πανευσεβεῖ μεθ' ἡμῶν Χριστοῦ
προσ κυνητῇ, τοῦ μονογενοῦς υἱοῦ τοῦ θεοῦ καὶ σωτῆρος ἡμῶν, τοῦ τὴν
οἰκουομικὴν κατὰ τὰς θείας γραφὰς ἐν Ἱεροσολύμοις τελέσαντος σωτηρίαν, τοῦ
πατήσαντος μὲν ἐνταῦθα τὸν θάνατον, οἰκείοις δὲ τιμίοις αἵμασι τὰς ἀνθρώπων
ἁμαρτίας ἐξαλείψαντος, ζωὴν δὲ καὶ ἀφθαρσίαν καὶ πνευματικὴν ἐπουράνιον χάριν
τοῖς πιστεύουσιν ἅπασιν παρασχόντος. Οὗ τῇ τε δυνάμει καὶ τῇ χάριτί σε
φρουρούμενον, λαμπροτέροις τε καὶ μείζοσιν εὐσεβείας ἐμπρέποντα κατορθώμασιν,
υἱῶν τε γνησίων βασιλικοῖς ἐναβρυνόμενον βλαστήμασιν, αὐτὸς ὁ παμβασιλεὺς
θεός, ὁ πάσης ἀγαθωσύνης πάροχος, πολλαῖς ἐτῶν εἰρηνικαῖς περιόδοις, καύχημα
χριστιανῶν καὶ κόσμῳ σύμπαντι φυλάξειε πανέστιον. 8 ἐρρωμένον σε καὶ πάσῃ
κεκοσμημένον ἀρετῇ καὶ τὴν συνήθη τῶν τε ἁγίων ἐκκλησιῶν καὶ τῆς ῥωμαίων
ἀρχῆς τὴν φιλανθρωπίαν ἐνδεικνύμενον καὶ μείζοσιν εὐσεβείας βραβείοις
ἐναβρυνόμενον, πολλαῖς εἰρηνικαῖς ἐτῶν περιόδοις ὁ τῶν ὅλων θεὸς ἡμῖν χαρίσηται
πανέστιον, Αὔγουστε βασιλεῦ θεοφιλέστατε.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

