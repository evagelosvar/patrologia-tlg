
//this function displays or hides object t -- IE only
function toggle(t) {
	var x=document.getElementById(t);
  if (x.style.display != "none")
    x.style.display = "none";
  else
    x.style.display = "inline";
}

function t(div,pic) {
  var d, p;
  if (document.getElementById) { // DOM-compliant browsers
    d=document.getElementById(div);
    p=document.getElementById(pic);
  }
  if (d.style.display == "block") {
    d.style.display="none";
    p.src="../../../../pix/shut.gif";
  } else {
    d.style.display="block";
    p.src="../../../../pix/open.gif";
  }
}


//this function will page backward or forward with the press of
//an arrow key (in IE) or comma or dot (Netscape or IE)
function handler(e) {
  if (navigator.appName.indexOf("Microsoft") != -1)
  {
    k = window.event.keyCode;
    shift = window.event.shiftKey;
  }
  else if(navigator.userAgent.indexOf("Firefox") != -1)
  {
    k = e.keyCode;
    shift = e.shiftKey;
  }
  else
  {
    k=e.which;
    shift=false;
  }

  if (shift) {
    if (k == 37) location.href=document.links[0].href;	//left arrow
    if (k == 38) location.href=document.links[1].href;	//up arrow
    if (k == 39) location.href=document.links[2].href;	//right arrow
    // up and down arrows scroll
  } else {
    if (k==37 || k==44 || k==188)
      location.href=document.links[0].href;		//prev of navbar
    if (k==39 || k==46 || k==190 || k==13)
      location.href=document.links[2].href;		//next of navbar
  }
}

function recurseIE(terms,thetext,begin,end) {
	if (end < begin)
		return -1;
	var mid=Math.floor((begin+end)/2);
	if (terms[mid].innerText.slice(0,-1).toLowerCase()==thetext)
		return mid;
	if (terms[mid].innerText.slice(0,-1).toLowerCase() < thetext)
		return recurseIE(terms,thetext,mid+1,end);
	return recurseIE(terms,thetext,begin,mid-1);
}

function recurseNet(terms,thetext,begin,end) {
	if (end < begin)
		return -1;
	var mid=Math.floor((begin+end)/2);
	if (terms[mid].firstChild.data.toLowerCase()==thetext)
		return mid;
	if (terms[mid].firstChild.data.toLowerCase() < thetext)
		return recurseNet(terms,thetext,mid+1,end);
	return recurseNet(terms,thetext,begin,mid-1);
}

function findDict(dictword) {
	var thetext = dictword.toLowerCase();
	var place, terms;
	if (navigator.appName.indexOf("Netscape") != -1) {
		terms = document.body.getElementsByTagName("dt");
		/*for (var i=0; i &lt; terms.length; i++) {
			var t = terms[i];
			if (t.firstChild.data == thetext) {
				self.location = "#" + t.getAttribute("id");
				return false;
			}
		}*/
		place = recurseNet(terms,thetext,0,terms.length-1);
	}
	else if (navigator.appName.indexOf("Microsoft") != -1) {
		terms = document.all.tags("dt");
		place = recurseIE(terms,thetext,0,terms.length-1);
		/*for (var i=0; i &lt; terms.length; i++) {
			var t = terms[i];
			if (t.innerText.slice(0,-1) == thetext) {
				self.location = "#" + t.getAttribute("id");
				return false;
			}
		}*/
	}

	if (place != -1) {
		self.location = "#" + terms[place].getAttribute("id");
		return false;
	}

	alert("Word not found: " + dictword);
	return false;
}

document.onkeydown=handler;


//-------------------------------------------------------------
//Image zoom in/out script- by javascriptkit.com
//Visit JavaScript Kit (http://www.javascriptkit.com) for script
//Credit must stay intact for use

var zoomfactor=0.15 //Enter factor (0.05=5%)

function zoomhelper(originalW, originalH){ 
 if (parseInt(whatcache.style.width)>10&&parseInt(whatcache.style.height)>10){ whatcache.style.width=parseInt(whatcache.style.width)+parseInt(whatcache.style.width)*zoomfactor*prefix 
whatcache.style.height=parseInt(whatcache.style.height)+parseInt(whatcache.style.height)*zoomfactor*prefix
}
}

function zoom(originalW, originalH, what, state){
if (!document.all&&!document.getElementById)
return
whatcache=eval("document.images."+what)
if (parseInt(whatcache.style.width) > (originalW*3) && state=="in") {
return
}
if (parseInt(whatcache.style.width) < (originalW/3) && state=="out") {
return
}
prefix=(state=="in")? 1 : -1

if (whatcache.style.width==""||state=="restore"){
whatcache.style.width=originalW
whatcache.style.height=originalH
if (state=="restore")
return
}
zoomhelper(originalW, originalH)
}

function clearzoom(){
if (window.beginzoom)
clearInterval(beginzoom)
}

function getCookie(NameOfCookie){
  if (document.cookie.length > 0){
    begin = document.cookie.indexOf(NameOfCookie+"=");
    if (begin != -1){
      begin += NameOfCookie.length+1;
      end = document.cookie.indexOf(";", begin);
      if (end == -1) end = document.cookie.length;
      return unescape(document.cookie.substring(begin, end));
    }
  }
  return null;
}


//----------------------------------------------------------
// document footer for client-side documents
//

function goToPage(fileName, defaultPath) {
    var ccelRoot = getCookie('ccel_home');
    if (ccelRoot) {
        location.href = ccelRoot + fileName;
    }
    else {
        location.href = defaultPath + fileName;
    }
}


function writefooter() {

document.writeln('<br><table align="center" style="clear:right">');
document.writeln('<tr>');
document.writeln('<td><a href="http://www.ccel.org"><img src="../pix/gem-icon2.gif" border="0" alt="CCEL home page"></a></td>');
document.writeln('<td align="center"><span style="font-size:9pt; font-style:italic">');
document.writeln('This document is from the <a class="TOC" href="http://www.ccel.org">Christian Classics ');
document.writeln('Ethereal Library</a> at <a class="TOC" href="http://www.calvin.edu">Calvin College</a>,<br>');
document.write('<a href="javascript:goToPage(');
document.write("'prefs.xml', '../../../../../')");
document.write('">Preferences</a> | <a href="javascript:goToPage(');
document.write("'main.xml', '../../../../../')");
document.write('">Main</a>');
document.writeln('<td><a class="TOC" href="http://www.calvin.edu"><img src="../pix/seal.gif" border="0" alt="Calvin seal: My heart I offer you O Lord, promptly and sincerely"></a></td>');
document.writeln('</tr>');
document.writeln('</table>');
document.close();
}

