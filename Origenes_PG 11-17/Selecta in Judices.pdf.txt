Selecta in Judices
ΩΡΙΓΕΝΟΥΣ ΕΚΛΟΓΑΙ ΕΙΣ ΤΟΥΣ ΚΡΙΤΑΣ.
12.949 Καὶ εὔξατο Ἰεφθάε εὐχὴν τῷ Κυρίῳ. Εἰ Πνεῦμα Κυρίου ἐπ' αὐτὸν, πῶς εὔχεται
ἃ μὴ προσήκει; Ἐνῆν γὰρ καὶ κύνα ἀπαντῆσαι ἐπανιόντι τῷ Ἰεφθάε. Ἆρ' οὖν ἐσφάλη
τὸ Πνεῦμα; οὐδαμῶς· ἀλλὰ τὸ μὲν Πνεῦμα βοηθεῖν παραγέγονεν, οὐχ ὑποβάλλειν
τὴν ἄτοπον εὐχήν· αὐτὸς δὲ σφάλλεται ἀπερισκέπτως ὑποσχόμενος. Καὶ εἶπαν αὐτῷ
Εἱπὸν δὴ στάχυς. Οἱ δὲ ἔλεγον στάχυς· σημαίνει σέβηλα, ὅπερ ἐστὶν ἄσταχυς. Οἱ δὲ
ἔλεγον σεβήλω καὶ οὐκ ἐδύναντο τρανῶς εἰπεῖν σέβηλα. Εἰ γὰρ καὶ Ἑβραῖοι
ἐτύγχανον, ἀλλὰ κατὰ συμμετρίαν βραχύτητι παρήλλασσον ἐν τῇ τῆς διαλέξεως
προφορᾷ. Τὸ οὖν σύνθεμα ἦν σέβηλα, ὅπερ οἱ ἐπερωτώμενοι λέγοντες σεβήλω,
ἠλέγχοντο οὐκ ὄντες τῶν οἰκείων τοῦ Ἰεφθάε, ἀλλὰ τῶν ἐπαναστάντων. Καὶ γυνὴ
αὐτοῦ στεῖρα καὶ οὐκ ἔτεκεν. Οὐκ ἀπὸ τῆς φύσεως, ἀλλ' ἀπὸ τῆς διακονίας ἐπὶ τοῦ
παρόντος στείραν καλεῖ. Σίδηρος οὐκ ἀνέβη ἐπὶ τὴν κεφαλήν μου ὅτι Ναζιραῖος τοῦ
Θεοῦ ἐγώ εἰμι. ∆ι' ὅλης τῆς περὶ τὸν Σαμψὼν ἱστορίας αἱ τρίχες φαίνονται τὴν πᾶσαν
αὐτῷ περιποιεῖν ἰσχύν. Τοῦ αὐτοῦ. Ναζιραῖον τὸ πεφυλαγμένον κατὰ τὴν Σύρων
φωνὴν ὀνομάζει. Μὴ ἐξακολουθεῖν δὲ τῇ τάξει τῶν Ἰσραηλιτῶν τὸν Σαμψὼν ὁ Θεὸς
προστάσσει, κατ' ἐκεῖνο καιροῦ τὴν κεφαλὴν ξυρωμένων διὰ τὸ εὐσήμους εἶναι πρὸς
πάντας. Καὶ ἐποίησεν Ἐφὼδ καὶ Θεραφείν. Ἐφὼδ ὕφασμα ἦν ἱερατικὸν, ὅπερ
περικειμένῳ τῷ ἱερεῖ ἐφοίτα τοῦ ἁγίου Πνεύματος ἡ χάρις. Τὸ δὲ Θεραφεὶν τοιοῦτο
μέντοι, οὐ θεϊκὸν δὲ, ἀλλ' εἰδωλικὸν, ὡς ἐν ταῖς βασιλείαις δείκνυται. Καὶ ἐξῆλθον
οἱ υἱοὶ Βενιαμίν. Τίνος ἕνεκεν ἡττῶνται δικαίως ἐπελθόντες; Ἢ δῆλον ὅτι διὰ
προλαβούσης αἰτίας. Καὶ ὁ ψαλμός φησιν· «Ὅταν λάβω καιρὸν, ἐγὼ εὐθύτητας
κρινῶ.» Εἶτα καὶ μετὰ τὸ ἡττηθῆναι, καὶ μίαν καὶ δευτέραν, μετῆλθεν ὀργὴ ἐπὶ τοὺς
ἠδικηκότας, καὶ νικῶσι δίκαιαν νίκην.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

