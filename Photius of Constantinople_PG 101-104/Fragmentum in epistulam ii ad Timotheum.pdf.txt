Fragmentum in epistulam ii ad Timotheum
637 2 Tim 2,2 Πιστοῖς ἀνθρώποις. ἤγουν ἐπισκόποις, πρεσβυτέροις, οὓς ἔμελλεν ὁ
Τιμόθεος χειροτονεῖν. ∆ιὰ πολλῶν μαρτύρων, τοῦτ' ἔστι νόμου καὶ προφητῶν.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

