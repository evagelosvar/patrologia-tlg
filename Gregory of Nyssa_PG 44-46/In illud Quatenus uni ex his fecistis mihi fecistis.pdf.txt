In illud: Quatenus uni ex his fecistis mihi fecistis
ΓΡΗΓΟΡΙΟΥ ΕΠΙΣΚΟΠΟΥ ΝΥΣΣΗΣ ΕΙΣ ΤΟ ΕΦ' ΟΣΟΝ ΕΝΙ ΤΟΥΤΩΝ
ΕΠΟΙΗΣΑΤΕ ΕΜΟΙ ΕΠΟΙΗΣΑΤΕ
Ἔτι πρὸς τῷ θεάματι τῆς φοβερᾶς τοῦ βασιλέως ἐπιφανείας εἰμί, ἣν
ὑπογράφει τὸ εὐαγγέλιον· ἔτι κατέπτηχεν ἡ ψυχὴ πρὸς τὸν φόβον τῶν εἰρημένων
ἐνατενίζουσα ὡς καθορῶσα τρόπον τινὰ αὐτόν τε τὸν οὐράνιον βασιλέα, καθώς
φησιν ὁ λόγος, ἐπὶ τοῦ θρόνου τῆς δόξης φοβερῶς προκαθήμενον· καὶ τὸν
μεγαλοπρεπῆ θρόνον ἐκεῖνον, ὅστις ποτὲ καὶ ὁ θρόνος ἐστὶν ὁ χωρῶν ἐν ἑαυτῷ τὸν
ἀχώρητον· τάς τε ἀπείρους τῶν ἀγγέλων μυριάδας ἐκείνας ἐν κύκλῳ τὸν βασιλέα
περιεστώσας· αὐτόν τε τὸν μέγαν καὶ φοβερὸν βασιλέα ἐκ τῆς ἀφράστου δόξης ἐπὶ
τὴν ἀνθρωπίνην κατακύ πτοντα φύσιν καὶ πᾶν γένος ἀνθρώπων τῶν ἀφ' οὗ
γεγόνασιν ἄνθρωποι καὶ μέχρι τῆς φοβερᾶς ἐμφανείας ἐκείνης εὑρισκο μένων
ἀθροίζοντά τε πρὸς ἑαυτὸν καὶ κατὰ τὴν ἀξίαν τῶν 9.112 βεβιωμένων ἑκάστῳ τὴν
κρίσιν ἐπάγοντα, τοῖς μὲν δεξιᾷ τῇ προαιρέσει συζήσασι τὰ δεξιά, καθὼς εἴρηται,
χαριζόμενον, τοῖς δὲ σκαιοῖς τε καὶ ἀποβλήτοις τὴν κατάλληλον τοῖς βεβιωμένοις
ἀποκληροῦντα ψῆφον, ἐπιλέγοντα ἑκατέροις, τοῖς μὲν τὴν γλυκεῖαν καὶ ἀγαθὴν
ἐκείνην φωνὴν ὅτι ∆εῦτε οἱ εὐλογημένοι, τοῖς δὲ τὴν φρικώδη καὶ φοβερὰν ἀπειλὴν
ὅτι Πορεύεσθε οἱ κατηραμένοι.
Οὕτω δέ μοι τῆς ψυχῆς πρὸς τὸν τῶν ἀνεγνωσμένων φόβον διατεθείσης, ὡς
πρὸς αὐτοῖς δοκεῖν εἶναι τοῖς πράγμασι καὶ τῶν παρόντων ἐπαισθάνεσθαι μηδενός,
οὐδεμίαν ὁ νοῦς ἄγει σχολὴν πρὸς ἄλλο τι βλέπειν τῶν προκειμένων εἰς ἐξέτασίν τε
καὶ θεωρίαν τῷ λόγῳ· καίτοι γε οὐ μικρὰ ταῦτά ἐστιν οὐδὲ ὀλίγης ζητήσεως ἄξια, τὸ
γνῶναι πῶς ἔρχεται ὁ ἀεὶ παρών. Ἰδοὺ γάρ, φησί, μεθ' ὑμῶν εἰμι πάσας τὰς ἡμέρας·
καὶ εἰ μεθ' ἡμῶν νῦν εἶναι πεπίστευται, πῶς ἥξειν ὡς οὐ παρὼν ἐπαγγέλλεται; εἰ γὰρ
Ἐν αὐτῷ ζῶμέν τε καὶ κινούμεθα καὶ ἐσμέν, καθώς φησιν ὁ ἀπόστολος, οὐδεμία
μηχανὴ τοῦ περιδεδραγμένου τῶν πάντων τοὺς ἐν αὐτῷ κατεχομένους τοπικῶς
ἀποστήσει, ὥστε ἢ νῦν μὴ παρεῖναι τοῖς ἐμπεριεχομένοις ἢ χρόνοις ὕστερον
παρέσεσθαί ποτε προσδοκῆσαι. τίς δὲ καὶ ἡ τοῦ ἀσωμάτου καθέδρα καὶ ἡ τοῦ
ἀχωρήτου ἐν τῷ θρόνῳ περιγραφὴ καὶ πάντα τὰ τοιαῦτα, ὡς μείζονα ἢ κατὰ τὸν
παρόντα καιρόν, ὑπερθήσομαι. ὅπως δ' ἂν μὴ εἰς τὴν τῶν ἀποβλήτων ἀπωσθείημεν
τάξιν, πρὸς 9.113 τοῦτο, καθὼς ἂν οἷός τε ὦ, πρὸς τὸ κοινὸν κέρδος τρέψω τὸν
λόγον. Σφόδρα γάρ, ἀδελφοί, σφόδρα πρὸς τὴν ἀπειλὴν κατεπτόη μαι καὶ οὐκ
ἀρνοῦμαι τῆς ψυχῆς μου τὸ πάθος. ἐβουλόμην δ' ἂν καὶ ὑμᾶς μὴ καταφρονητικῶς
ἔχειν τῶν φόβων· Μακάριος γὰρ ὃς καταπτήσσει πάντα δι' εὐλάβειαν. ὁ δὲ
καταφρονῶν πράγματος καταφρονηθήσεται ὑπ' αὐτοῦ, φησί που τῆς σοφίας ὁ λόγος.
οὐκοῦν πρὸ τῆς ὥρας τῶν κακῶν τὸ μὴ ἐν πείρᾳ τῶν σκυθρωπῶν γενέσθαι
φροντίσωμεν. Τίς δὲ ἡ ἀπαλλαγὴ τῶν φοβερῶν; τὸ ταύτην τοῦ βίου τὴν ὁδὸν
ἑλέσθαι, ἣν νῦν ὑπέδειξεν ἡμῖν ὁ λόγος, τὴν πρόσφα τον ὄντως καὶ ζῶσαν. τίς δὲ ἡ
ὁδός; Ἐπείνων, ἐδίψων, ξένος ἤμην καὶ γυμνὸς καὶ ἀσθενὴς καὶ ἐν φυλακῇ· ἐφ' ὅσον
ἐποιήσατε ἑνὶ τούτων, ἐμοὶ ἐποιήσατε. καὶ διὰ τοῦτο φησὶ ∆εῦτε οἱ εὐλογημένοι τοῦ
πατρός μου. τί διὰ τούτων μανθάνομεν; ὅτι εὐλογία μέν ἐστιν ἡ τῶν ἐντολῶν
ἐπιμέλεια, κατάρα δὲ ἡ πρὸς τὰς ἐντολὰς ῥᾳθυμία. ἀγαπήσωμεν τὴν εὐλογίαν καὶ
φύγωμεν τὴν κατάραν. ἐφ' ἡμῖν γάρ ἐστιν ἑλέσθαι ἢ μὴ ἑλέσθαι κατ' ἐξουσίαν
ἑκάτερον· πρὸς ὅτι γὰρ ἂν τῇ προθυμίᾳ ῥέψωμεν, ἐν ἐκείνῳ ἐσόμεθα. οὐκοῦν
οἰκειωσώ μεθα τὸν τῆς εὐλογίας κύριον τὸν ἑαυτῷ τὴν περὶ τοὺς δεομένους
σπουδὴν λογιζόμενον· καὶ μάλιστα νῦν, ὅτε πολλὴν ἔχει κατὰ τὸν παρόντα βίον
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

ὕλην ἡ ἐντολὴ καὶ πολλοὶ μὲν οἱ τῶν ἀναγκαίων ἐνδεεῖς, πολλοὶ δὲ καὶ αὐτοῦ τοῦ
σώματος ἔχουσιν ἐνδεῶς ἐκ τῆς πονηρᾶς ἀρρωστίας δαπανηθέντες. οὐκοῦν ἐν τῇ
περὶ τούτους σπουδῇ τὴν ἀγαθὴν ἐπαγγελίαν ἑαυτοῖς κατορθώσωμεν· λέγω δὲ
σαφῶς οὑτωσὶ περὶ τῶν ἐκ τῆς χαλεπῆς νόσου διαλωβηθέντων· ὅσῳ γὰρ 9.114
μείζων ἐπὶ τούτων ἡ ἀρρωστία, τοσούτῳ δῆλον ὅτι πλείων ἡ εὐλογία τοῖς τὸ
ἐπίπονον τῆς ἐντολῆς κατορθώσασιν.
Τί οὖν χρὴ πράττειν; μὴ ἀντιβαίνειν τῇ διατάξει τοῦ πνεύματος· αὕτη δέ ἐστι
τὸ μὴ ἀλλοτριοῦσθαι τῶν κοινωνούν των τῆς φύσεως μηδὲ μιμεῖσθαι τοὺς ἐν τῷ
εὐαγγελίῳ κατηγορηθέντας ἐκείνους, τὸν ἱερέα λέγω καὶ τὸν λευΐτην, ἀσυμπαθῶς
παραδραμόντας τὸν ἐλέους δεόμενον ἐκεῖνον, ὅν φησι τὸ διήγημα ὑπὸ τῶν λῃστῶν
ἡμιθανῆ γενέσθαι. εἰ γὰρ ἐκεῖνοι ὑπαίτιοι μὴ ἐπιστραφέντες πρὸς τὰς πληγὰς ἐν
γυμνῷ τῷ σώματι διοιδούσας, πῶς ἡμεῖς ἀνεύθυνοι τοὺς ὑπαιτίους μιμούμενοι;
καίτοι τί τοσοῦτον ἦν ἐπὶ τοῦ τοῖς λῃσταῖς παραπεπτωκότος θεάσασθαι, οἷόν ἐστιν
ἐπὶ τῶν ἑαλωκότων τῷ πάθει τὸ θέαμα; ὁρᾷς ἄνθρωπον διὰ τῆς πονηρᾶς ἀρρωστίας
εἰς τετραπόδου σχῆμα μεταπλασσόμενον, ἀντὶ ὁπλῆς καὶ ὀνύχων ξύλα ταῖς
παλάμαις ὑπολαμβανόμενον, καινὸν ἴχνος ταῖς ἀνθρωπίναις ὁδοῖς ἐπιβάλλοντα· τίς
ἂν ἐκ τοῦ ἴχνους ἐπιγνοίη, ὅτι ἄνθρωπος τοὺς τοιούτους τύπους ἐνεσημήνατο τῇ
πορείᾳ; ἄνθρωπος, ὁ τῷ σχήματι ὄρθιος, ὁ εἰς οὐρανὸν βλέπων, ὁ τὰς χεῖρας πρὸς
τὴν τῶν ἔργων ὑπηρεσίαν παρὰ τῆς φύσεως ἔχων, οὗτος εἰς τὴν γῆν κλίνεται καὶ
τετράποδον γίνεται καὶ μικρὸν ἀποδεῖ τοῦ ἄλογος εἶναι· ἐν δασεῖ καὶ βραγχῶντι τῷ
ἄσθματι βιαίως ἔνδοθεν ἀπὸ τῶν σπλάγχνων ὑποβρυχόμενος, μᾶλλον δέ, εἰ χρὴ
τολμήσαντα εἰπεῖν, καὶ τῶν ἀλόγων γινόμενος ἀθλιώτερος· τὰ μὲν γὰρ 9.115 τὸν
ἀπὸ γενέσεως χαρακτῆρα μέχρι παντὸς ὡς τὰ πολλὰ διεσώσατο, καὶ οὐδὲν τούτων ἔκ
τινος συμφορᾶς τοιαύτης εἰς ἕτερόν τινα χαρακτῆρα μετεκινήθη. οὑτοσὶ δὲ ὥσπερ
ἀλλαχθείσης τῆς φύσεως ἄλλο τι φαίνεται καὶ οὐ τὸ σύνηθες ζῷον· αἱ χεῖρες αὐτῷ
τὴν τῶν ποδῶν χρῆσιν καταλαμβάνουσι, τὰ γόνατα βάσεις γίνονται, αἱ δὲ κατὰ
φύσιν βάσεις καὶ τὰ σφυρὰ ἢ παντελῶς ἀπορρέουσιν ἢ ὥς τι τῶν ἔξωθεν ἐφολκίων
εἰκῆ παρηρτημένα κατὰ τὸ συμβὰν ἐπισύρεται. ἐν τοιούτοις τοίνυν βλέπων τὸν
ἄνθρωπον οὐκ αἰδῇ τὴν συγγένειαν; οὐκ ἐλεεῖς τὸν ὁμόφυλον, ἀλλὰ βδελύσσῃ τὴν
συμφορὰν καὶ μισεῖς τὸν ἱκέτην, ὥσπερ θηρίου τινὸς προσβολὴν τὸν προσεγγισμὸν
ἀποφεύγων; καίτοι ἔδει εὐγνωμόνως ἀναλογίζεσθαι, ὅτι σοῦ τοῦ ἀνθρώπου ἄγγελος
ἅπτεται, καὶ ἀσώματός τε καὶ ἄϋλος ὢν τὸν σαρκὶ καὶ αἵματι ἐγκεκραμένον οὐκ
ἐβδελύξατο. τί δὲ ἀγγέλους λέγω; αὐτὸς ὁ τῶν ἀγγέλων κύριος, ὁ τῆς οὐρανίας
μακαριότητος βασιλεύς, διὰ σὲ ἄνθρωπος γέγονε καὶ τὸ δυσῶδες τοῦτο καὶ ῥυπαρὸν
σαρκίον μετὰ τῆς ἐνδεδε μένης ἐν αὐτῷ ψυχῆς ἑαυτῷ περιέθηκεν, ἵνα τὰ σὰ πάθη διὰ
τῆς ἰδίας ἐπαφῆς ἐξιάσηται. σὺ δὲ ὁ αὐτὸς ὢν κατὰ τὴν φύσιν τῷ ἀρρωστήσαντι
φεύγεις τὸν ὁμογενῆ. μή, ἀδελφέ, μὴ ἀρεσάτω σοι ἡ βουλὴ ἡ κακή. γνῶθι τίς ὢν περὶ
τίνων βουλεύῃ· ὅτι περὶ ἀνθρώπων ἄνθρωπος, οὐδὲν ἰδιάζον ἐν σεαυτῷ παρὰ τὴν
κοινὴν κεκτημένος φύσιν. μὴ προαποφαίνου τοῦ μέλλον τος· ἐν ᾧ γὰρ καταψηφίζῃ
τοῦ πάθους τέως ἐν ἀλλοτρίῳ σώματι φαινομένου, ἀόριστον κατὰ πάσης τῆς φύσεως
ἐκ φέρεις ἀπόφασιν· μετέχεις δὲ καὶ σὺ τῆς φύσεως παραπλησίως τοῖς πᾶσιν. οὐκοῦν
ὡς ὑπὲρ κοινοῦ τοῦ πράγματος ὁ λόγος ἔστω.
∆ιὰ τί τοίνυν οὐδείς σε τῶν φαινομένων οἶκτος εἰσέρχεται; 9.116 ὁρᾷς
ἀνθρώπους νομάδας ὡσπερεὶ τὰ βοσκήματα πρὸς τὴν τῆς τροφῆς χρείαν
ἐσκεδασμένους· ῥάκια περικεκεντημένα, τοῦτο ἡ ἐσθής· βακτηρία ἐν ταῖς χερσί,
τοῦτο τὸ ὅπλον, τοῦτο τὸ ὄχημα· οὐδὲ αὐτὴ δακτύλοις κατεχομένη, ἀλλά τισιν
ὀχάνοις πρὸς τὰς παλάμας συνδεδεμένη· πήρα ῥωγαλέα, καὶ ἄρτου τρύφος, εὐρῶτι
καὶ σηπεδόνι διεφθαρμένον· ἡ ἑστία, ὁ οἶκος, ἡ στιβάς, ἡ κλίνη, τὸ ταμιεῖον, ἡ
τράπεζα, πᾶσα ἡ τοῦ βίου παρασκευὴ ἡ πήρα ἐστίν. εἶτα οὐ λογίζῃ τίς ὁ ἐν τούτοις
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

ὤν; ὅτι ἄνθρωπος, ὁ κατ' εἰκόνα θεοῦ γεγονώς, ὁ κυριεύειν τῆς γῆς τεταγμένος, ὁ
ὑποχείριον τὴν τῶν ἀλόγων ὑπηρεσίαν ἔχων. οὗτος εἰς τοῦτο συμφορᾶς καὶ
μεταβολῆς προῆλθεν, ὥστε ἀμφίβολον τὸ φαινόμενον εἶναι, οὔτε ἀνθρώπου
καθαρῶς οὔτε τινὸς ἄλλου τῶν ζῴων ἐναργῆ φέρων ἐφ' ἑαυτοῦ τὰ γνωρίσματα· ἐὰν
πρὸς ἄνθρωπον εἰκάσῃς, ἀρνεῖται τὴν ἀμορφίαν ὁ χαρακτὴρ ὁ ἀνθρώπινος· ἐὰν πρὸς
τὰ ἄλογα τρέψῃς τὴν εἰκασίαν, οὐδὲ ἐκεῖνα τὴν ὁμοιότητα τοῦ φαινομέ νου
προσίεται. μόνοι τοιοῦτοι πρὸς μόνους ἑαυτοὺς βλέποντες καὶ μετ' ἀλλήλων
ἀγελαζόμενοι διὰ τὴν ὁμοιοτροπίαν τοῦ πάθους· οἱ τοῖς μὲν ἄλλοις πᾶσι βδελυκτοὶ
γινόμενοι, ἀλλή λους δὲ ὑπ' ἀνάγκης οὐ βδελυσσόμενοι· πανταχόθεν γὰρ
ἐξειργόμενοι ἴδιος γίνονται δῆμος, πρὸς ἀλλήλους συρ ρέοντες. ὁρᾷς τοὺς ἀτερπεῖς
χορευτάς, τὸν γοερὸν καὶ κατεστυγνασμένον τοῦτον χορόν; πῶς ἐμπομπεύουσι τοῖς
ἑαυτῶν δυστυχήμασιν; πῶς θεατρίζουσιν ἐν ἑαυτοῖς ἀσχημο νοῦσαν τὴν φύσιν,
ὥσπερ τινὲς θαυματοποιοὶ τὰς πολυτρόπους ἀρρωστίας τοῖς συνιοῦσιν
ἐπιδεικνύμενοι; ποιηταὶ γοερῶν μελῳδημάτων, ἐφευρεταὶ σκυθρωπῶν διηγημάτων,
μελο 9.117 ποιοὶ τῶν πονηρῶν ἐκείνων ᾀσμάτων, τραγῳδοὶ τῆς καινῆς ταύτης καὶ
δυστυχοῦς τραγῳδίας, οὐκ ἀλλοτρίοις τραγῳδή μασι πρὸς τὰ πάθη χρώμενοι, ἀλλὰ
διὰ τῶν οἰκείων κακῶν τὴν σκηνὴν πληροῦντες. οἷα τὰ σχήματα· οἷα τὰ διηγήματα.
τί δὲ λεγόντων ἀκούομεν; πῶς παρὰ τῶν γεννησαμένων ἀπώσθησαν ἐπ' οὐδενὶ
ἀδικήματι; πῶς ἀπελαύνονται μὲν πόλεων, ἀπελαύνονται δὲ συλλόγων κοινῶν,
ἑορτῶν τε καὶ πανηγύρεων, ὥσπερ ἀνδροφόνοι τινὲς ἢ πατραλοῖαι τῇ ἀειφυγίᾳ
κατάκριτοι, μᾶλλον δὲ κἀκείνων παρὰ πολὺ δυστυχέστεροι· εἴγε τοῖς ἀνδροφόνοις
μὲν ἔξεστιν ἀλλαχοῦ μεταστᾶσι μετ' ἀνθρώπων ζῆν, οὗτοι δὲ μόνοι τῶν πάντων
ἁπανταχόθεν ἀπείργονται ὡς κοινοί τινες ἀποδεδειγμένοι πολέμιοι, οὐ στέγης μιᾶς,
οὐ τραπέζης κοινῆς, οὐ χρήσεως σκευῶν ἀξιούμενοι. καὶ οὔπω τοῦτο δεινόν. ἀλλ'
οὐδὲ πηγαὶ βρύουσι τούτοις πρὸς τοὺς ἄλλους ἀνθρώπους κοιναὶ οὐδὲ ποταμοὶ
πιστεύονται μηδὲν ἐφέλκεσθαι τοῦ τῆς ἀρρωστίας μολύσματος. ἐὰν κύων λάψῃ τοῦ
ὕδατος τῇ αἱμοβόρῳ γλώσσῃ, οὐκ ἐνομίσθη βδελυκτὸν διὰ τὸ θηρίον τὸ ὕδωρ· ἐὰν ὁ
ἄρρω στος προσεγγίσῃ τῷ ὕδατι, εὐθὺς ἀπεκηρύχθη καὶ τὸ ὕδωρ διὰ τὸν ἄνθρωπον.
ταῦτα διεξέρχονται, ταῦτα ὀδύρονται, διὰ τοῦτο ῥίπτουσιν ἑαυτοὺς κατ' ἀνάγκην
πρὸ τῶν ἀνθρώπων οἱ δείλαιοι, ἱκέται παντὸς τοῦ παρατυγχάνοντος γινόμενοι.
πολλάκις ἐπε δάκρυσα τῷ σκυθρωπῷ τούτῳ θεάματι, πολλάκις πρὸς αὐτὴν τὴν
φύσιν ἀπεδυσπέτησα καὶ νῦν πρὸς τῇ μνήμῃ συγχέομαι. εἶδον πάθος ἐλεεινόν, εἶδον
θέαμα δακρύων πλῆρες· πρόκεινται κατὰ τὰς ὁδοὺς τῶν παριόντων ἀνθρώπων
ἄνθρωποι νεκροί· μᾶλλον δὲ οὐκέτι ἄνθρωποι, ἀλλὰ τῶν ποτε ἀνθρώπων 9.118
ἄθλια λείψανα, συμβόλων τινῶν καὶ γνωρισμάτων εἰς τὸ πιστευθῆναι δεόμενοι. οὐ
γὰρ ἔχουσιν ἀπὸ τῶν τῆς φύσεως χαρακτήρων ἐπιγινώσκεσθαι ἄνθρωποι· μόνοι τῶν
πάντων ἑαυτοὺς μισοῦντες, μόνοι τὴν γενέθλιον ἡμέραν διὰ κατάρας ἄγοντες·
μισοῦσι γὰρ εἰκότως τὴν ἡμέραν ἐκείνην τὴν ἄρξασαν αὐτοῖς τῆς τοιαύτης ζωῆς.
ἄνθρωποι, οἳ καὶ ὀνομάζειν ἑαυτοὺς ἀπὸ τῆς κοινῆς προσηγορίας αἰσχύνονται, ὡς ἂν
μὴ τῇ κοινωνίᾳ τοῦ ὀνόματος τὴν κοινὴν φύσιν δι' ἑαυτῶν καθυβρίσειαν· οἰμωγαῖς
ἀεὶ συζῶντες, ἄπαυστον τῶν θρήνων τὴν ὑπόθεσιν ἔχοντες. ἕως γὰρ ἂν ἑαυτοὺς
βλέπωσιν, ἀεὶ τὰς τοῦ θρηνεῖν ἔχουσιν ἀφορμάς, ἀποροῦντες ὅτι καὶ μᾶλλον
ὀδύρονται, τὰ μηκέτι ὄντα τοῦ σώματος ἢ τὰ λει πόμενα· ὅσα προανάλωσεν ἡ νόσος
ἢ ὅσα τῇ νόσῳ λείπεται· ὅτι βλέπουσιν ἐφ' ἑαυτῶν τοιαῦτα ἢ ὅτι οὐδὲ βλέπειν
ἰσχύου σιν ἀμαυρωθείσης ἐν τῷ πάθει τῆς ὄψεως· ὅτι τοιαῦτα ἔχουσι περὶ ἑαυτῶν
διηγεῖσθαι ἢ ὅτι οὐδὲ διηγήσασθαι τὰ πάθη δύνανται τὴν φωνὴν ἐκ τοῦ πάθους
ἀφῃρημένοι· ὅτι τοιαύτῃ χρῶνται τροφῇ ἢ ὅτι οὐδὲ ἐκείνην εὐκόλως <παρα
δέχονται> ἐκ νόσου τῇ διαφθορᾷ τῶν περὶ τὸ στόμα μορίων ἐμποδιζούσης τῆς
βρώσεως· ὅτι ἐν αἰσθήσει τὰ τῶν νεκρῶν δυστυχοῦσιν ἢ ὅτι καὶ τῶν αἰσθήσεων
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

3

αὐτῶν προσαφῄρηνται. ποῦ γὰρ παρ' ἐκείνοις ἡ ὅρασις; ποῦ δὲ ἡ ὄσφρησις; ποῦ δὲ ἡ
ἁφή; ποῦ δὲ τὰ λοιπὰ τῶν αἰσθητηρίων, ἃ κατ' ὀλίγον νεμομένου τοῦ πάθους ἡ
σηπεδὼν ἐπιβόσκεται;
∆ιὰ ταῦτα κατὰ πάντας ἀλῶνται τοὺς τόπους, καθάπερ τὰ ἄλογα πρὸς τὴν
ἀφθονωτέραν νομὴν μετεξανιστάμενοι· ἐφόδιον τῆς κατὰ τὴν τροφὴν ἐμπορίας τὴν
συμφορὰν περιφέροντες καὶ πᾶσι 9.119 τὴν νόσον ἀνθ' ἱκετηρίας προτείνοντες καὶ
τῶν χειραγωγούν των δι' ἀρρωστίαν δεόμενοι καὶ ἀλλήλους διὰ τὴν ἀπορίαν
ὑποστηρίζοντες. καθ' ἑαυτὸν γὰρ ἕκαστος ἀσθενοῦντες ἔ ρεισμα τῷ ἑτέρῳ γίνονται
τοῖς ἀλλήλων μέλεσιν ἀντὶ τῶν λειπόντων χρώμενοι· οὐ γὰρ καθ' ἕνα φαίνονται,
ἀλλά τι ἔχει καὶ ἡ ταλαιπωρία σοφὸν εἰς τὴν τοῦ βίου ἐπίνοιαν, τὸ μετ' ἀλλήλων
αὐτοὺς θέλειν ὁρᾶσθαι. ὄντες γὰρ καὶ καθ' ἑαυτοὺς ἕκαστος ἐλεεινοί, ὡς ἂν μᾶλλον
συμπαθεστέρους τοὺς ἀνθρώπους ποιήσειαν, προσθήκη τοῦ πάθους ἀλλήλοις
γίνονται, τὸν πονηρὸν τῷ κοινῷ συνεισφέροντες ἔρανον, ἄλλος δι' ἄλλης τινὸς
συμφορᾶς ἔλεον ἑαυτοῖς ἐρανίζοντες. ὁ μὲν χεῖρας ἠκρωτηριασμένας προτείνων,
ἄλλος γαστέρα προδεικνύων φλεγμαίνουσαν καὶ πρόσωπον ἠχρειωμένον ἕτερος καὶ
ἄλλος τὸ σκέλος ἀποσηπόμενον, ὁ δὲ ὃ ἂν τύχῃ τοῦ σώματος ἀπογυμνώσας ἐν
ἐκείνῳ τὸ πάθος ἔδειξεν.
Τί οὖν; ἆρ' ἐξαρκεῖ τοσοῦτον πρὸς τὸ μηδὲν εἰς τὸν τῆς φύσεως νόμον
ἐξαμαρτεῖν, αὐτὸ τοῦτο τὸ τραγῳδεῖν τὰ πάθη τῆς φύσεως καὶ λόγῳ διασκευάζειν
τὴν νόσον καὶ πρὸς τὴν μνήμην παθαίνεσθαι; ἤ τινος ἡμῖν καὶ ἔργου χρεία τοῦ
ἐπιδεικνυμένου πρὸς τοὺς τοιούτους τὸ συμπαθὲς καὶ φιλάλληλον; ὃ γάρ εἰσιν αἱ
σκιαγραφίαι πρὸς τὰ ὄντως πράγματα, τοῦτο οἱ λόγοι διεζευγμένοι τῶν ἔργων. οὐ
γὰρ ἐν τῷ λέγειν φησὶν ὁ κύριος εἶναι τὴν σωτηρίαν, ἀλλ' ἐν τῷ ποιεῖν τὰ ἔργα τῆς
σωτηρίας. οὐκοῦν ἀντιληπτέον ἡμῖν αὐτοῖς τῆς ὑπὲρ αὐτῶν ἐντολῆς. μὴ γὰρ δὴ
τοῦτο λεγέτω τις, ὡς ἱκανόν ἐστι πόρρω που τῆς ἡμετέρας ζωῆς ἐπί τινος ἐσχατιᾶς
ἀποικισθεῖσι τὴν τροφὴν χορηγεῖν· ἡ γὰρ τοιαύτη γνώμη οὐχὶ ἐλέου τινὰ καὶ
συμπαθείας ἐπίδειξιν ἔχει, ἀλλά τις εὐπρόσωπός ἐστιν ἐπίνοια τοῦ παντελῶς τοὺς
ἀνθρώπους 9.120 τῆς ἡμετέρας ἐξορισθῆναι ζωῆς. εἶτα οὐκ αἰσχυνόμεθα τὸν ἡμῶν
αὐτῶν βίον, εἰ χοίρους καὶ κύνας ὁμωροφίους ποιοῦ μεθα; οὐδὲ τῆς κοίτης ἑαυτοῦ
πολλάκις ἀπείργει ὁ κυνηγέτης τὸν σκύλακα. οἶδε καὶ φιλήματι τὸν μόσχον ὁ
γεωργὸς δεξιώσασθαι. καὶ οὔπω τοῦτο πολύ, ἀλλὰ ταῖς ἰδίαις χερσὶ νίπτει τοὺς
πόδας τοῦ ὄνου ὁ ὁδοιπόρος καὶ τοῖς τραύμασιν ἐπιβάλλει τὴν χεῖρα καὶ ὑποκαθαίρει
τὴν κόπρον καὶ τῆς εὐνῆς ἐπιμελεῖται· τὸν δὲ ὁμογενῆ καὶ ὁμόφυλον ἀτιμότερον καὶ
τῶν ἀλόγων αὐτῶν ποιησόμεθα; μὴ ταῦτα, μή, ἀδελφοί· μὴ κυρωθήτω αὕτη κατὰ
τῶν ἀνθρώπων ἡ ψῆφος.
Μνησθῆναι προσήκει τίνες ὄντες περὶ τίνων βουλευόμεθα· ὅτι περὶ
ἀνθρώπων ἄνθρωποι, οὐδὲν ἰδιάζον παρὰ τὴν κοινὴν φύσιν ἐφ' ἑαυτῶν ἔχοντες. μία
πᾶσιν ἐπὶ τὸν βίον ἡ εἴσοδος· εἷς ὁ τῆς ζωῆς ἅπασι τρόπος, βρῶσις καὶ πόσις· ἡ τοῦ
ζῆν ἐνέργεια ὁμοιότροπος· ἡ κατασκευὴ τῶν σωμάτων μία καὶ ἡ καταστροφὴ τοῦ
βίου· ἅπαν τὸ σύνθετον εἰς διάλυσιν ἔρχεται· οὐδὲν τῶν συνεστώτων παγίαν ἔχει
τὴν σύστασιν, πομφόλυγος δίκην ἐπ' ὀλίγον ἡμῶν τῷ πνεύματι περιταθέντος τοῦ
σώμα τος· εἶτα ἀπέσβημεν οὐδὲν ἴχνος τοῦ προσκαίρου τούτου φυσήματος
καταλιπόντες τῷ βίῳ. ἐν στήλαις καὶ λίθοις καὶ ἐπιγραφαῖς τὰ μνημόσυνα καὶ οὐδὲ
ταῦτα εἰς ἀεὶ διαρ κοῦντα. ταῦτα τοίνυν ἐπὶ σεαυτοῦ λογιζόμενος Μὴ ὑψηλο φρόνει,
καθώς φησιν ὁ ἀπόστολος, ἀλλὰ φοβοῦ· ἄδηλον, εἰ μὴ σαυτῷ νομοθετεῖς τὴν
ἀπήνειαν. φεύγεις, εἰπέ μοι, τὸν ἀρρωστήσαντα; τί ἐγκαλεῖν ἔχων; ὅτι διέφθαρται τὸ
ὑγρὸν ἐν ἐκείνῳ καί τις σηπεδονώδης χυμὸς ἐγκατεσπάρη τῷ αἵματι τῆς μελαίνης
χολῆς τῷ ὑγρῷ παρεγχεθείσης; ταῦτα γὰρ ἔστιν ἰατρῶν ἀκούειν φυσιολογούντων τὰ
πάθη. τί οὖν ἀδικεῖ ὁ ἄνθρωπος, εἰ ῥευστὴ οὐσία οὖσα ἡ φύσις καὶ ἄστατος 9.121 ἐπὶ
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

4

τὸ εἶδος τοῦτο τῆς ἀρρωστίας παρώλισθεν; σὺ δὲ οὐχ ὁρᾷς καὶ ἐπὶ τῶν ὑγιαινόντων
σωμάτων, ὅτι τἄλλα τις εὐπαθῶν φλυκτίδι πολλάκις ἢ ἀνθρακίῳ ἤ τινι τοιούτῳ
συνηνέχθη πάθει ἐκθερμανθείσης πέρα τοῦ δέοντος περὶ τὸ μέρος τῆς ὑγρᾶς οὐσίας
καὶ διὰ τοῦτο φλεγμονήν τε καὶ ἔρευθος καί τινα πυαλώδη διαφθορὰν παθούσης; τί
οὖν; μὴ πολεμοῦμεν τῷ ἀρρωστήσαντι τῆς σαρκὸς μέρει; τοὐναντίον μὲν οὖν· ἅπαν
τὸ ὑγιαῖνον τοῦ σώματος πρὸς τὴν τοῦ νενοσηκότος τρέπομεν θεραπείαν. οὔκουν τὸ
πάθος βδελυκτόν. ἢ γὰρ ἂν καὶ ἐφ' ἡμῶν αὐτῶν τοῦ μέρους πονέσαντος τὸ ὑγιαῖνον
ἠλλοτριοῦτο τῆς θεραπείας τοῦ κάμνοντος. ἀλλὰ τίς ἐστιν αἰτία ἡ τῶν τοιούτων
ἡμᾶς ἀφιστῶσα; τίς αὕτη; τὸ μὴ φοβεῖσθαι τὴν ἀπειλὴν τοῦ εἰπόντος ὅτι Πορεύεσθε
ἀπ' ἐμοῦ εἰς τὸ πῦρ τὸ αἰώνιον· ἐφ' ὅσον γὰρ οὐκ ἐποιήσατε ἑνὶ τούτων, φησίν, οὐδὲ
ἐμοὶ ἐποιήσατε. εἰ γὰρ ταῦτα οὕτως ἔχειν ᾤοντο, οὐκ ἂν τοιαύτην ἔσχον περὶ τῶν
ἀρρωστούντων γνώμην, ὥστε ἀπωθεῖσθαι ἀφ' ἑαυτῶν καὶ μολυσμὸν οἴεσθαι τοῦ
ἡμετέρου βίου τὴν περὶ τοὺς ταλαιπωρουμένους σπουδήν. οὐκοῦν εἰ πιστὸν
ἡγούμεθα τὸν ἐπαγγειλάμενον, ἀντιληπτέον ἂν εἴη τῶν ἐντολῶν, ὧν ἄνευ οὐκ ἔστι
τῶν ἐλπιζομένων ἀξιωθῆναι. ξένος καὶ γυμνὸς καὶ τροφῆς ἐνδεής, ἀσθενῶν καὶ
κατάκλειστος καὶ πάντα σοι οὗτός ἐστιν, ὅσα ἐν τῷ εὐαγγελίῳ προείρηται. ἀλήτης
καὶ γυμνὸς περιέρχεται, νοσῶν δὲ καὶ τῶν ἀναγκαίων ἐπιδεὴς διὰ τὴν
ἐπακολουθοῦσαν τῷ πάθει πενίαν. ὁ γὰρ μήτε τι τῶν οἴκοθεν ἔχων μήτε μισθωτὸς
γενέσθαι δυνάμενος ἐξ ἀνάγκης πάντως ἄπορος τῶν πρὸς τὴν ζωὴν ἐπιτηδείων
ἐστίν, δεσμώτης δὲ ὑπὸ τῆς ἀρρωστίας πεπεδημένος. οὐκοῦν ὅλον τὸ πλήρωμα τῶν
9.122 ἐντολῶν ἐν τούτοις ἔχεις πληρούμενον καὶ αὐτὸν τὸν τῶν ὅλων κύριον
ὑπόχρεών σοι διὰ τῆς εἰς τοῦτον φιλανθρωπίας γενόμενον. τί οὖν φιλονεικεῖς κατὰ
τῆς ἰδίας ζωῆς; τὸ γὰρ σύνοικον ἑαυτῷ τὸν θεὸν τῶν ὅλων μὴ ἐθελῆσαι ἔχειν οὐδὲν
ἄλλο ἐστὶν ἢ καθ' ἑαυτοῦ τινα ἰσχυρὸν γενέσθαι. ὡς γὰρ διὰ τῆς ἐντολῆς
εἰσοικίζεται, οὕτω διὰ τῆς ἀπηνείας ἀποχωρίζε ται.
Ἄρατε, φησί, τὸν ζυγόν μου ἐφ' ὑμᾶς· ζυγὸν δὲ λέγει τὴν τῶν ἐντολῶν
ἐργασίαν. Ὑπακούσωμεν τῷ κελεύοντι· γενώμεθα τοῦ Χριστοῦ ὑποζύγιον, ταῖς
ζεύγλαις τῆς ἀγάπης ἑαυτοὺς ἐνδήσαντες. μὴ ἀποσεισώμεθα τὸν τοιοῦτον ζυγόν·
χρηστός ἐστιν, ἐλαφρός ἐστιν, οὐ τρίβει τὸν αὐχένα τοῦ ὑπελθόντος, ἀλλὰ λεαίνει.
Σπείρωμεν ἐπ' εὐλογίαις, φησὶν ὁ ἀπόστολος, ἵνα καὶ ἐπ' εὐλογίαις θερίσωμεν.
πολύχους ὁ στάχυς ἐκ τῆς τοιαύτης ἀναβλαστήσει σπορᾶς· βαθὺ τῶν ἐντολῶν τοῦ
κυρίου τὸ λήιον· ὑψηλὰ τὰ τῆς εὐλογίας γεννήματα. βούλει μαθεῖν εἰς ὅσον
ἀνατείνεται ὕψος τῶν γεννημάτων ἡ αὔξησις; αὐτῶν τῶν οὐρανίων ὑψωμάτων
ἐφάπτεται· ὅσα γὰρ ἂν τούτοις ποιήσῃς, τοῖς οὐρανίοις θησαυροῖς καρποφορεῖς. μὴ
ἀπελ πίσῃς τῶν λεγομένων μηδὲ εὐκαταφρόνητον λογίσῃ τῶν τοιούτων τὴν φιλίαν.
ἡ χεὶρ ἠκρωτηρίασται, ἀλλ' οὐκ ἀσθενεῖ πρὸς συμμαχίαν· ὁ ποὺς ἠχρείωται, ἀλλὰ
πρὸς τὸν θεὸν οὐ κωλύεται τρέχειν· ὁ ὀφθαλμὸς ἐξερρύη, ἀλλὰ βλέπει διὰ τῆς ψυχῆς
τὰ ἀγαθὰ τὰ ἀθέατα. μὴ τοίνυν τὴν ἀμορφίαν κατανόει τοῦ σώματος. μικρὸν
ἀνάμεινον καὶ ὄψει τὸ παντὸς θαύματος ἀπιστότερον· οὐ γὰρ ὅσα περὶ τὴν ῥευστὴν
φύσιν συμβέβηκε, ταῦτα καὶ εἰς ἀεὶ παραμένει. ἀλλ' ὅταν ἐλευθερω θῇ τῆς πρὸς τὸ
φθαρτόν τε καὶ γήϊνον συμπλοκῆς ἡ ψυχή, τότε τῷ ἰδίῳ κάλλει ἐνωραίζεται.
τεκμήριον δέ· οὐκ 9.123 ἐβδελύξατο τοῦ πτωχοῦ τὴν χεῖρα μετὰ τὸν βίον τοῦτον ὁ
τρυφητὴς ἐκεῖνος ὁ πλουσίος, ἀλλ' ἠξίου γενέσθαι αὐτῷ τόν ποτε διεφθορότα τοῦ
πτωχοῦ δάκτυλον τῆς τοῦ ὕδατος σταγόνος διάκονον, ἐπιθυμῶν αὐτῇ τῇ γλώσσῃ
τὴν περὶ τὸν δάκτυλον ἰκμάδα τοῦ πτωχοῦ περιλάψαι· οὐκ ἂν τούτου ἐπιθυμήσας, εἰ
τὴν ἐκ τοῦ σώματος ἀηδίαν τῷ χαρακτῆρι τῆς ψυχῆς ἐνεώρα. πόσα εἰκὸς ἦν μάτην
παλινῳδεῖν ἐν τῇ μεταβολῇ τοῦ βίου τὸν πλούσιον; πόσα μακαρίζειν τῆς ἐν τῇ ζωῇ
ταύτῃ δυσκληρίας τὸν πένητα; πόσα καταμέμφεσθαι τὴν ἑαυτοῦ λῆξιν ὡς ἐπὶ κακῷ
τῆς ψυχῆς ἀποκληρωθεῖσαν τῷ πλούτῳ; εἰ δὲ δὴ καὶ ἀναβιῶναι πάλιν ἐξῆν, ἐν τίσιν
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

5

ἂν ἐδέξατο πάλιν γενέσθαι; ἆρ' ἐν τοῖς κατὰ τὸν βίον τοῦτον εὐδαιμονοῦσιν ἢ ἐν
τοῖς δυσκληροῦσιν; ἀλλ' οὐκ ἄδηλον ὅτι προετίμησεν ἂν τῶν δυσημερούντων τὸν
κλῆρον. ἀξιοῖ γὰρ γενέσθαι μηνυτὴν τοῖς ἀδελφοῖς ἐκ τῶν νεκρῶν τινα, ὡς ἂν μὴ
κἀκεῖνοι τῇ ὑπερηφανίᾳ τοῦ πλούτου βλαβέντες ἐν τῇ τρυφῇ τῆς σαρκὸς ἐπὶ τὸ αὐτὸ
χάσμα τοῦ ᾅδου κατενεχθῶσι διὰ τοῦ λείου τῆς ἡδονῆς ὀλισθήσαντες. τί οὖν οὐ
σωφρονιζόμεθα τοῖς τοιούτοις τῶν διηγημάτων; τί οὐκ ἐμπορευόμεθα τὴν καλὴν
ἐμπορίαν, ἣν συμβουλεύει ὁ θεῖος ἀπόστολος;
Τὸ ὑμῶν περίσσευμα, φησίν, εἰς τὸ ἐκείνων ὑστέρημα, ὡς ἂν τὸ περισσὸν τῆς
ἐκείνων ἀνέσεως ἐν τῷ μετὰ ταῦτα βίῳ καὶ ἡμῖν πρὸς σωτηρίαν ἀρκέσειεν. εἰ τοίνυν
βουλόμεθα λαβεῖν τι χρηστόν, προλαβόντες παράσχωμεν· εἰ ἀνεθῆναι μετὰ ταῦτα,
νῦν ἀναπαύσωμεν. εἰ δεχθῆναι παρ' αὐτῶν εἰς τὰς αἰωνίους σκηνάς, νῦν αὐτοὺς ἐν
ταῖς ἡμετέραις καταδεξώμεθα· εἰ θεραπευθῆναι τῶν ἁμαρτιῶν τὰ τραύματα, τοῦτο
καὶ αὐτοὶ τοῖς σώμασι τῶν κεκακωμένων ποιήσωμεν. Μακάριοι γὰρ οἱ ἐλεήμονες,
ὅτι αὐτοὶ ἐλεηθήσονται. 9.124 Ἀλλ' ἴσως ἐρεῖ τις ἀγαθὴν μὲν εἰς ὕστερον εἶναι τὴν
ἐντολήν· νυνὶ δὲ μετάδοσίν τινα καὶ κοινωνίαν τοῦ πάθους διευλαβεῖται καὶ διὰ τὸ
μὴ παθεῖν τι τῶν ἀβουλήτων φεύγειν οἴεται χρῆναι τὸν προσεγγισμὸν τῶν
τοιούτων. λόγοι ταῦτα καὶ προφάσεις καὶ πλάσματα καὶ τῆς περὶ τὰς ἐντολὰς τοῦ
θεοῦ ῥᾳθυμίας εὐπρόσωπά τινα προκαλύμματα. τὸ δ' ἀληθὲς οὐχ οὕτως ἔχει· οὐδεὶς
γὰρ ὕπεστι φόβος τῇ τῆς ἐντολῆς ἐργασίᾳ. μηδεὶς κακῷ τὸ κακὸν θεραπευέτω.
πόσους γὰρ ἔστιν ἰδεῖν ἐκ νεότητος καὶ μέχρι γήρως ταῖς θεραπείαις τούτων
ἀποσχολάζοντας καὶ οὐδέν τι τῆς κατὰ φύσιν εὐεξίας τοῦ σώματος διὰ τῆς τοιαύτης
σπουδῆς ἀμαυρώσαντας; οὐδὲ γὰρ εἰκός ἐστι γενέσθαι ταῦτα. ἀλλ' ἐπειδή τινα τῶν
νοσημάτων, οἷον αἱ λοιμώδεις ἐπιφοραὶ καὶ ὅσα τοιαῦτα τῆς ἔξωθεν αἰτίας
ἠρτημένα, ὅταν ἐκ διαφθορᾶς ἀέρος ἢ ὕδατος γίνηται, ὕποπτα τοῖς πολλοῖς ἐστιν, ὡς
ἐκ τῶν προεαλωκότων καὶ πρὸς τοὺς προσεγγίζοντας διαβαίνοντα, (οὐδὲ ἐκεῖ τοῦ
πάθους, ὡς οἶμαι, τῷ ὑγιαίνοντι τὴν ἀρρωστίαν ἐκ διαδόσεως ἐμποιοῦντος, ἀλλὰ τῆς
κοινῆς ἐπιφορᾶς τὴν ὁμοιότητα τοῦ ἀρρωστήματος ἐπαγούσης) ἔσχεν αἰτίαν ἡ νόσος
ὡς ἐκ τῶν προεαλωκότων καὶ εἰς τοὺς λοιποὺς διαβαίνουσα. ἐνταῦθα δὲ ἔνδοθεν
συνισταμένης τῆς τοῦ τοιούτου πάθους κατασκευῆς καί τινα τοῦ αἵματος ἐκ τῆς
παρεγχύσεως τῶν φθοροποιῶν χυμῶν διαφθορὰν ὑπομένοντος ἐν τῷ κάμνοντι τὸ
πάθος περιορίζεται· καὶ ὅτι ταῦτα οὕτως ἔχει, πάρεστι καὶ ἀπὸ τούτου μαθεῖν· μή τις
ἀπὸ τῶν εὐεκτούντων τοῖς ἀρρωστοῦσι κοινωνία τῆς κρείττονος ἕξεως γίνεται, κἂν
σφόδρα λιπαρῶς τῇ θεραπείᾳ προσμένωσιν; οὐκ ἔστι ταῦτα. οὕτως οὖν εἰκὸς καὶ τὸ
ἔμπαλιν μηδὲ ἀπὸ τῶν ἀσθενούντων πρὸς τοὺς εὐεκτοῦντας μεταβαίνειν τὸ πάθος.
εἰ τοίνυν τὸ μὲν 9.125 κέρδος τῆς ἐντολῆς τοσοῦτον, ὥστε βασιλείαν οὐρανῶν δι'
αὐτῆς ἑτοιμάζεσθαι, ζημία δὲ πρόσεστιν οὐδεμία τῷ σώματι τοῦ θεραπεύοντος, τί τὸ
κωλύον ἐστὶ λοιπὸν ἐνεργὸν εἶναι τὴν ἐντολὴν τῆς ἀγάπης;
Ἀλλ' ἐπίπονον εἶναι λέγεις τὸ βιάσασθαι φυσικῶς τὰ τοιαῦτα μυσαττομένην
τῶν πολλῶν τὴν ἕξιν; καλῶς· συντίθεμαι τῷ λόγῳ, ὅτι ἐπίπονον. τί δὲ ἄλλο τῶν
κατ' ἀρετὴν τελουμένων ἄπονον δείξεις; πολλοὺς ἱδρῶτας καὶ πόνους τῶν
οὐρανίων ἐλπίδων ὁ θεῖος προσέταξε νόμος καὶ δυσπόρευτον τὴν ἐπὶ τὴν ζωὴν ὁδὸν
τοῖς ἀνθρώποις ὑπέδειξε τοῖς ἐπιπονωτέροις καὶ τραχυτέροις τῶν ἐπιτηδευμάτων
ἁπανταχόθεν ἀποστενώσας. Στενὴ γὰρ καὶ τεθλιμμένη, φησίν, ἡ ὁδὸς ἡ ἀπάγουσα εἰς
τὴν ζωήν. τί οὖν; διὰ τοῦτο τῆς ἐλπίδος ἐκείνης τῶν ἀγαθῶν ἀμελήσομεν, ὅτι μὴ
μετὰ ῥᾳστώνης ἔξεστι κτήσασθαι; ἐρωτήσωμεν τὴν νεότητα, εἰ μὴ ἐπίπονος ἡ
σωφροσύνη δοκεῖ ἢ τὸ ἀνέδην ταῖς ἐπιθυμίαις κεχρῆσθαι τῆς ἐγκρατοῦς διαγωγῆς
μὴ πολὺ ποθεινότερον. ἆρ' οὖν διὰ τοῦτο τοῦ μὲν ὅτι ἡδύ τε καὶ εὐπετὲς ἀνθεξόμεθα,
πρὸς δὲ τὸ τραχὺ τῆς ἀρετῆς ἀποκνήσομεν; οὐ ταῦτα δοκεῖ τῷ τῆς ζωῆς νομοθέτῃ τῷ
τὴν πλατεῖαν καὶ κατωφερῆ καὶ εὐρύχωρον ὁδὸν κατὰ τὸν βίον κωλύσαντι.
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

6

Εἰσέλθετε γάρ, φησί, διὰ τῆς στενῆς καὶ τεθλιμμένης πύλης. οὐκοῦν καὶ ἐνταῦθα ἓν
τῶν διὰ πόνου κατορθουμένων καὶ τοῦτο νομίσωμεν, τὸ ἀγαγεῖν εἰς συνήθειαν
ἀπεξενωμένην τοῦ βίου τὴν ἐντολήν, καὶ τὴν φυσικὴν τῶν ὑγιαινόντων
ἀποστροφὴν τῇ ἐπιμονῇ τοῦ πράγματος θεραπεύσωμεν. δεινὴ γάρ ἐστιν ἡ συνήθεια
καὶ τοῖς δυσκολωτέροις εἶναι δοκοῦσιν ἡδονήν τινα 9.126 διὰ τῆς ἐπιμονῆς
ἐνεργάσασθαι. μὴ τοίνυν λεγέτω τις, ὡς ἐπίπονον, ἀλλ' ὅτι ἐπωφελὲς τοῖς μετιοῦσι
τὸ κατορθούμενον. καὶ ἐπειδὴ μέγα τὸ κέρδος, ὑπεροπτέον τοῦ πόνου διὰ τὸ κέρδος.
ἡδὺ δὲ τῷ χρόνῳ τὸ τέως ἐπίπονον διὰ τῆς συνηθείας γενήσεται.
Εἰ δὲ δεῖ καὶ τοῦτο προσθεῖναι τοῖς εἰρημένοις, ὅτι καὶ παρ' αὐτὸν τοῦτον τὸν
βίον ἐπωφελής ἐστι τοῖς ὑγιαίνουσιν ἡ πρὸς τοὺς ἀτυχοῦντας συμπάθεια· καλὸς γάρ
ἐστι τοῖς νοῦν ἔχουσιν ἔρανος ἐλέου ἐν ταῖς ἑτέρων δυσπραγίαις προαποκείμενος.
ἐπειδὴ γὰρ μιᾷ φύσει διοικεῖται πᾶν τὸ ἀνθρώπινον καὶ οὐδὲν ἔχει οὐδεὶς τῆς
διηνεκοῦς εὐπραγίας βέβαιόν τι παρ' ἑαυτῷ τὸ ἐνέχυρον, διὰ παντὸς προσήκει
μεμνῆσθαι τοῦ εὐαγγελικοῦ παραγγέλματος τοῦ συμβουλεύοντος, ἅπερ ἂν θέλωμεν
ἵνα ποιῶσιν ἡμῖν οἱ ἄνθρωποι, ταῦτα ποιεῖν. ἕως τοίνυν εὐπλοεῖς, ὄρεξον χεῖρα τῷ
ναυαγήσαντι. κοινὴ ἡ θάλασσα, κοινὸν τὸ κλυδώνιον, κοινὴ τῶν κυμάτων ἡ ταραχή·
ὕφαλοι καὶ σπιλάδες καὶ σκόπελοι καὶ τὰ λοιπὰ τῆς τοῦ βίου ναυαγίας κακὰ ἴσον
παρέχει τοῖς ναυτιλλομένοις τὸν φόβον. ἕως ἀπαθὴς εἶ, ἕως ἀκινδύνως διαπλέεις
τοῦ βίου τὴν θάλασσαν, μὴ παρέλθῃς ἀνηλεῶς τὸν προσπταίσαντα. τίς ἐγγυητής σοι
τῆς διηνεκοῦς εὐπλοίας; οὔπω κατῆρας εἰς τὸν λιμένα τῆς ἀναπαύσεως, οὔπω τῶν
κυμάτων ἐκτὸς ἕστηκας, οὔπω σοι βέβηκεν ἐπὶ σταθεροῦ ἡ ζωή. ἔτι φέρῃ διὰ τοῦ
βίου πελάγιος. οἷον σεαυτὸν δείξεις τῷ δυστυχήσαντι, τοιούτους παρασκευάσεις
σεαυτῷ τοὺς συμπλέοντας. ἀλλὰ πάντες καταντήσαιμεν εἰς τὸν λιμένα τῆς
ἀναπαύσεως τῷ ἁγίῳ πνεύματι πρὸς τὴν προκειμένην τοῦ βίου ναυτιλίαν
διευδιάζοντες. παρείη δὲ ἡμῖν ἡ διὰ τῶν ἐντολῶν ἐργασία καὶ τὸ τῆς ἀγάπης
πηδάλιον· δι' ὧν εὐθυνόμενοι καταλάβοιμεν τὴν γῆν τῆς 9.127 ἐπαγγελίας, ἐν ᾗ ἡ
πόλις ἐστὶν ἡ μεγάλη, ἧς τεχνίτης καὶ δημιουργός ἐστιν ὁ θεὸς ἡμῶν, ᾧ ἡ δόξα καὶ τὸ
κράτος εἰς τοὺς αἰῶνας τῶν αἰώνων. ἀμήν.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

7

