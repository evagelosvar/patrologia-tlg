In illud: Simile est regnum caelorum patri familias
Λόγος κατηχητικὸς εἰς τὸ ῥητὸν τοῦ Εὐαγγελίου· "Ὁμοία ἐστὶν ἡ
βασιλεία τῶν οὐρανῶν ἀνθρώπῳ οἰκοδεσπότῃ, ὅστις ἐξῆλθεν ἅμα πρωῒ
μισθώσασθαι ἐργάτας εἰς τὸν ἀμπελῶνα αὐτοῦ," καὶ τὰ ἑξῆς.
59.577
αʹ. Ὁρῶ τῇ ποίμνῃ τῶν εὐσεβῶν μεμιγμένα πρόβατα ξένα, νέαν τὴν σφραγῖδα
τοῦ ἐπουρανίου ποιμένος ἐπὶ τῶν μετώπων ἐπιφερόμενα· καὶ βούλομαι διδασκαλίας
χλόην εὑρεῖν κατάλληλον τοῖς γευσαμένοις ἤδη τῆς 59.578 χάριτος, καὶ τοῖς
μέλλουσι κρῖναι τὴν πόαν τοῦ πνεύ ματος· ἵνα τῆς αὐτῆς κατηχήσεως ἡ νομὴ τοῖς
μὲν πε
φωτισμένοις ὑπόμνησις γένηται τῶν τοῦ Θεοῦ δωρεῶν, τοῖς δὲ
φωτισθησομένοις πλείονα προτροπὴν πρὸς τὸ 59.579 βάπτισμα. ∆εῖ γὰρ πρὸς
ἑκατέρους κεκρᾶσθαι τὸν λό γον, διὰ τὸ κεκρᾶσθαι τὸν φιλόθεον δῆμον ἔκ τε μεμυη
μένων καὶ μυουμένων ἀνδρῶν.
Εὗρον τοίνυν πόθεν ἑστιάσω καὶ ὑμᾶς τοὺς οἰκογενεῖς ἀρχαίους ἅπαντας τοῦ
Χριστοῦ, καὶ πῶς τοὺς νεωνήτους δούλους ἡμῶν προτρέψω πρὸς τὴν ἐλευθερίαν τῆς
κλήσεως· ἀπαγγελῶ τοῖς ἐμοῖς ἀδελφοῖς ῥήσεις δεσποτικὰς δυναμένας καὶ τοὺς
πάλαι καταλαβόντας τὸν βασιλικὸν ἀμπελῶνα προ θυμοτέρους ἐργάσασθαι, καὶ
τοὺς ἄρτι παρακύψαντας εἰς αὐτὸν διεγεῖραι πρὸς τὸν ὅμοιον ζῆλον. Ὁμοία ἐστὶν ἡ
βασιλεία τῶν οὐρανῶν ἀνθρώπῳ οἰκοδεσπότῃ, ὅστις ἐξῆλθεν ἅμα πρωῒ
μισθώσασθαι ἐργάτας εἰς τὸν ἀμπελῶνα αὐτοῦ. Οἰκοδεσπότην ὁ ∆εσπότης ἑαυ τὸν
ὀνομάζει· ἐργάτας δὲ περὶ τρίτην καὶ ἕκτην καὶ ἐννάτην καὶ ἑνδεκάτην καλουμένους
προσαγορεύει πάν τας τοὺς εὐσεβεῖς, τοὺς ἐν διαφόροις ἡλικίαις δεξαμέ νους τὸ
βάπτισμα· ἀμπελῶνα τὰ σωτήρια λέγει προσ τάγματα, μικρὸν ἔχοντα πόνον, πολὺ δὲ
τὸ κέρδος. Ὁμοία ἐστὶν ἡ βασιλεία τῶν οὐρανῶν ἀνθρώπῳ οἰκοδεσπότῃ, ὅστις
ἐξῆλθεν ἅμα πρωῒ μισθώσασθαι ἐργάτας εἰς τὸν ἀμπελῶνα αὐτοῦ. Συμφωνήσας δὲ
μετ' αὐτῶν, τουτέστι τῶν ἐργατῶν, ἐκ δηναρίου τὴν ἡμέραν, ἀπέστειλεν αὐτοὺς εἰς
τὸν ἀμπελῶνα αὐτοῦ. Ὁ οἰκοδεσπότης ἐγώ εἰμι· ὥσπερ γὰρ ὁ οἰκοδεσπότης, οἰκίας,
ὡς βούλεται, οἰκοδομεῖ, οὕτως ἐγὼ τὸν κόσμον ὅλον, ὡς θέλω διατάττω. Ἐγὼ ὁ τῆς
δικαιοσύνης ἥλιος φορέσας τὴν νεφέλην τῆς ἀνθρωπότητος, συγκατέβην τῇ
ἀνθρωπότητι, καὶ περιέρχομαι ζητῶν ἐργάτας εἰς τὸν ἀμπελῶνά μου. Οὐ χρῄζω
ἀνθρώπων ἀμπελουργῶν· ἔχω γὰρ ἀγγέλους καὶ ἀρχαγγέλους ποιοῦντας ἃ
βούλομαι· ἀλλὰ ποθῶ τοὺς ἐπιγείους δούλους μου ἐν τοῖς ἐμοῖς κτήμασιν
αὐλιζομένους ὁρᾷν. Ἐγὼ περιτρέχω, καὶ περι βλέπομαι πανταχοῦ τίνας εὕρω, τίνας
μισθώσωμαι, οὐχ ἵνα μόχθοις ἀλλοτρίοις πλουτήσω, ἀλλ' ἵνα πλουτίσω τοὺς
ὑπακούοντάς μοι. Ἐγὼ τὸν Πέτρον καὶ Ἰωάννην καὶ τοὺς ἄλλους μαθητὰς, τοὺς
ἀόκνους φυτουργοὺς τῆς οἰκουμένης συνήγαγον· συνεφώνησα πρὸς αὐτοὺς ἐκ
δηναρίου τὴν ἡμέραν· ἀπεμισθωσάμην αὐτῶν πᾶσαν τὴν ζωὴν, καὶ ἐπηγγειλάμην
αὐτοῖς ὑπὲρ πάσης ὁμοῦ τῆς ἐργασίας διδόναι μισθὸν τὴν βασιλείαν τῶν οὐρανῶν·
ἀλλ' οὐκ ἀρκοῦσιν οὗτοι μόνοι τὴν ἐμὴν κορέσαι ψυχήν. Οὐ δύναμαι βλέπειν
εὐαριθμήτους γεωργοὺς γεωργοῦν τάς μου τὸ γεώργιον· στενοχωροῦμαι τοὺς
ὀφθαλμούς μου, τοσούτους μόνον ὁρῶν· ἔτι καὶ ἄλλους γηπόνους διψῶ, ἔτι καὶ
ἄλλους μισθοφόρους ποθῶ, ἔτι καὶ ἄλλους μύστας ζητῶ. Εὐρύχωρός ἐστι καὶ μέγας ὁ
παράδεισος τῶν ἐμῶν ἐντολῶν· πάντας τοὺς πιστεύσαντάς μοι χω ρεῖ· πάντας τοὺς
φιλαρέτους ὑποδεχόμενος ἔτι πλατύ τερος γίνεται.
Τί οὖν ποιήσω; πόθεν πληρώσω τοὺς ἐμοὺς περιβόλους; πόθεν πλείονας
ἀπογράφους προ τρέψομαι; Ἐξέλθω πάλιν εἰς τὰς τριόδους αὐτῶν, ἐξέλθω πρὸς
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

τοὺς πλανωμένους τὴν ἑκούσιον πλάνην, ἐξέλθω πρὸς τοὺς ῥᾳθύμους, ἐξέλθω πρὸς
τοὺς ἀπόρους ὁ εὔπο ρος, ἵνα τοὺς ἀπόρους εὐπόρους ἐργάσωμαι; ἐξέλθω πάλιν
πρὸς τὴν τῶν ἐργατῶν συλλογήν; Ἐγὼ τὸ ὀφει λόμενον ποιήσω· καλέσω καὶ πάλιν
τοὺς πένητας. Ἐμόν ἐστι τὸ προτρέψαι, ἐκείνων τὸ πεισθῆναι· ἐμόν ἐστι τὸ θελῆσαι
δοῦναι μισθὸν, ἐκείνων ἐστὶν τὸ θελῆσαι λαβεῖν· ἐὰν ὑπακούσωσί μοι, καὶ ἐμοὶ καὶ
ἑαυτοῖς χαριοῦνται· ἐὰν δὲ παρακούσωσί μου, ἑαυτοὺς ζημιώσουσιν· ἐμοὶ γὰρ οὐ
λείψουσι γεωργοὶ δυνάμενοι περισκάψαι καὶ περιβοθρεῦσαι καὶ περικλαδεῦσαι τὰ
κλήματα· οὐ δύ ναται ὁ ἐμὸς ἀμπελὼν μεῖναι κεχερσωμένος ἢ ἄκαρπος. Καὶ
ἐξελθὼν περὶ τὴν τρίτην ὥραν, εἶδεν ἄλλους ἐν τῇ ἀγορᾷ ἀργοὺς, κἀκείνοις εἶπεν·
Ὑπάγετε καὶ ὑμεῖς εἰς τὸν ἀμπελῶνά μου.
Καὶ ποῦ ἦτε πρωΐ; ποῦ ἦτε τὰς προλαβούσας ἡμέρας; πῶς οὐκ ἐζητήσατέ με
τὸν εἰς ζήτησιν ὑμῶν ἀφικόμενον; πῶς οὐκ ὠρθρίσατε πρὸς τὸ συμφέρον ὑμῖν;
Οὕτως ἀγνοεῖτε τῆς εὐσεβείας τὰς ἀκτῖνας, ὅτι τὴν ἡμέραν νύκτα νομίζετε; καὶ λάμ
ποντος τοῦ φωτὸς τῆς ἀληθείας, ὑμεῖς τί καρηβαρεῖτε καὶ νυστάζετε; Νῦν γοῦν
ἐξυπνίσθητε, νῦν γοῦν γρηγο
ρήσατε, νῦν γοῦν ἐπιστρέψατε, νῦν γοῦν
σωφρονήσατε. Ὑπάγετε καὶ ὑμεῖς εἰς τὸν ἀμπελῶνά μου Βλέπετε, πῶς ὑμᾶς ἀγαπῶ·
οὐ περιώρισα τῷ ὄρθρῳ τὸ κήρυγμα, οὐκ ἀπετείχισα τοῖς δευτέροις τὴν πρόσοδον, οὐ
περι 59.580 έκλεισα τῇ πρώτῃ κλήσει τὴν ὑμετέραν ἐπιστροφήν· πᾶσαν ὥραν ὑμῖν
ἐχαρισάμην πρὸς μετάνοιαν· πᾶσαν ἡλικίαν ἐναγκαλίζομαι. Κἂν βρέφη προσενεχθῇ
μοι, χαίρων προσίεμαι· ἐγὼ γάρ εἰμι τῶν βρεφῶν δημιουρ γὸς, καὶ πατὴρ καὶ μήτηρ,
καὶ τροφεὺς καὶ τροφή· κἂν παῖδες ὑποψελλίζοντες προσφύγωσιν, ἥδομαι· κἂν νεώ
τεροι προσχωρήσωσιν, μᾶλλον εὐφραίνομαι· κἂν ἄνδρες τέλειοι προσδράμωσιν,
ὑποδέχομαι· πάντας τοὺς μετὰ πίστεως προστρέχοντας ὡς ἀδελφοὺς ἐμοὺς
περιπτύσσο μαι. Ὑπάγετε καὶ ὑμεῖς εἰς τὸν ἀμπελῶνα. Οὐ λογί ζομαι τῶν
παρελθουσῶν ὡρῶν τὴν ζημίαν, ἐὰν ἴδω προθυμίαν ταλαντευομένην τῇ ῥᾳθυμίᾳ· οὐ
μειῶ τῆς ἡμέρας τοὺς μισθοὺς, ἐὰν τοὺς προφθάσαντας ὑμᾶς ἐπι φθάσαι
σπουδάζητε. Ἄφετε τὸν ἀμπελῶνα τῶν Ἰουδαίων τὸν ἀγνώμονα, τὸν ἀχάριστον, τὸν
ὄξος μόνον ἐπιστά μενον γεωργεῖν· καταλείπετε τὴν ἄμπελον τὴν ἐξ Αἰ γύπτου
μεταφυτευθεῖσαν, καὶ τῆς Αἰγυπτιακῆς εἰδωλο λατρείας τὴν πικρίαν περιφέρουσαν,
καὶ βότρυν πικρίας μαθοῦσαν βλαστάνειν τῷ φυτουργῷ, καὶ πρὸς τὸν ἐμὸν
ἀμπελῶνα μετάστητε· Καὶ ὃ ἐὰν ᾖ δίκαιον, δώσω ὑμῖν. Οὐκ εἰμὶ ἄδικος, κατὰ φύσιν
δίκαιος πέφυκα· οὐκ εἰμὶ πλεονέκτης, ὁ κατὰ τῆς πλεονεξίας νόμους ἐκθέ μενος·
οὐκ εἰμὶ ἀγνώμων περὶ τοὺς εὐγνώμονας δού λους, οὐκ εἰμὶ μικρόλογος ἢ
μικρόψυχος περὶ τοὺς μεγα λοψύχους περὶ ἐμέ· δώσω ὑμῖν μισθοὺς παριόντας τοὺς
πόνους· χορηγήσω δωρεὰς νικώσας τοὺς ἱδρῶτας ὑμῶν.
Πάλιν ἐξῆλθε περὶ τὴν ἕκτην καὶ ἐννάτην ὥραν, καὶ ἐποίησεν ὡσαύτως. Οὐ
καταλιμπάνω καιρὸν οὐ δένα· σαγηνεύω τοὺς θέλοντας, καὶ οὐ κάμνω τοὺς
κάμνοντας ἐν ταῖς ἁμαρτίαις πρὸς δικαιοσύνην καλῶν. Ἐξῆλθον ἅπαξ, καὶ θηρεύσας
ὑπέστρεψα ἐξῆλθον δεύτερον, καὶ ζωγρήσας ἀνέκαμψα· ἐξῆλθον τρίτον, καὶ
πλουτήσας ἐπανελήλυθα· ἐξῆλθον τέταρτον περὶ τὴν ἐννάτην ὥραν, καὶ λαβὼν ἥκω
τινάς. Ὅσον συνεχῆ ποιοῦμαι τὸν δρόμον, τοσοῦτον θερμότερον ἀναφλέγω τὸν
πόθον· ὅσους ἐὰν προσάγωμαι, τοσούτους ἄλλους ποθῶ· κἀκείνους εὑρὼν, ἑτέρους
ἐπιζητῶ· θέλω γὰρ πάντας ἀνθρώπους σωθῆναι, καὶ εἰς ἐπίγνωσιν ἀληθείας ἐλθεῖν.
βʹ. Περὶ δὲ τὴν ἑνδεκάτην ὥραν ἐξελθὼν, εὗρεν ἄλλους ἑστῶτας ἀργούς. Οὐ
παύομαι γυμνῶν τὸν διά βολον, ἕως ἂν ὁ βίος οὗτος παρείη· οὐ παύσομαι λῃ
στεύων τὸν θάνατον, τὸν δεινὸν κλέπτην τῆς ἀνθρωπό τητος· οὐκ ἀνέχομαι τοὺς
ἀπὸ γνώμης οἰκείας γενομέ νους θνητοὺς μὴ μεταβαλεῖν πρὸς ἀθανασίαν, ἕως
αὐτῆς αὐτῶν τελευταίας ἀναπνοῆς. ∆ιὰ τοῦτο οὐδὲ τῆς ἑνδε κάτης ὥρας
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

καταφρονῶ· διὰ τοῦτο προσεδρεύω ταῖς πύλαις τοῦ ᾅδου, ἵνα δυνηθῶ τινας πρὸ τοῦ
θανάτου μεταστῆσαι πρὸς τὴν ζωήν. Περὶ δὲ τὴν ἑνδεκάτην ὥραν ἐξελθὼν, εὗρεν
ἄλλους ἑστῶτας ἀργοὺς, καὶ λέγει αὐτοῖς· Τί ὧδε ἑστήκατε ὅλην τὴν ἡμέραν ἀργοί;
Ἐκκλησίας οὐκ ἔχετε; προφήτας οὐκ ἔχετε; ἀποστόλους οὐκ ἔχετε; μάρτυρας οὐκ
ἔχετε; θείας Γρα φὰς οὐκ ἔχετε; πνευματικὸν ἔργον οὐδέν; Τί οὖν ἑστή κατε ὧδε
ὅλην τὴν ἡμέραν ἀργοί; Οὐ βλέπετε τὸν ἥλιον τὸν τεταγμένον δρόμον ἀνύοντα; Οὐ
βλέπετε τὴν σελήνην διακονοῦσαν τῷ κτίσαντι; οὐ βλέπετε τὴν κτίσιν δουλεύουσαν
τῷ ποιητῇ; οὐ βλέπετε τῶν στοιχείων ἕκα στον ἐργαζόμενον τὸ ἔργον, δι' ὅπερ
ἐγένετο; ∆ιὰ τί οὖν ὑμεῖς μόνοι τῇ ἀργίᾳ τὴν φύσιν ὑβρίζετε; διὰ τί στήλας μιμεῖσθε
νεκράς; Οὐκ ἔχετε ὀφθαλμοὺς, ἵνα βλέποντες τοῦ κόσμου τὸ κάλλος, τὸν
κοσμοποιὸν ἀν υμνήσητε; οὐκ ἔχετε στόμα, ἵνα τὰς γλώσσας ὑμῶν πρὸς ὑμνῳδίας
κινήσητε; οὐκ ἔχετε χεῖρας, ἵνα τὴν ἐλεημοσύνην ἐργάσησθε; οὐκ ἔχετε πόδας, ἵνα
δράμητε δρόμους ψυχωφελεῖς;
Τί ὧδε ἑστήκατε ὅλην τὴν ἡμέ ραν ἀργοί; Μέχρι πότε τὴν ἐμὴν δεσποτείαν
ἐκτρέ πεσθε; Τί ὧδε ἑστήκατε ὅλην τὴν ἡμέραν ἀργοί; Οἱ δὲ λέγουσιν αὐτῷ· Οὐδεὶς
ἡμᾶς ἐμισθώσατο. Τοιοῦτοί εἰσιν οἱ προφασιζόμενοι προφάσεις ἐν ἁμαρτίαις, οἱ τὸ
παρὸν εἰς τὸ μέλλον ὑπερτιθέμενοι, οἱ λέγοντες, ἄφες ἄρτι, καὶ ὅτε κελεύει με ὁ
Θεὸς, τότε βαπτίζομαι· ἔνδος μοι τοῦτο τὸ πάσχα, καὶ εἰς τὸ μέλλον φωτίζομαι.
Τοιοῦτοί εἰσιν οἱ λέγοντες, Οὐδεὶς ἡμᾶς ἐμισθώσατο. Ἀλλὰ τί πρὸς αὐτοὺς ὁ τῇ
χρηστότητι νικῶν τὰς ὑπερ θέσεις τῶν ἀναβαλλομένων τὴν χάριν; Ὑπάγετε καὶ
ὑμεῖς εἰς τὸν ἀμπελῶνα. Οὐκ ἤλεγξε τὴν ψευδολογίαν 59.581 αὐτῶν, οὐκ
ἐστηλίτευσε τὴν εἰρωνείαν τῶν πεπλανημέ νων ἠθῶν, οὐκ εἶπε πρὸς τοὺς
ῥᾳθύμους, Τί λέγετε, ὅτι Οὐδεὶς ἡμᾶς ἐμισθώσατο; τί τῇ ῥᾳθυμίᾳ καὶ τὸ ψεῦδος
συῤῥάπτετε, οὐδεὶς ὑμᾶς ἐμισθώσατο; οὐ παρεκάλεσαν ὑμᾶς οἱ προφῆται; οὐ
προετρέψαντο ὑμᾶς οἱ ἀπόστολοι; οὐ παρεγενόμην ἐγὼ δι' ἐμαυτοῦ πρὸς ὑμᾶς; οὐκ
ἐκά λεσα πολλάκις, ὑμεῖς δὲ οὐχ ὑπηκούσατε; οὐκ εἴδετέ με μισθούμενον τοὺς
ἑταίρους ὑμῶν, καὶ ποιοῦντα μετ' ἐκείνων σύμφωνα; διὰ τί οὖν οὐκ ἐβουλήθητε
μιμήσα σθαι τοὺς πεισθέντας μοι, ἀλλ' ἐμείνατε μέχρι γήρους κατάσκοποι; Τούτων
οὐκ εἶπεν οὐδὲν ὁ Σωτὴρ, ἀλλ' Ὑπάγετε καὶ ὑμεῖς εἰς τὸν ἀμπελῶνά μου. ∆έχομαι
καὶ γέροντας ἐργατεύσασθαι θέλοντας· δέχομαι καὶ πο
λιὰς εὐσεβείᾳ
λαμπρυνομένας, δέχομαι καὶ τρέμοντα μέλη πίστει νευρούμενα· ἡδέως ὁρῶ
γέροντας κρατοῦν τας βακτηρίας ὁμοῦ καὶ σταυρούς.
Ὑπάγετε καὶ ὑμεῖς εἰς τὸν ἀμπελῶνά μου. Τῷ διαβόλῳ τὴν νεότητα προσ
ηνέγκατε, ἐμοὶ κἂν τὸ γῆρας δανείσατε· τῷ ἐχθρῷ τὴν ἰσχὺν ἐχαρίσασθε, ἐμοὶ κἂν
τὴν ἄχρηστον ἡλικίαν ἐπί δοτε· τῷ τυράννῳ τὸν χρόνον ὅλον, ὡς ἐκεῖνος ἠθέλησεν,
ἐστρατεύσασθε, ἐμοὶ τῷ ἐννόμῳ βασιλεῖ τὸν ἐπίλοιπον χρόνον σχολάσατε. Γίνεσθε
πιστοὶ, πρὶν γίνεσθαι νεκροί· λάβετε πρὸ τοῦ θανάτου τὴν τῆς ἀθανασίας στολήν·
ἑαυτοὺς πρὸ τῆς τελευτῆς ἐνταφιάσατε τοῖς ἀφθάρτοις ἐνταφίοις τῆς χάριτος·
δράμετε, σπεύσατε πρὸ τῆς ἑσπέρας ἔσω τοῦ ἀμπελῶνος εὑρεθῆναι· φιλονεικήσατε,
ἵνα ἔνδον ὑμᾶς καταλείπῃ δύνων ὁ ἥλιος. Ἐὰν πρὸ ἡλίου δυσμῶν ἑαυτοὺς
συναριθμήσητε τοῖς ἐμοῖς ἐρα σταῖς, ὄψεσθε τῆς ἐμῆς θεότητος τὴν δόξαν, ὡς φωτὸς
ἐρασταί· ἐὰν μετὰ τῶν κεκλημένων ἐκ τοῦ ἀμπελῶνος ἐξέλθητε, καὶ μὴ καμόντας
ὑμᾶς ὡς καμόντας ἀμεί ψομαι· ἐὰν ἔξω τῆς αὐλῆς ἱσταμένους ὑμᾶς καταλάβῃ τοῦ
θανάτου ἡ νὺξ, οὐκ ἔτι λοιπὸν ὑμᾶς ὡς φίλους ἔχειν ἀνέχομαι· νεκρὸς γὰρ οὐ
πιστεύει, νεκρὸς οὐχ ὁμολο γεῖ, νεκρὸς οὐκ ἐργάζεται καὶ θαῤῥεῖται μυστήριον,
νεκρὸς οὐκ ἐργάζεται σωφροσύνην, νεκρὸς οὐ δύναται μισθὸν ἀπαιτεῖν, νεκρὸς
ἀσφράγιστος καὶ πρὸ τῆς κρί σεως κατακέκριται· ζῶν καὶ ὑγιὴς αἰνέσει τὸν Κύριον·
ὁ τοιοῦτος νεκρὸς καὶ μετὰ θάνατον ζῇ, ὁ τοιοῦτος νε κρὸς μετὰ τὴν ταφὴν πάλιν
ἀνθεῖ. Ὑπάγετε καὶ ὑμεῖς εἰς τὸν ἀμπελῶνα.
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

3

Ἕως ἔχετε μικρὰν ἔτι προθεσμίαν ζωῆς, πραγματεύσασθε· μὴ περιμείνητε
τῆς ἀῤῥωστίας τὴν ἀνάγκην λαβεῖν τῆς ὑμετέρας γνώμης τὸν στέφανον. Μὴ
βουληθῆτε παρὰ τὸν καιρὸν τοῦ θανάτου μεταβα λέσθαι τὰ τῆς ἀθανασίας ἐφόδια·
ἐν ζωῇ τὴν ζωὴν ἐμπο ρεύσασθε· ἐν ὑγείᾳ καὶ νήψει τὸ τάλαντον ὑποδέξασθε, ἵνα
καὶ τὴν ἐργασίαν κερδάνητε. Ὑπάγετε καὶ ὑμεῖς εἰς τὸν ἀμπελῶνα· καὶ ὃ ἐὰν ᾖ
δίκαιον, λήψεσθε, Καὶ ὑπὲρ τὸ δίκαιον, λήψεσθε· μόνον μετανοήσατε, μόνον
προσέλθετε, μόνον γίνεσθε μετ' ἐμοῦ. Οὐ μέμ ψεσθέ μου τὴν προτροπήν· οὐδεὶς γὰρ
ἐμοὶ πιστεύσας ἀπῆλθεν ἀχαριστῶν, οὐδὲ ὁ ἐμοὶ διακονήσας ἔμεινεν ἔτι πτωχός.
Οὕτως ὁ οἰκοδεσπότης τοῦ οἴκου τούτου τὸν ἀμπελῶνα τοῦτον ἀνεπλήρωσεν.
Ὀψίας δὲ γενομένης. Ἤδη τῆς παρούσης ζωῆς ὡς ἡμέρας ὑποῤῥευσάσης, ἤδη
παραγενομένης τῆς μακρᾶς τοῦ θανάτου νυκτὸς, ἤδη τῆς ἀναστάσεως τῶν νεκρῶν
ἀνατειλάσης, ὥσπερ ὄρθρου τινὸς ἑσπέραν μὴ κεκτημένου διάδοχον, λέγει ὁ κύριος
τοῦ ἀμπελῶνος τῷ ἐπιτρόπῳ αὐτοῦ· Κάλεσον τοὺς ἐργάτας.
Κάλεσον πάντας τοὺς ἐξ Ἀδάμ· κάλεσον τὸν Ἄβελ τὸν διὰ θυσίαν τυθέντα·
κάλεσον τὸν Ἐνὼχ τὸν εὐαρεστήσαντα, καὶ μετατεθέντα· κάλεσον τὸν Νῶε τὸν τῆς
ναυαγησάσης φύσεως καλὸν κυβερνήτην· κάλε σον τὸν πιστότατον Ἀβραὰμ, τὸν
φιλόθεον μᾶλλον ἢ φι λότεκνον, τὸν στρατευσάμενον κατὰ τῶν σπλάγχνων αὐτοῦ,
τὸν τηρήσαντα τὴν πίστιν· κάλεσον τὸν Ἰωσὴφ τὸν τῆς σωφροσύνης ἐργάτην,
κάλεσον τοὺς πατριάρχας, κάλεσον τοὺς προφήτας· κάλεσον τὸν Ἠλίαν τὸν καλῶς
ἡνιοχήσαντα καὶ γενόμενον ἡνίοχον πυρίνων ἁρμάτων· κάλεσον τοὺς ἀποστόλους,
τοὺς τῆς Καινῆς ∆ιαθήκης φωστῆρας· κάλεσον τοὺς μάρτυρας, καὶ τοὺς μιμησα
μένους μου τὸν ὑπὲρ αὐτῶν θάνατον· κάλεσον Στέφα νον τὸν δι' ἐμὲ λιθασθέντα·
κάλεσον Βαρλαὰμ τὸν διὰ φυλακὴν τοῦ ἀμπελῶνος κατακαέντα τὴν δεξιάν· κάλε
σον πάντας τοὺς καλῶς κοπιάσαντας· κάλεσον τοὺς τὰ ἐμὰ καλλιεργήσαντας
κτήματα, Καὶ ἀπόδος αὐτοῖς τὸν μισθόν.
Ἄνοιξον τοὺς ἐμοὺς θησαυροὺς, ἄνοιξον τὰς ἐμὰς ἀποθήκας, ἄνοιξον τὰ
φαιδρὰ τῆς ἐμῆς βασιλείας βασίλεια· προκόμισον τοὺς βασιλικοὺς στεφάνους, προ
κόμισον τὰ θεῖα βραβεῖα· μὴ φείσῃ μηδενὸς τῶν ἐμῶν 59.582 κειμηλίων. Οὐκ
ἐφείσαντο γὰρ οὗτοι τῶν οἰκείων ψυχῶν δι' ἐμέ. Στεφάνωσον τὰς κεφαλὰς τῶν
ἀθλησάντων, ὡς ἠβουλόμην ἐγώ· ἔμβαλε τὰ βραβεῖα ταῖς καμούσαις χερσίν. Μηδεὶς
ἀστεφάνωτος ἐντεῦθεν ἐξέλθῃ, μηδεὶς ἀγέραστος ὑποστρέψῃ, μηδεὶς ἀποστερηθῇ
τῶν ἀμοιβῶν· πάντες πλουτήσωσιν, πάντες χορεύσωσιν, καὶ ἴδωσιν, ἐπὶ τίσιν
ἐμισθωσάμην αὐτούς· ἴδωσιν, ποίων οἱ πειθό μενοι ἐμοὶ τυγχάνουσιν ἀγαθῶν·
ἴδωσι, ποίων δωρεῶν ἑαυτοὺς ἀποστεροῦσιν οἱ καλούμενοι, καὶ παραγενέσθαι μὴ
βουλόμενοι. Ἀπόδος αὐτοῖς τὸν μισθὸν, ἀρξάμενος ἀπὸ τῶν ἐσχάτων, ἕως τῶν
πρώτων, τοὺς ἐσχάτους πρώτους εὐφραίνων ταῖς δωρεαῖς· οὗτοι γὰρ μάλιστα χρείαν
ἔχουσι τῆς ἐμῆς ἀγαθότητος. Ἐκεῖνοι μέντοι πρώϊθεν εὐθέως ἅμα τῇ ἡμέρᾳ τῶν
πόνων ἀρξάμενοι, αὐτὴν τὴν ἡμέραν καὶ τὰς ὥρας καὶ τὰς ἀρετὰς καὶ τοὺς ἀγῶνας
καὶ τὰς συνθήκας συνηγόρους ἔχουσιν· οὗτοι δὲ ἔσχατοι, πάντων τούτων ἔρημοι
καθεστῶτες, πρὸς μόνην τὴν φιλανθρωπίαν τὴν ἐμὴν ἀποβλέπουσιν. Τούτοις οὖν
τοῖς ἐσχάτοις πρώτοις ἐπίδος τὰ δῶρα, καὶ τότε τοῖς πρώτοις ἐπιδώσεις τὰς ἀμοιβάς.
Καὶ ἐλθόντες οἱ περὶ τὴν ἑνδεκάτην ὥραν, ἔλαβον ἕνα δηνάριον. Καὶ τοῦτο
περιχαρῶς ἐν ταῖς ἑαυτῶν χερσὶ περι στρέφοντες ἔλεγον· Ἐκινδυνεύσαμεν ἀπιστεῖν
τοῖς ἀγα θοῖς· ᾔδεισαν γὰρ οὐδὲν ἄξιον ὧν ἔλαβον ἐργασάμενοι. Ὁρῶντες δὲ τὸ
δῶρον τοῖς ὀφθαλμοῖς, καὶ ταῖς παλά μαις τὴν χάριν κατέχοντες, ἐχόρευον,
ἐπανηγύριζον ἐπὶ τοῖς παρ' ἐλπίδα καλοῖς. Ἐλθόντες δὲ οἱ πρῶτοι ἐνό μισαν, ὅτι
πλείονα λήψονται, καὶ ἔλαβον καὶ αὐτοὶ ἕνα δηνάριον. Λαβόντες δὲ ἐγόγγυζον κατὰ
τοῦ οἰκοδεσπότου, λέγοντες· Οὗτοι οἱ ἔσχατοι. Ὁ δε σπότης τῷ λόγῳ παρήγαγεν,
οὐχ ἵνα τοῖς τιμωμένοις φθονήσωσι (χαίρουσι γὰρ, οὐκ ἀγανακτοῦσι πάντων
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

4

σωζομένων οἱ ὅσιοι), ἀλλ' ἵνα δείξῃ τὴν οἰκείαν φιλαν θρωπίαν τοιαύτην οὖσαν,
ὥστε καὶ τοῖς ἀφθόνοις, εἰ ἐνεχώρει, φθόνον τεκεῖν.
γʹ. Λαβόντες δὲ ἐγόγγυζον κατὰ τοῦ οἰκοδεσπότου, λέγοντες, Οὗτοι οἱ
ἔσχατοι, οὗτοι οἱ γεγηρακότες, οὗτοι οἱ μόλις τὴν σὴν δεσποτείαν ἐπεγνωκότες, οἱ
μόλις τὰ γόνατα πρὸς τὴν σὴν προσκύνησιν κάμψαντες, οἱ μόλις τοῖς ὑποτρόμοις
δακτύλοις τοῦ σωτηρίου σου σταυ ροῦ τὸ σύμβολον τοῖς μετώποις ἐπιχαράξαντες, οἱ
μὴ βα στάσαντες κοφίνους πειρασμῶν, οἱ μὴ κρατήσαντες δι κέλλας συμφορῶν·
Οὗτοι οἱ ἔσχατοι μίαν ὥραν ἐποίη σαν. Εἶδον μόνον τὸν σὸν ἀμπελῶνα, τοῖς
ὀφθαλμοῖς ἡμῖν μόνοις ἐκοινώνησαν τῆς ἐργασίας, τὰς ὄψεις αὐτῶν ὑπέδειξαν, οὐ
κατώρθωσαν πράξεις τινάς· μάρτυρες τῶν μόχθων, οὐ μερισταὶ γεγόνασιν· Καὶ ἴσους
ἡμῖν αὐτοὺς ἐποίησας τοῖς βαστάσασιν τὸ βάρος τῆς ἡμέ ρας καὶ τὸν καύσωνα;
Ἡμεῖς ἐφλέχθημεν ταῖς ἡδοναῖς, ἀλλ' οὐκ ἐσβέσθημεν ταῖς ἀρεταῖς· ἡμεῖς
ἐδιώχθημεν διὰ σὲ, ἡμεῖς μυρίους ὑπεμείναμεν πόνους, ἵνα τὴν σὴν οὐ σίαν
πλατύνωμεν· καὶ βαδίζομεν ἐντεῦθεν λαβόντες μι σθοὺς ὁμοίους τοῖς μὴ
κεκμηκόσιν ὁμοίως ἡμῖν· ὁ πόνος ἀνόμοιος, καὶ τὸ κέρδος προσόμοιον· ὁ κόπος
ἄνισος, καὶ τὸ κλέος ἐφάμιλλον.
Οὗτοι οἱ ἔσχατοι μίαν ὥραν ἐποίησαν. Ἵνα μίαν ὥραν ἐγγύς σου κλαύσῃ
γυνὴ πόρνη, καὶ λύσῃ τὰς τρίχας, ἃς ἔπλεκε πρὸ τούτου κα κῶς, καὶ φιλήσῃ τοὺς
ἀχράντους σου πόδας σώφροσι χείλεσι, καὶ προσενέγκῃ σοι τῷ οὐρανίῳ μύρον
ἐπίγειον, ἔδωκας αὐτῇ πρέσβεια θυγατρὸς καὶ παρθένου. Ἵνα μίαν ὥραν
ἐσταυρωμένος λῃστὴς ἐπὶ τοῦ σταυροῦ σοι προσ φύγῃ καὶ βοήσῃ, Μνήσθητί μου,
Κύριε, ὅταν ἔλθῃς ἐν τῇ βασιλείᾳ σου, ὑπὲρ μόνης τῆς φωνῆς ταύτης ἀνέῳξας αὐτῷ
τὸν παράδεισον. Ἵνα μίαν ὥραν μετανοήσῃ Παῦλος ὁ διώκτης, ὁ πολέμιος,
Εὐαγγελιστὴς ἀναδέδει κται. Οὗτοι οἱ ἔσχατοι μίαν ὥραν ἐποίησαν, καὶ ἴσους ἡμῖν
αὐτοὺς ἐποίησας τοῖς βαστάσασιν τὸ βάρος τῆς ἡμέρας καὶ τὸν καύσωνα. Τί οὖν πρὸς
τοὺς οὕτω δια κειμένους ὁ φιλάνθρωπος Κύριος; Ἀποκριθεὶς ἑνὶ αὐ τῶν εἶπε·
Ἑταῖρε, οὐκ ἀδικῶ σε. Ὢ φιλανθρωπίας ἀνυπερβλήτου! ὁ ∆εσπότης ὑπὲρ τῶν
δούλων τῷ δούλῳ δικάζεται· ὁ κριτὴς ὑπὲρ τῶν κρινομένων ἀπολογεῖται τῷ
κατηγόρῳ. Ἑταῖρε, οὐκ ἀδικῶ σε.
∆ίκασαί μοι κατὰ λόγον, ἑταῖρε, καὶ μὴ ἀδίκως λυποῦ· διαλέχθητί μοι
νομίμως, καὶ μὴ παρανόμως δυσχέραινε· εἰπὲ, τί ἠδικήθης αὐτὸς, τιμηθέντος τοῦ
συνδούλου σου; εἰπὲ, τί ἐβλάβης αὐτὸς, συμβασιλεύοντος ἔτι τοῦ σοῦ ἀδελφοῦ; Οὐκ
ἔλαβες, δι' ὃ ἔκαμες; οὐχ ὑπεδέξω τῆς ἡμέρας 59.583 ὅλης τέλειον τὸν μισθόν; οὐκ
ἀπέλαβες, ὅπερ ἐπόθεις; τί οὖν ἄλλο πλέον ζητεῖς; Οὐχὶ δηναρίου συνεφώνη σάς
μοι; Ὑπόμνησον ἑαυτὸν τῶν ἐμῶν ὑποσχέσεων, ἐφ' οἷς ἐμισθωσάμην σε τὴν ἀρχήν.
Εἰ παρὰ τὰ συγκεί μενα ἐποίησά τι, δικαίως ἀγανακτεῖς· εἰ δὲ τὰς συνθήκας
ἐπλήρωσα, μάτην ἐγκαλεῖς τῷ ἀνεγκλήτῳ. Οὐχὶ δηνα ρίου συνεφώνησάς μοι; Μή τι
τῶν σῶν ἐμείωσα δω ρεῶν; μὴ τὰ σὰ λαβὼν ἑτέροις προσέδωκα; μὴ τὴν σὴν δόξαν
ἄλλοις ἀπένεμον; οὐχὶ τὴν ἐμὴν οὐσίαν, οἷς ἠθέ λησα, κατεμέρισα; τί οὖν γογγύζεις,
βλαβεὶς ὡς δοῦλος, ὡς ἐλεύθερος τιμηθείς; τί οὖν γογγύζεις ὁ τοσ οῦτον ὠφεληθείς;
τί φθονεῖς τοῖς σοῖς ἀδελφοῖς, ἄφθονον ἔχων δεσπότην; Ἆρον τὸ σὸν, καὶ ὕπαγε.
Ἀρκέσθητι τοῖς παροῦσι· μὴ θέλε μόνος βασιλεύειν κεκλημένος μετὰ πολλῶν· ἀρκεῖ
ἡ βασιλεία τῶν οὐρανῶν καὶ σοὶ καὶ πᾶσι τοῖς ἀξίοις αὐτῆς. Θέλω δὲ τούτῳ τῷ
ἐσχάτῳ δοῦναι ὡς καὶ σοί. Τῷ θελήματί μου τίς ἀντιστήσεται; Θεὸς ὁ δικαιῶν, καὶ
τίς ὁ κατακρίνων; Θεὸς ὁ χορηγῶν, τίς ὁ λογοθετῶν; Θεὸς ὁ πλουτίζων, τίς ὁ
γυμνῶσαι δυνάμε νος; ὁ κριτὴς συνήγορος, τίς ὁ κατήγορος; ὁ δικαστὴς σύνδικος,
τίς ἀντίδικος εὑρεθήσεται; Θέλω δὲ τούτῳ τῷ ἐσχάτῳ δοῦναι ὡς καὶ σοί. Οὐ φέρω
τοὺς ἐμοὺς ἀν δριάντας γυμνοὺς δόξης καταλιπεῖν· οὐ φέρω τὴν ἐμὴν εἰκόνα
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

5

πτωχεύουσαν παριδεῖν· οὐ φέρω μὴ συμβασιλεῦ σαί μοι καὶ τούτους, δι' οὓς
κατῆλθον ἐκ τῶν οὐρανῶν. Θέλω δοῦναι τούτῳ τῷ ἐσχάτῳ ὡς καὶ σοί. Ἔσχατος
ἦλθεν, οἶδα κἀγώ· ἀλλὰ προσήγαγέ μοι πίστιν ἀπολο γουμένην ὑπὲρ τῆς
γεγενημένης βραδύτητος· ἔσχατος ἦλθεν, ἀλλ' ἐπεδείξατο πόθον, οἷον ποθῶ·
ἐπλανήθη τὸν παρελθόντα χρόνον ὡς πρόβατον, ἀλλ' ἐπέγνω τὸν ποι μένα τὸ
πρόβατον. Ἐμὸς ἦν οὗτος καὶ ὅτε ἐῤῥέμβετο. Ἔσχατος ἦλθεν, ἀλλὰ διὰ τῆς ἐμῆς
κολυμβήθρας παρῆλ θεν, ὥσπερ καὶ σύ. Ἔσχατος ἦλθεν, ἀλλὰ τῆς ἐμῆς τρα πέζης
ἀπήλαυσεν, ὥσπερ καὶ σύ· ἔσχατος ἦλθεν, ἀλλ' εἶδέ μου πᾶσαν τοῦ ἀμπελῶνος τὴν
εὐπρέπειαν· εἶδε τοῦ ξύλου τῆς ζωῆς τὸ φυτόν· κατεφίλησε τὰς ἀκάνθας, ἃς
ἐστεφανοφόρησα δι' αὐτόν· περιεπτύξατο τὸν σταυρὸν, ὃν ὑπέμεινα δι' αὐτόν. Οὐ
δύναμαι οὖν τὸν οὕτω με πο θήσαντα, τὸν οὕτω μοι πιστεύσαντα, μὴ τιμῆσαι πάσῃ
σπουδῇ καὶ τιμῇ. Ὡς ἐπίστευσας, ἐπίστευσεν· ὡς εὐπό ρησας, εὐπόρησεν· ὡς
προσεκύνησας, προσεκύνησεν· ὡς ἐβασίλευσας, ἐβασίλευσεν· ὡς ἔκαμες, οὐκ ἔκαμεν,
ἀλλ' ἐγὼ τῇ χάριτι τὸ λεῖπον ἀνεπλήρωσα. Ἢ οὐκ ἔξ εστί μοι ποιῆσαι, ὃ θέλω, ἐν
τοῖς ἐμοῖς; μὴ γὰρ τὰ σὰ δαπανῶ; Τὰ ἐμὰ χορηγῶ. Μὴ γὰρ ἐν τοῖς σοῖς φιλο
τιμοῦμαι; Ἐν τοῖς ἐμοῖς αὐθεντῶ. Μὴ γὰρ ἐπίτροπόν σε τῆς ἐμῆς κατέστησα γνώμης;
Μισθωτόν σε ἔλαβον ὡς ἐξ ἀρχῆς. Μὴ γὰρ κύριόν σε τῆς ἐμῆς ἐξουσίας ἐχειρο
τόνησα; Ὡς φιλάνθρωπος κοινωνὸν ἐποιησάμην τῶν ἐμαυτοῦ. Εἰ ὁ ὀφθαλμός σου
πονηρός ἐστιν, οὐ δύναται βλέπειν, ἃ ὀφθαλμὸς οὐκ εἶδε, καὶ οὖς οὐκ ἤκουσεν. Ἀθο
λώτου χρεία βλέμματος εἰς τὴν τῶν μυστηρίων ἐπίσκε ψιν· καθαρᾶς χρεία ψυχῆς εἰς
τὴν τῶν καθαρῶν κατα νόησιν.
Εἴδετε, πῶς ἀγαθὸς ὁ ∆εσπότης ἡμῶν· εἴδετε, πῶς τοὺς ἐν διαφόροις καιροῖς
προσιόντας, μίαν δὲ πίστιν καὶ προθυμίαν καὶ πολιτείαν ἐπιδεικνυμένους, ὁμοίαις
τιμαῖς καὶ δωρεαῖς ἠξίωσεν· εἴδετε, πῶς καὶ τοὺς ἐσχά τους ἴσους τοῖς πρώτοις
ἐποίησε διὰ τὴν ἄβυσσον τῶν αὐτοῦ οἰκτιρμῶν. Ἐπήχθητε τοίνυν πρὸς τὸν οὕτω
φιλάν θρωπον· αὐτὸς καὶ νῦν ὑμᾶς ἐμισθώσατο· αὐτὸς λέγει καὶ πρὸς ὑμᾶς·
Ὑπάγετε καὶ ὑμεῖς εἰς τὸν ἀμπελῶνα. Θεὸς ὁ μισθούμενος, ὁ μισθὸς οὐράνιος, τὸ
ἔργον σωτή ριον, ὁ ἀμπελὼν εὐτρεπὴς, ἡ θύρα τοῦ ἀμπελῶνος, ἡ κολυμβήθρα
καθέστηκεν. Ἡ θεία χάρις εἰς θύραν τῆς κολυμβήθρας παρισταμένη, καθάπερ φαιδρὰ
πυλωρὸς πρὸς πάντας τοὺς δι' αὐτῆς εἰσιόντας βοᾷ· Αὕτη ἡ πύλη τοῦ Κυρίου·
νεοφώτιστοι διελεύσονται δι' αὐτῆς. Ἡμεῖς οὐ φθονοῦμεν ὑμῖν ὡς τελευταῖον
ἐλθοῦσιν, καὶ τῆς αὐτῆς ἡμῖν δόξης μετέχουσιν· ἀλλὰ συγχορεύομεν ὡς ἀδελφοῖς,
καὶ συγχαίρομεν, ἡμετέραν δόξαν τὴν ὑμετέ ραν σωτηρίαν ἡγούμενοι. Ποθήσατε
οὖν τὸν καλέσαντα· 59.584 λάβετε τῆς εἰς τὸν ἀμπελῶνα εὐπορίας τὴν πίστιν ὁδηγὸν
ἀπλανῆ· πίστις γὰρ τῆς ἐπὶ τὴν κολυμβήθραν ὁδοῦ χει ραγωγὸς ἀγαθὸς, πίστις
χειμαζομένης φύσεως ἄγκυρα, πίστις ἀνθρωπότητος λιμὴν γαληνὸς, πίστις τῶν
ἀοράτων κάτοπτρον τηλαυγὲς, πίστις τῆς ὁμοουσίου Τριάδος διδάσκαλος ἀκριβὴς,
πίστις λέγειν ἡμᾶς παρεσκεύασεν ἐκεῖνα τὰ σωτήρια ῥήματα· "Πιστεύω εἰς ἕνα Θεὸν
Πα τέρα παντοκράτορα." Πιστεύω, οὐκ ἐρευνῶ· πιστεύω, οὐ διώκω τὸ ἀκατάληπτον·
πιστεύω, οὐ μετρῶ τὸ ἀμέτρη τον. Ἐὰν πιστεύσω, φωτίζομαι τὴν ψυχήν· ἐὰν περιερ
γάζωμαι, σκοτίζω μου τοὺς λογισμούς· ἐὰν πιστεύσω καλῶς, ἀνυψοῦμαι πρὸς
οὐρανόν· ἐὰν ζητήσω περιέρ γως, καταφέρομαι πρὸς βυθόν. Πιστεύω εἰς ἕνα καὶ
μόνον ἀληθινὸν Θεὸν καὶ Πατέρα παντοκράτορα
. Ἀρνοῦ μαι τοὺς παρὰ τῶν Ἑλλήνων πεπλανημένους θεούς· πολυθεΐα γὰρ
οὐρανὸν οὐκ οἰκεῖ. Ἕνα καὶ μόνον Θεὸν ἀνυμνῶ· μισῶ τὸν Ἀρειανισμόν· οὐ λέγω,
Ἦν ποτε, ὅτε οὐκ ἦν Πατήρ· ἀλλὰ ἀεὶ Θεὸν τὸν Θεὸν, ἀεὶ Πατέρα τὸν αὐτὸν κηρύττω,
οὕτως ὄντα Πατέρα, ὡς οἶδεν αὐτός. Πιστεύω εἰς τὸν Κύριον ἡμῶν Ἰησοῦν Χριστὸν,
τὸν Υἱὸν αὐτοῦ τὸν μονογενῆ, τὸν ἐξ αὐτοῦ γεννηθέντα πρὸ πάν των τῶν αἰώνων,
ὡς αὐτὸς μόνος οἶδεν ὁ γεννηθείς· γεννηθέντα, οὐ ποιηθέντα· γεννηθέντα, οὐ
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

6

κτισθέντα· γεννηθέντα, οὐ δημιουργηθέντα· ἐκ τῆς πατρῴας οὐσίας ἀχρόνως
ἐκλάμψαντα, οὐκ ἔξωθεν ἐπεισελθόντα ποθὲν, οὐχ ὑποβολιμαῖον, ἢ νόθον υἱὸν,
ἀλλὰ γνήσιον ἀπαύγασμα τοῦ γνησίου Πατρὸς, ἀπαράλλακτον χαρακτῆρα ἀοράτου
μορφῆς. Οἶδα τὸν Χριστὸν Υἱὸν τοῦ Θεοῦ Πατρὸς ὡς Θεόν· οἶδα τὸν Χριστὸν υἱὸν τῆς
δούλης αὐτοῦ καὶ μη τρὸς, φαινόμενον ὡς ἄνθρωπον· οἶδα τὸν Χριστὸν ὁμοού σιον
τοῖς ἀνθρώποις, ὡς ἄνθρωπον ἐξ ἀνθρώπου· οἶδα τὸν Χριστὸν ἀόρατον καὶ
ὁρώμενον· οἶδα τὸν Χριστὸν ἀψηλάφητον, καὶ παρὰ τοῦ Θωμᾶ ψηλαφούμενον· οἶδα
τὸν Χριστὸν χιλιάδας ῥήματι θρέψαντα, καὶ τοῖς κύμα σιν ἐπιβαδίσαντα, καὶ τοῖς
δαίμοσιν ἐπιτάξαντα, καὶ νεκροὺς ἀναστήσαντα, καὶ μυρία ποιήσαντα θαύματα τῇ
δυνάμει τῆς οἰκείας θεότητος· οἶδα τὸν Χριστὸν πεινά σαντα, καὶ διψήσαντα, καὶ
κοπιάσαντα, καὶ ἀγωνιάσαντα, καὶ ἱδρώσαντα τῇ φύσει τῆς ἀνθρωπότητος· οἶδα τὸν
Χριστὸν ὡς Θεὸν ἀπαθῆ· οἶδα τὸν Χριστὸν ἀποθανόντα, καὶ σαρκὶ τὸ πάθος
δεξάμενον· οἶδα τὸν Χριστὸν κατὰ τὸ δυνατὸν ὑπομείναντα τὴν τελευτήν· οἶδα τὸν
Χριστὸν ἀναστήσαντα τοῦ σώματος αὐτοῦ τὸν ναόν.
Γνωρίζω τῶν δύο φύσεων αὐτοῦ τὸ διάφορον· οὐ μερίζω τὸν ἕνα Υἱὸν εἰς δύο
υἱούς. Πιστεύω καὶ εἰς τὸ Πνεῦμα τὸ ἅγιον. τὸ Πνεῦμα τῆς ἀληθείας, τὸ παρὰ τοῦ
Πατρὸς ἐκπορευό μενον, τὸ τῆς Τριάδος συμπληρωτικόν· αὐτῷ θαῤῥῶ τὸν ἐμὸν
ἁγιασμὸν, αὐτῷ θαῤῥῶ τὴν ἐκ τῶν νεκρῶν ἐξανά στασιν.
δʹ. Ταῦτα τῇ ψυχῇ καὶ τῇ γλώττῃ μελετᾶτε διαπαντός· ταῦτα τῷ φρονήματι
καὶ τῷ στόματι βοᾶτε διηνεκῶς. Ἔχετε τὸν ἀποστολικὸν ὅρον τῆς πίστεως καὶ
πολιτείας θεοφιλοῦς· ἄνευ γὰρ τῆς πίστεως τὰ ἔργα νεκρά. Ἀν θρώπινον λέγω διὰ
τὴν ἀσθένειαν τῆς σαρκὸς ὑμῶν· Ὥσπερ γὰρ παρεστήσατε τὰ μέλη ὑμῶν ὅπλα ἀδι
κίας τῇ ἁμαρτία, οὕτω νῦν παραστήσατε τὰ μέλη ὑμῶν ὅπλα δικαιοσύνης τῷ Θεῷ. Ὁ
πόρνος γενέσθω σώφρων, ὁ ἄδικος γενέσθω δίκαιος, ὁ ἅρπαξ εὐμετάδο τος, ὁ
πλεονέκτης φιλόπτωχος, ὁ ἀπάνθρωπος καὶ φιλό χρυσος πρὸ τούτου, νῦν φανήτω
φιλόχριστος, ὁ βλάσφη μος ὑμνολόγος, ὁ τρέχων εἰς θέατρα, τρεχέτω λοιπὸν εἰς τὰς
αὐλὰς τοῦ Θεοῦ, ὁ τοῖς δρόμοις τῶν ἵππων ἐπιμαι νόμενος, σχολασάτω τοῖς
ἀποστόλοις, ὧν ὁ δρόμος τοῖς μιμουμένοις σωτήριος. Ὡς μέλλοντες χρηματίζειν νύμ
φαι Χριστοῦ, οὕτω στολίσατε τὰς ὑμετέρας ψυχὰς καὶ τὰ σώματα.
Ἔστω ὑμῶν τὰ βλέμματα τῆς κατὰ ψυχὴν γαλήνης ἐξεικονίσματα· ἔστωσαν
αἱ ὄψεις τῆς ἔνδον εὐ λαβείας χαρακτῆρες σαφεῖς· ἔστω ὑμῶν τὰ στόματα ψαλμῶν
καὶ ὕμνων εὐωδίαν ἐκπέμποντα, κόσμια, σώ φρονα, πρέποντα παρθένῳ μελλούσῃ
νυμφεύεσθαι· ἔστωσαν αἱ ψυχαὶ καθαραὶ, λευκαὶ, ἵνα γνησίαν τῆς 59.585 βασιλικῆς
ἁλουργίδος τὴν βαφὴν ὑποδέξωνται. Ἐπειδὰν δὲ καταλάβητε τὸν νυμφῶνα τοῦ
Πνεύματος, ἐπειδὰν εἰσδράμητε τὴν παστάδα τῆς χάριτος, ἐπειδὰν πλησίον γένησθε
τῆς φοβερᾶς ὁμοῦ καὶ ποθεινῆς κολυμβήθρας, ὡς αἰχμάλωτοι προσπέσατε τῷ
βασιλεῖ· ῥίψατε πάντες ὁμοίως ἐπὶ γόνατα, καὶ τὰς χεῖρας ἀνατείναντες εἰς τὸν
οὐρανὸν, ὅπου κάθηται βασιλικῶς ὁ πάντων ἡμῶν βασιλεὺς, καὶ τοὺς ὀφθαλμοὺς
εὐθύναντες πρὸς τὸν ἀκοί μητον ὀφθαλμὸν, τοιούτοις πρὸς τὸν φιλάνθρωπον χρή
σασθε ῥήμασιν· Ἄνοιξον ἡμῖν τὸν ἀμπελῶνά σου, Κύ ριε, εἰ καὶ περὶ τὴν ἑνδεκάτην
ὥραν ἐφθάσαμεν· μὴ ὀργισθῇς ἡμῖν ὡς βραδύνασιν, ∆έσποτα, μηδὲ κλείσῃς εἰς τὰς
ἡμετέρας ὄψεις τῆς σῆς φιλανθρωπίας τὰς θύρας· κατέγνωμεν τῆς φθασάσης ἡμῶν
ἀργίας, κατέγνωμεν τῆς προλαβούσης ἡμέρας τὴν ῥᾳθυμίαν, ἐβλάβημεν παρα
κούσαντες, ἐζημιώθημεν καθευδήσαντες, ἐναυαγήσαμεν τερπόμενοι, ἐγυμνώθημεν
παίζοντες καὶ παιζόμενοι, ἀπωλόμεθα τοῖς ματαίοις ψυχαγωγούμενοι. Μόλις ἐγρη
γορήσαμεν, μόλις ἐνήψαμεν, μόλις εὕρομεν τὸ συμφέρον, μόλις ἐφύγομεν τὸν
πόλεμον· ἀπεταξάμεθα τῷ πονηρῷ παντελῶς, ἀπεταξάμεθα τοῖς ὀλεθρίοις κακοῖς
καὶ τερ πνοῖς συσχολάσαι· τῷ Σωτῆρι λοιπὸν συνταττόμεθα, τῷ μόνῳ Θεῷ. ∆έξαι τὸ
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

7

γραμμάτιον τῆς ὁμολογίας ἡμῶν, ἵνα φοβούμενοι τὸ πρόστιμον, ἐμμένωμεν οἷς καὶ
συν εθέμεθα· δέξαι τὸ σύμβολον τῶν πρὸς σὲ συνθηκῶν, ἵνα τὸ σύμβολον
αἰσχυνόμενοι, τὸν τρόπον ἡμῶν ἀπερίτρε πτον καθιδρύσωμεν.
Οἴκτειρον τοὺς σοὺς δούλους, τοὺς αἰχμαλώτους, καὶ γυμνοὺς, καὶ νεκρούς·
ἀπόδος ἡμῖν τὴν ἀρχαίαν συγγένειαν· ἔνδυσον οὓς ὁ διάβολος ἀπεγύ μνωσεν· χρῖσον
ἡμᾶς τῷ ἐλαίῳ τῆς ἁγίας σου χρίσεως· ἵνα μὴ βλαβῶμεν ἐὰν πάλιν δηχθῶμεν ὑπὸ
τοῦ ὄφεως. Θέλομεν ἐργάσασθαι τὰς σὰς ἐντολὰς, θέλομεν εὑρεθῆναι τοῦ
ἀμπελῶνος ἐντὸς, θέλομεν ὡς ἐγγίζοντές σοι χρη ματίζειν ἐργάται· ἐζήσαμεν πάσας
ἡμῶν τὰς ἡμέρας 59.586 κακῶς. θέλομεν ζῆσαι τὰς ὑπολοίπους καλῶς. Θέλοις ὡς
πόρνοις συγχωρῆσαι. Συγχώρησον. Θέλοις ὡς τελώναις περιποιήσασθαι· θέλοις ὡς
λῃστὰς φιλανθρωπεύσασθαι· θέλοις ὡς διώκτας περισῶσαι· περίσωσον, ὡς θέλεις·
ἐλέησον μετανοοῦντας, οἷς ἐμακροθύμησας ἐξαμαρτά νουσιν· ἐλέησον
προσπίπτοντας, οὓς καὶ ἀπολακτίζοντας ᾤκτειρες· ἐλέησον καθικετεύοντας, οὓς
ὑβρίζοντας ἔφε ρες. Ἄνοιξον ἡμῖν τοῖς χειμαζομένοις τὸν τῆς κολυμβή θρας λιμένα·
ἄνοιξον ἡμῖν τοῖς τετραυματισμένοις τὸ ἀνώδυνον ἰατρεῖον· ἄνοιξον τοῖς
παλαιωθεῖσιν τὸ ἄλυπον χωνευτήριον· ἄνοιξον τοῖς χρεωστοῦσιν θάνατον τὸν
ἀκίνδυνον τάφον· ἄνοιξον ἡμῖν τῆς τριημέρου σου νε κρώσεως τὸ ζωηφόρον
ἐκτύπωμα· ἄνοιξον ἡμῖν τῆς πα λιγγενεσίας τὴν παρθένον γαστέρα· ἄνοιξον ἡμῖν
τῆς υἱοθεσίας τὴν ἄφθαρτον μητέρα· ἄνοιξον ἡμῖν τῆς ἀθα νασίας τὴν πύλην·
ἄνοιξον ἡμῖν τὴν θύραν τῶν οὐρα νῶν· ὅτι σὺ εἶπας, Κρούετε, καὶ ἀνοιγήσεται ὑμῖν.
Ἅπερ εἶπας, ἀληθείᾳ πλήρωσον· ἅπερ ἐλάλησας, ὡς ἀψευδὴς ἐπιτέλεσον.
Πιστεύομεν, ὅτι δύνασαι πάντα ποιῆσαι· πιστεύομεν, ὅτι δύνασαι τῶν ἁμαρτημάτων
ἡμῶν ἀποπλῦναι τὸν βόρβορον· πιστεύομεν, ὅτι δύνασαι διὰ Πνεύματος καὶ ὕδατος
ἀναγεννῆσαι τοὺς ἐν ἁμαρ τίαις γηράσαντας· πιστεύομεν, ὅτι δύνασαι τὴν ξένην
λοχείαν ἐργάσασθαι· πιστεύομεν, ὅτι δύνασαι γυναῖκας καὶ βρέφη καὶ νέους καὶ
ἄνδρας καὶ γέροντας ἐκ τῶν ὑδάτων νεοπλάστους ἀναγαγεῖν ὡς ἀρτίτοκα βρέφη· πι
στεύομεν, ὅτι δύνασαι τοῖς ὕδασιν ἀποπνῖξαι τὰς ἁμαρ τίας ἡμῶν, καθάπερ τὸν
Φαραὼ καὶ τὴν δύναμιν αὐτοῦ πᾶσαν κατέκλυσας· πιστεύομεν, ὅτι δύνασαι
σωτηρίας ὁδὸν ἐξευρεῖν, ὥς ποτε τοῖς Ἰσραηλίταις ἐδημιούργησα; ὁδὸν ἐν μέσῳ τῆς
θαλάσσης· πιστεύομεν, ὅτι δύνασαι τοσαῦτα, ὅσα καὶ θέλεις
Ταῦτα λέγοντες πρὸς τὸν Κύ ριον ἐπιμείνατε, ἵνα εἴπῃ καὶ πρὸς ὑμᾶς ὁ
Σωτήρ· Πι στεύετε, ὅτι δύναμαι τοῦτο ποιῆσαι; Κατὰ τὴν πίστιν ὑμῶν γενηθήτω
ὑμῖν. Αὐτῷ ἡ δόξα εἰς τοὺς αἰῶνας τῶν αἰώνων. Ἀμήν.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

8

