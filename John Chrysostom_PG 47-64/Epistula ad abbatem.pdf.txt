Epistula ad abbatem
Τοῦ ἐν ἁγίοις πατρὸς ἡμῶν Ἰωάννου τοῦ Χρυσοστόμου ἐπιστολὴ πρός τινα
ἡγούμενον αἰτήσαντα παρ' αὐτοῦ πεμφθῆναι αὐτῷ κανόνα πνευματικῆς
διδασκαλίας εἰς ὠφέλειαν αὐτοῦ τε καὶ τῶν μετ' αὐτοῦ ἀδελφῶν.
Ἐπειδὴ ἔγραψάς μοι, ποθεινότατέ μου ἀδελφέ, ἵνα κανόνα σοι παραδώσω
ἀσκητικόν, τί σοι γράψω οὐκ οἶδα. ∆υνατὸς δέ ἐστιν ὁ Κύριος τῆς δόξης Χριστὸς Ἰησοῦς
γενέσθαι σοι ὁδηγός, ἐὰν γνησίως προσενέγκῃς ἑαυτὸν μετὰ τῶν ἀδελφῶν Κυρίῳ τῷ
Θεῷ σου, ἐὰν θέλῃς πατρίδα, γένος, ὕπαρξιν καὶ ὅλον ὁμοῦ τὸν κόσμον ἀθλητικῶς
ἀποδύσασθαι–ὁ γὰρ μὴ ἀποτασσόμενος πᾶσι τοῖς ὑπάρχουσιν αὑτοῦ καὶ ἀποθνῄσκων
ὅλῳ τῷ κόσμῳ οὐ δύναται τοῦ Κυρίου γενέσθαι μαθητής–, ἐὰν ἀγαπήσῃς Κύριον τὸν
Θεόν σου ἐξ ὅλης τῆς ψυχῆς σου καὶ τὸν πλησίον σου ὡς ἑαυτόν, ἐὰν τὴν παρθενίαν σου
ἀμέμπτως φυλάξῃς σώματι καὶ ψυχῇ ἕως τέλους, ἐὰν ἔλεον καὶ συμπάθειαν κτήσηται
ἕκαστος ὑμῶν ἐν τοῖς τῆς καρδίας ἀγγείοις καὶ ἑτοιμάσῃ ἑαυτὸν μετὰ λαμπάδων
φαιδρῶν εἰς ἀπάντησιν τοῦ ἐπουρανίου νυμφίου, ἵνα μὴ ἐν τῇ ὥρᾳ ἐκείνῃ σβεσθῇ ἡ
λαμπὰς ἑκάστου μὴ ἔχουσα ἔλαιον. Τί γὰρ ὠφελήσει ἄνθρωπος, ἐὰν ἁγνὸν φυλάξῃ τὸ
σῶμα, τὸ δὲ ἔσωθεν τῆς παροψίδος, τουτέστιν ἡ καρδία, γέμει πονηρίας, μίσους,
μνησικακίας, θυμοῦ, ζήλου, φθόνου, ὑπερηφανείας; Ὁ γὰρ ὀργιζόμενος τῷ ἀδελφῷ ἔτι
ἐν τῇ σκοτίᾳ περιπατεῖ καὶ ἀνθρωποκτόνος ἐστί, λέγει γὰρ ἡ Γραφή. Εἰ δέ τις νομίζει ὅτι
ἀκαίρως γράφω, ἀποβλέψῃ εἰς τὴν παραβολὴν τῶν δέκα παρθένων. Τὰς μὲν πέντε
φρονίμους, τὰς δὲ πέντε μωρὰς ὠνόμασεν ἡ θεία Γραφή, οὐχὶ πόρνας, ἀλλ' οὐδὲν αὐτὰς
ὠφέλησεν ἡ ἔξωθεν παρθενία. Ἀντὶ γὰρ ἐλέου καὶ φιλαδελφίας, ἰοῦ καὶ πονηρίας
ἐπεπλήρωτο ἡ καρδία αὐτῶν καὶ διὰ τοῦτο ἐξεβλήθησαν ἀπὸ τοῦ ἐπουρανίου ἐκείνου
καὶ ἀρρήτου νυμφῶνος κράζουσαι· Κύριε, Κύριε, ἄνοιξον ἡμῖν. Καὶ οὐκέτι οὐδὲν
ὠφελήθησαν παρὰ τοῦ Κυρίου τῆς δόξης, ἀλλὰ μόνον τὸ οὐκ οἶδα ὑμᾶς ἤκουσαν· οὐκ
ἔστι γὰρ μετὰ θάνατον ἐξομολόγησις μετανοίας, ἀλλὰ ἀπαραίτητος κόλασις. Ὧδε γὰρ
δέησις, ὧδε λύσις κακῶν· καὶ ὃ ἐὰν δήσῃ ἕκαστος φορτίον, τοῦτο καὶ βαστάσει.
Παρακαλῶ οὖν ὑμᾶς, τέκνα μου καὶ ἀδελφοί, ὑποτάγητε τῷ Κυρίῳ καὶ ἀλλήλοις ἐν
φόβῳ Χριστοῦ. Καὶ ἕκαστος ὑμῶν προτιμησάτω τὸν ἀδελφόν· ὁ γὰρ δοξάζων τὸν
ἀδελφὸν Ἰησοῦν δοξάζει, ὁ δὲ ἀτιμάζων τὸν ἀδελφὸν τὸν Κύριον ἀθετεῖ τὸν εἰπόντα·
ἐφ' ὅσον οὐκ ἐποιήσατε ἑνὶ τούτων τῶν ἐλαχίστων οὐδὲ ἐμοὶ ἐποιήσατε.
Παρακαλῶ οὖν ὑμᾶς, τέκνα, ὑποτάγητε τῷ ἡγουμένῳ, ὡς αὐτῷ τῷ Χριστῷ, καὶ
ἀλλήλοις, ἵνα μή τις ἐξ ὑμῶν ὡς Ἰούδας εὑρεθῇ, ἀλλ' ὡς οἱ ἀπόστολοι ὑποτασσόμενοι
τῷ Κυρίῳ· οἱ γὰρ ἀνθεστηκότες κρῖμα ἑαυτοῖς λήψονται. Οὐ κατακρίνων δὲ ταῦτα λέγω,
ἀλλὰ νουθετῶν παρακαλῶ. Παρακαλῶ οὖν ὑμᾶς, τέκνα, μή τις ἐξ ὑμῶν ἀντίλογος ἢ
ὑβριστὴς ἢ φλύαρος ἢ ἀργολόγος ἢ κατάλαλος ἢ γογγυστής. Ταῦ τα γὰρ πάντα καὶ τὰ
τούτοις ὅμοια τοῦ διαβόλου εἰσὶ καρποὶ καὶ ἐκ τοῦ περισσεύματος τῆς καρδίας λαλεῖ τὸ
στόμα. Οὔτε γὰρ τὴν ἀδελφὴν Μωσέως καὶ Ἀαρὼν ἡ παρθενία εὐηργέτησεν, ἀλλ'
ἐλεπρώθη, ἐπειδὴ ἐξήμαρτεν εἰς τὸν μείζονα ἀδελφὸν αὑτῆς, ὡς ἡ Γραφὴ Μωϋσέως
διδάσκει. Παρακαλῶ οὖν ὑμᾶς, τέκνα, ὑπακοὴν κτήσασθε χωρὶς ἀντιλογίας. Ἡ γὰρ
ὑπακοὴ μήτηρ ἐστὶ τῆς αἰωνίου ζωῆς, ἡ δὲ παρακοὴ μήτηρ ἐστὶ τῆς ἀπωλείας.
Παρακαλῶ οὖν ὑμᾶς, τέκνα, ἀγαπήσατε ἀλλήλους διὰ τὸν Κύριον τὸν εἰπόντα ὅτι· ἐὰν
ἀγαπᾶτέ με τὰς ἐντολάς μου τηρήσατε καὶ ἐν τούτῳ, φησί, γνώσονται πάντες οἱ
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

ἄνθρωποι ὅτι ἐμοὶ μαθηταί ἐστε, ἐὰν ἀγαπᾶτε ἀλλήλους. Ὅπου γὰρ ἀγάπη ἐκεῖ Χριστὸς
κατοικεῖ· Χριστὸς γὰρ ἀγάπη ἐστίν. Ὅπου δὲ μῖσος καὶ τὰ τοιαῦτα, ὅσα προείπομεν, ἐκεῖ
Χριστὸς οὐκ ἔστιν, ἀλλὰ πονηρὸς δρά κων ἐκεῖ αὐλίζεται. Καλὸν μὲν οὖν ἐστιν
ἀσφαλίσασθαι, ἵνα μὴ φιλονεικία ἢ μάχη ἀνακύψῃ μεταξὺ ὑμῶν. Ἐὰν δὲ καὶ ἡττηθῇ τις
ἐξ ὑμῶν, εὐθέως προσπέσῃ μετὰ δακρύων τῷ ἀδελφῷ, ᾧ ἐλύπησε, καὶ διαλλαγῇ αὐτῷ ἐξ
ὅλης καρδίας, ὡς ὁ Κύριος ὁρᾷ· ἄνθρωπος γὰρ εἰς πρόσωπον, Θεὸς δὲ εἰς καρδίαν. Ἐὰν
γὰρ μὴ ἀφῆτε τοῖς ἀνθρώποις τὰ παραπτώματα αὐτῶν, εἶπεν ὁ Κύριος, οὐδὲ ὁ πατὴρ
ὑμῶν ὁ οὐράνιος ἀφήσει τὰ παραπτώματα ὑμῶν· καὶ ἄφετε καὶ ἀφεθήσεται ὑμῖν· καὶ ὁ
ἥλιος μὴ ἐπιδυέτω ἐπὶ τῷ παροργισμῷ ὑμῶν· οὐ γὰρ οἴδαμεν τί τέξεται ἡ ἐπιοῦσα καὶ
οὐκ οἴδαμεν ποίᾳ ὥρᾳ ὁ κλέπτης ἔρχεται. Ἀδύνατον γὰρ μνησίκακον ψυχὴν εἰς θύραν
ζωῆς εἰσελθεῖν. Ἐὰν πᾶσαν δικαιοσύνην ποιήσῃ, βδέλυγμά ἐστιν ἐνώπιον Κυρίου καὶ
ματαία πᾶσα ἡ ἐργασία τοῦ τοιούτου ἀνθρώπου. Καὶ λοιπὸν ἕκαστος εἴτε δικαίως εἴτε
ἀδίκως ἐπιτιμηθῇ ἢ ὀνειδισθῇ ἢ ὑβρισθῇ, ἑαυτὸν ἐχέτω αἴτιον καὶ μὴ τὸν ἀδελφόν. Καὶ
ὁ μὴ ὑπομένων νῦν τότε θλίβεται, ὅτι οὐχ ὑπέμεινεν, ἀλλὰ ἐξαπώλεσε τὸν μισθὸν
αὑτοῦ. Ποία γὰρ χάρις, λέγει Κύριος, ἐὰν ἀγαπᾶτε τοὺς ἀγαπῶντας ὑμᾶς; Ἀλλὰ ἀγαπᾶτε
τοὺς ἐχθροὺς ὑμῶν καὶ εὔχεσθε ὑπὲρ τῶν ἐπηρεαζόντων ὑμᾶς, εὐλογεῖτε τοὺς
διώκοντας ὑμᾶς, εὐλογεῖτε καὶ μὴ καταρᾶσθε. Καὶ ἀπὸ τοῦ αἴροντος τὰ σὰ μὴ ἀπαίτει.
Καὶ ἐὰν τίς σε ῥαπίσῃ εἰς τὴν δεξιὰν σιαγόνα, πάρεχε αὐτῷ καὶ τὴν ἄλλην. Καὶ μακάριοί
ἐστε, ὅταν ὀνειδίσωσιν ὑμᾶς καὶ διώξωσι καὶ εἴπωσι πᾶν πονηρὸν ῥῆμα καθ' ὑμῶν
ψευδόμενοι ἕνεκεν ἐμοῦ. Χαίρετε καὶ ἀγαλλιᾶσθε ὅτι ὁ μισθὸς ὑμῶν πολὺς ἐν τοῖς
οὐρανοῖς. Οὐκ εἶπεν ὅτι χολᾶτε, ἀλλὰ χαίρετε. Οὐκοῦν τῇ ἐντολῇ τοῦ Κυρίου προσέχειν
δεῖ καὶ αὐτῇ ὑποτάσσεσθαι μετὰ βίας καὶ μακροθυμίας πολλῆς. Ἐὰν γὰρ μὴ ᾖ βία καὶ
μακροθυμία, ποῖος μισθός; Οἱ γὰρ ἀδικοῦντες ὑμᾶς καὶ συκοφαντοῦντες οὗτοι μισθὸν
οὐράνιον προξενοῦσιν ὑμῖν καὶ τὰς ψυχὰς ὑμῶν εὐεργετοῦσι.
Συνεργεῖ γὰρ τῇ ἀκοῇ τῶν καλῶν προαίρεσις οὐκ ἀγαθή· ὅσον γὰρ σπουδάζει ὁ
διά βολος διὰ τῶν ἀμελεστέρων ἀδελφῶν ἢ ἑτέραις μεθοδίαις θανατῶσαι τὴν ψυχήν,
τοσοῦτον παρασκευάζει τὸν σπουδαῖον προστρέχειν καὶ προσκολλᾶσθαι τῷ Θεῷ. Καὶ
λοιπὸν κατὰ μέρος ὑπὸ Κυρίου βοηθουμένη καὶ δυναμουμένη ἀνωτέρα γίνεται τῶν
παθῶν καὶ οὕτω κομίζεται τὸν μισθὸν παρὰ τοῦ Κυρίου καὶ εὑρίσκεται ὁ διάβολος, ὡς
οὐ θέλει, εὐεργέτης γινόμενος αὐτῆς ἕως θανάτου ἀνταγωνιζομένης τῆς καρδίας.
Λοιπὸν ὁ ἔχων ὦτα ἀκούειν, ἀκουέτω. Παρακαλῶ οὖν, τέκνον μου καὶ ἀδελφέ, ἐπειδὴ
πάλιν μοι πρὸς σὲ ὁ λόγος, ἐὰν ἁμάρτῃ ὁ ἀδελφός σου εἰς σὲ ἢ εἰς τὸν πλησίον σου ἢ εἰς
ἕτερόν τι εὑρεθῇ σφάλμα καὶ θέλεις αὐτὸν διορθώσασθαι, πρῶτον ἀσφαλίζου τὸν ἔσω
σου ἄνθρωπον, τουτέστι τὴν καρδίαν σου καὶ φύλαξον αὐτὴν ἀτάραχον μετὰ τῆς
μνήμης τοῦ Κυρίου Ἰησοῦ Χριστοῦ, ἵνα μὴ ἄλλον θέλων διορθώσασθαι τὴν ψυχήν σου
διαφθείρῃς ἐν τῇ ὀργῇ–κακία γὰρ κακίαν οὐκ ἐκβάλλει, ἀλλὰ νίκα ἐν τῷ ἀγαθῷ τὸ
κακόν–καὶ πρὸς τὸ σφάλμα τὴν διόρθωσιν ἐπάγαγε, μὴ διὰ ὕβρεων ἢ πληγῶν, ἀλλὰ διὰ
νηστείας, εἰ δέοι, καθὼς δοκιμάσῃς μετὰ τοῦ Κυρίου, τοῦτο εἰδώς, ὅτι χωρὶς αὐτοῦ
οὐδὲν δύνασαι. Παρακάλει οὖν αὐτὸν ἐν παντὶ καιρῷ, ἵνα αὐτός σοι γένηται ὀφθαλμὸς
καὶ χαρίσηταί σοι σύνεσιν καλοῦ τε καὶ κακοῦ. Ἐὰν δὲ προσπέσῃ μετανοῶν καὶ οὕτω
δέξαι αὐτόν· καλὴ γὰρ ἡ συγκατάβασις. Καὶ οὕτω, νομίζω, ἐπὶ συμπάθειαν οὐκ οἶδεν
ἀγανακτεῖν ὁ Κύριος, ἀλλ' ἐπὶ τῇ ἀποτομίᾳ ὀργίζεται. Ὃ καὶ ἐπὶ τῷ χρεωφειλέτῃ τῶν
μυρίων ταλάντων εὑρίσκομεν, τῶν τοσούτων κακῶν τὴν ἄφεσιν, καὶ αὐτὸς
παρακαλούμενος ὑπὸ τοῦ συνδούλου αὑτοῦ οὐκ ἀφῆκε τὴν ὀφειλὴν τῶν ἑκατὸν
δηναρίων καὶ διὰ τοῦτο κατεδικάσθη εἰς τὸ ἐξώτερον σκότος. Ἐὰν δὲ τοῖς αὐτοῖς ἐμμένῃ
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

τις, περὶ τούτου γνώμην δοῦναι οὔτε τολμῶ οὔτε δύναμαι. Λοιπόν, εἴ τι δοκιμάσεις ἐν
Κυρίῳ, τοῦτο πράττε· καλὸν γὰρ τὸ κερδῆσαι ψυχήν· ὁ γὰρ ἐπιστρέψας ἁμαρτωλὸν ἐκ
πλάνης ὁδοῦ αὐτοῦ σώσει ψυχὴν ἐκ θανάτου καὶ καλύψει πλῆθος ἁμαρτιῶν αὑτοῦ.
Παρακαλῶ οὖν ὑμᾶς, τέκνα, διὰ τοῦ Κυρίου τὴν ταπεινοφροσύνην ἀγαπήσατε, ἣν
μάλιστα εἶπεν ὁ Χριστός, ὁ ταπεινώσας ἑαυτὸν δι' ἡμᾶς μέχρι θανάτου, θανάτου δὲ
Σταυροῦ, ἵνα λοιπὸν καὶ ἡμεῖς μιμηταὶ αὐτοῦ γενώμεθα καὶ οὕτω νικήσωμεν τὴν
ὑπερηφάνειαν τοῦ διαβόλου διὰ τῆς ταπεινοφροσύνης· καὶ ὁ Κύριος διὰ τὴν ταπείνωσιν
εἶπεν ὅτι ὁ θέλων ἐν ὑμῖν εἶναι μέγας ἔστω πάντων ἔσχατος καὶ πάντων δοῦλος καὶ
πάντων διάκονος· ὁ γὰρ ὑψῶν ἑαυτὸν ταπεινωθήσεται, ὁ δὲ ταπεινὸς οὐδαμόθεν
δύναται καταπεσεῖν ὑποκάτω πάντων κείμενος, ὁ δὲ ὑψῶν ἑαυτὸν κάτω φέρεται εἰς τὴν
ἄβυσσον τοῦ πυρός. Παρακαλῶ οὖν ὑμᾶς, τέκνα, ἐλεήσατε ἑαυτούς. Παρακαλῶ ὑμᾶς
κλαύσατε, ὡς ἔχετε καιρόν, ἵνα μὴ ἐκεῖ κλαύσωμεν, ὅπου ὁ βρυγμὸς τῶν ὀδόντων, ὅπου
ὁ σκώληξ ὁ ἀκοίμητος οὐδέποτε τελευτᾷ, ὅπου τὸ πῦρ τὸ ἄσβεστον ποταμηδὸν διοδεύει,
ὅπου ἄσπλαγχνοι καὶ ἀνελεήμονές εἰσιν ἄγγελοι. Οὐκ ἔχουσιν ἐξουσίαν ἐλεῆσαί τινα ἢ
ἐντραπῆναι γέροντα ἢ φείσασθαί τινος ἢ τιμῆσαι βασιλέα, ἀλλ' ἕκαστον ἀφειδῶς
τιμωρήσασθαι κατὰ τὰς πράξεις αὐτοῦ. Παρακαλῶ οὖν ὑμᾶς, βιάσασθε τὸν νῶτον ἕως
θανάτου.
Πολλῆς βίας ἔχει χρείαν τοῦτο τὸ ἔργον, ὅτι στενὴ καὶ τεθλιμμένη ἡ ὁδὸς ἡ
ἀπάγουσα εἰς τὴν θύραν τῆς ζωῆς καὶ οἱ βιαζόμενοι εἰσέρχονται ἐν αὐτῇ· βιαστῶν γάρ
ἐστιν ἡ βασιλεία τῶν οὐρανῶν, λέγει Κύριος. Φεύγετε δὲ τὸ τῆς κενοδοξίας πνεῦμα καὶ
τὸν τῆς ἀνθρωπαρεσκείας δαίμονα, ὃ μάλιστα καὶ χωρίζει ἀπὸ τοῦ Θεοῦ· διεσκόρπισε
γὰρ ὁ Θεὸς ὀστᾶ ἀνθρωπαρέσκων, κατῃσχύνθησαν ὅτι ὁ Θεὸς ἐξουδένωσεν αὐτούς. Ἢ
ὧδε ἔχομεν δοξασθῆναι καὶ ἀπέχομεν ἐκεῖ τὸ χρέος εἰς τὴν γέενναν τοῦ πυρὸς εἰς
ἀτελευτήτους αἰῶνας ἢ ὧδε ἔχομεν ἀτιμασθῆναι καὶ ἐκεῖ ἔχομεν ἀπολαβεῖν τὸν μισθὸν
εἰς αἰῶνα αἰῶνος. Παρακαλῶ οὖν ὑμᾶς μή τις ἐξ ὑμῶν φλύαρος ἢ ἀργολό γος ἢ
κατάλαλος ἢ ἐφευρετὴς ἢ γογγυστής–ταῦτα γὰρ τὰ πάθη οὐκ ἐῶσι τὴν ψυχὴν τὴν δοκὸν
τὴν ἐν τῷ ἰδίῳ ὀφθαλμῷ ἰδεῖν, ἀλλὰ τὸ κάρφος τὸ ἐν τῷ ὀφθαλμῷ τοῦ ἀδελφοῦ–
ἑαυτοῖς ἀρέσκοντες, οὐχὶ τῷ Θεῷ, ὧν τὸ τέλος ἀπώλεια μετὰ τοῦ Φαρισαίου καὶ αὐτοῦ
τοῦ φρονήματος. Ἀγαπήσατε δὲ τὴν κατὰ Θεὸν θλῖψιν ὑπὲρ τὴν χαρὰν τοῦ αἰῶνος
τούτου καὶ τὸν κλαυθμὸν ὑπὲρ τὸν γέλωτα· μακάριοι γὰρ οἱ πενθοῦντες, εἶπεν ὁ Κύριος,
ὅτι αὐτοὶ παρακληθήσονται. Καὶ οὐαὶ ὑμῖν τοῖς γελῶσιν, ὅτι κλαύσετε. Φεύγετε τοὺς
θέλοντας ὑμᾶς ἀπασχολεῖν σοφιζομένους τινὰς οὐδὲν χρήσιμον ἀπὸ πολλῶν
ἀναγνωσμάτων ἐπὶ διαστροφῇ τῶν ἀκουόντων. Καὶ μὴ θηλάζετε ἰὸν ἀσπίδος διὰ τῆς
ἀκοῆς καὶ μὴ θελήσητε πολλὰ ἀναγινώσκειν ἀλλ' ὀλίγα καὶ καλῶς, ἵνα δῷ χάριν τοῖς
ἀκούουσι. Μάθετε δὲ μᾶλλον πολλὰ προσεύχεσθαι καὶ πολλὰ κλαίειν· τοῦτο γὰρ
στηρίζει τὸν νοῦν καὶ τρέφει τὴν ψυχήν, τὸ δὲ πολλὰ ἀναγινώσκειν μετεωρίζει τὸν
νοῦν. Μισήσατε δὲ τὰς πολλὰς συντυχίας καὶ τοὺς κατὰ σάρκα συγγενεῖς καὶ μὴ
ἐπιστραφῆτε πρὸς φίλτρον γονέων ἢ τέκνων ἢ ἄλλων τινῶν, ἀλλ' ὡς πνευματικοὺς
αὐτοὺς ὁρᾶτε ὡς καὶ τοὺς λοιπούς. Καὶ μὴ θελήσητέ ποτε μυστήριον τῆς μονῆς σαρκικῷ
συγγενεῖ ἐκφάναι, ἵνα μὴ κατακριθῆτε ἐν ἡμέρᾳ κρίσεως· ἀπεθάνετε γὰρ τῷ κόσμῳ καὶ
οὐδὲν πρᾶγμα ἔχετε μετὰ τοῦ κόσμου. Λέγει γὰρ ὁ Κύριος διὰ τοῦ Εὐαγγελίου, ὁπότε
ἦλθεν ἡ μήτηρ αὐτοῦ καὶ οἱ ἀδελφοὶ αὐτοῦ ἰδεῖν αὐτόν· καὶ ἐκτείνας τὴν χεῖρα αὐτοῦ
ἐπὶ τοὺς μαθητὰς αὐτοῦ λέγων· οὗτοί εἰσιν ἡ μήτηρ μου καὶ οἱ ἀδελφοί μου οἱ ποιοῦντες
τὸ θέλημα τοῦ πατρός μου τοῦ ἐν οὐρανοῖς. Καὶ πάλιν· ἔξελθε ἐκ τῆς γῆς σου καὶ ἐκ τῆς
συγγενείας σου καὶ δεῦρο εἰς γῆν, ἣν ἄν σοι δείξω καὶ τὰ λοιπά, ὅσα λέγει Κύριος. Ταῦτα
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

3

δὲ ἐγράφη εἰς νουθεσίαν ἡμετέραν, ὁ δὲ βουλόμενος τῷ Θεῷ εὐαρεστῆσαι καὶ τὸ θέλημα
τοῦ Κυρίου ποιῆσαι δυσὶ κυρίοις οὐ δουλεύει οὐδὲ ἀγαπᾷ ἄλλον τινὰ ὑπὲρ τὸν Κύριον.
Παρακαλῶ οὖν ὑμᾶς, μηδὲν προτιμήσητε τοῦ Κυρίου μηδὲ τῆς ψυχῆς ὑμῶν. Τί γὰρ
ὠφελήσει ἄνθρωπος, ἐὰν ἀπολέσῃ τὴν ψυχὴν αὐτοῦ, λέγει Κύριος, ἢ τί δώσει
ἀντάλλαγμα ὑπὲρ τῆς ψυχῆς αὐτοῦ; Οὐδὲν γὰρ εἰσηνέγκαμεν εἰς τὸν κόσμον, δῆλον ὅτι
οὐδὲ ἐξενεγκεῖν τι δυνάμεθα, ἀλλὰ γυμνοὶ ἔχομεν ἀναστῆναι μόνας τὰς πράξεις καὶ τὰ
ἔργα ἐνδεδυμένοι, εἴτε καλὰ εἴτε φαῦλα, ὥσπερ ἱμάτιον. Καὶ οὐ μόνον ἔργων, ἀλλὰ καὶ
ῥημάτων καὶ ἐνθυμημάτων καὶ οὕτως ἐπὶ τῷ φοβερῷ ἐκείνῳ παραστησόμεθα βήματι.
Καὶ ἕκαστος λοιπὸν τῶν ἰδίων κόπων ἔχει ἀπολαῦσαι κατὰ τὴν τοῦ Κυρίου δικαίαν
ἀπόφασιν εἴτε εἰς ἀνάπαυσιν εἴτε εἰς κόλασιν εἰς αἰῶνα αἰῶνος καὶ μακάριοι οἱ
κλαίοντες ἐν δάκρυσιν ὀδυνηροῖς ἀπὸ καρδίας ὅτι χαρήσονται τότε, μακάριοι οἱ
ἀγαπῶντες τὸν Κύριον καὶ τὸν πλησίον ὅτι αὐτοὶ ἐλεηθήσονται· ὁ γὰρ ἀγαπῶν τὸν
ἀδελφὸν ἑαυτὸν ἀγαπᾷ καὶ ὁ μισῶν τὸν ἀδελφὸν ἑαυτὸν μισεῖ· ἐν τῷ ἀδελφῷ γὰρ κεῖται
τὰ αἰώνια ἀγαθὰ κληρονομῆσαι. Μιμηταὶ οὖν γενώμεθα τῶν ἁγίων. Καθὼς αὐτοὶ
παρῆλθον ἐν πείνῃ καὶ δίψῃ, ἐν ψύχει καὶ γυμνότητι καὶ διὰ πυρὸς καὶ ξίφους, οἳ καὶ
διὰ πάσης θλίψεως προθύμως παρῆλθον καὶ ἡμεῖς οὖν ἕως θανάτου μιμησώμεθα. Ἐὰν
δὲ θελήσητε ἀναιδῶς ζῆσαι, τοῦτο θέλημα τοῦ Θεοῦ οὐκ ἔστιν.
Ἀδιαλείπτως προσεύχεσθε χωρὶς ὀργῆς καὶ διαλογισμῶν· πᾶς γὰρ λογισμὸς
χωρίζων τὸν νοῦν ἀπὸ τοῦ Θεοῦ, εἰ καὶ δοκεῖ ἀγαθὸς εἶναι, ὅλως διάβολός ἐστιν, ἵνα μὴ
εἴπω τοῦ διαβόλου ἐστίν. Ἵνα μόνον ἀποπλανήσῃ τὸν νοῦν ἀπὸ τοῦ Θεοῦ καί φησι καὶ
ἐντολὰς καὶ εὐποιίας ὑπαγορεύει ἔσωθεν ἐν τῇ καρδίᾳ καὶ ἄλλας τινὰς εὐ λόγους καὶ
ἀλόγους φαντασίας, αἷς οὐδὲ προσέχειν ὅλως προσήκει. Ὅλος γὰρ ὁ ἀγὼν τοῦ διαβόλου
ἐστὶν ἀποχωρίσαι καὶ ἀποβουκολῆσαι τὸν νοῦν ἀπὸ τοῦ Θεοῦ καὶ εἰς τὰς κοσμικὰς
περισύρειν ἡδονάς. Καὶ ὅλος ὁ ἀγών ἐστι τῆς ψυχῆς τοῦ μὴ χωρίζειν τὸν νοῦν ἀπὸ τοῦ
Θεοῦ μηδὲ συνδυάζειν καὶ συμφωνεῖν τοῖς ἀκαθάρτοις λογισμοῖς μηδὲ προσέχειν οἷς
εἰκονογραφεῖ ἐν τῇ καρδίᾳ ὁ πανταμίμητος καὶ παλαιὸς ζωγράφος διάβολος, ποτὲ μὲν
τύπους, ποτὲ δὲ τρόπους, ποτὲ δὲ πρόσωπα καὶ σχήματα. Μετὰ δὲ ταῦτα πάντα ὁ
διάβολος ἀπομορφοῦται καὶ ὁ ἄθλιος ἄνθρωπος ἑνὶ τόπῳ ἑστὼς νομίζει ἀλλαχοῦ ποῦ
ποτε εἶναι ἀπατώμενος καὶ δοκεῖ βλέπειν τινὰς καὶ προσώποις λαλεῖν καὶ διατάσσεσθαι
πράγματα ἅπερ πάντα πλάνης ἐστὶ διαβολικῆς. Χρὴ οὖν ἀσφαλίζεσθαι καὶ ἡνιοχεῖν τὸν
νοῦν καὶ χαλιναγωγεῖν αὐτὸν καὶ πάντα λογισμὸν καὶ πᾶσαν ἐνέργειαν κολάζειν διὰ
τοῦ ὀνόματος τοῦ Κυρίου ἡμῶν Ἰησοῦ Χριστοῦ τοῦ αἴροντος τὴν ἁμαρτίαν τοῦ κόσμου.
Ὅπου τὸ σῶμα ἵσταται ἐκεῖ καὶ ὁ νοῦς ἔστω, ἵνα μέσον τοῦ Θεοῦ καὶ ἀναμέσον τῆς
καρδίας μηδὲν ἕτερον εὑρίσκηται ἢ ὥσπερ μεσότοιχον ἢ ὡς φραγμὸς ἐπισκοτῶν τὴν
καρδίαν, εἰ μὴ μόνον λάλει τῷ Κυρίῳ. Ἐὰν δὲ καί ποτε συναρπάσῃ τὸν νοῦν, οὐ χρὴ τοῖς
λογισμοῖς ἐγχρονίζειν, ἵνα μὴ ἡ συγκατάθεσις τῶν λογισμῶν εἰς πρᾶξιν αὐτῷ λογισθῇ
ἐνώπιον Κυρίου ἐν ἡμέρᾳ κρίσεως, ὅτε κρινεῖ ὁ Θεὸς τὰ κρυπτὰ τῶν ἀνθρώπων καὶ ὅτε
ἐνθύμιον ἀνθρώπου ἐξομολογήσεταί σοι. Ἀδύνατον γὰρ ἐπιτυχεῖν τῆς βασιλείας, ἐὰν μὴ
πρῶτον ἀρνήσηται τὸ ἑαυτοῦ θέλημα καὶ τὰ ἐπιτασσόμενα αὐτῷ ὑπὸ τοῦ ἡγουμένου
ἀγογγύστως ποιῇ μετὰ φόβου Θεοῦ, καθὼς ὁ Κύριος λέγει· οὐκ ἦλθον ποιῆσαι τὸ
θέλημα τὸ ἐμόν, ἀλλὰ τὸ θέλημα τοῦ πέμψαντός με πατρός· ὃ οὖν οὐ θέλει τις, ἐὰν
πάσχῃ καὶ ὑπομένῃ, σταύρωσις αὐτῷ λογίζεται καὶ γίνεται τέκνον τῆς ἀναστάσεως καὶ
τῆς αἰωνίου ζωῆς. Μακάριος ἄνθρωπος, ὃς ὑπομένει πειρασμόν, ὅτι δόκιμος γενόμενος
λήψεται τὸν ἀμαράντινον στέφανον καὶ γενήσεται ναὸς τοῦ μεγάλου βασιλέως
Χριστοῦ· καὶ ἐνοικήσω καὶ ἐμπεριπατήσω, λέγει Κύριος, καὶ μονὴν παρ' αὐτῷ
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

4

ποιησόμεθα. Τοιαύτας οὖν ἔχοντες ἐπαγγελίας σχολάσατε διὰ παντὸς καὶ παραμείνατε
Κυρίῳ τῷ Θεῷ ἡμῶν, ἕως οὗ οἰκτειρήσῃ ὑμᾶς, καὶ μηδὲν ἕτερον ζητήσητε, εἰ μὴ μόνον
ἔλεος παρὰ Κυρίου τῆς δόξης καὶ ἀρκεῖ ὑμῖν. Ζητοῦντες δὲ ἔλεος ἐν ταπεινῇ καὶ ἐλεεινῇ
καρδίᾳ ζητεῖτε. Λοιπὸν βοᾶτε ἀπὸ πρωῒ ἕως ἑσπέρας, εἰ δυνατόν, καὶ ὅλην τὴν νύκτα
λέγοντες ἀπαύστως· Κύριε Ἰησοῦ Χριστέ, υἱὲ τοῦ Θεοῦ ἐλέησον· Κύριε Ἰησοῦ Χριστέ, υἱὲ
τοῦ Θεοῦ ἐλέησον· Κύριε Ἰησοῦ Χριστέ, υἱὲ τοῦ Θεοῦ ἐλέησον ἡμᾶς. Ἀμήν. Παρακαλῶ
οὖν ὑμᾶς, βιάσασθε, πάλιν λέγω βιάσασθε τῷ νῷ ὑμῶν ἕως θανάτου. Πολλῆς γὰρ βίας
χρείαν ἔχει τὸ ἔργον τοῦτο, ὅτι στενή ἐστι καὶ τεθλιμμένη ἡ ὁδὸς ἡ ἀπάγουσα εἰς τὴν
θύραν τῆς ζωῆς καὶ οἱ βιαζόμενοι εἰσέρχονται ἐν αὐτῇ· βιαστῶν γάρ ἐστιν ἡ βασιλεία
τῶν οὐρανῶν. Παρακαλῶ οὖν ὑμᾶς μὴ χωρίζεσθε ἀπὸ τοῦ Θεοῦ, μηδὲ τῆς καρδίας ὑμῶν,
ἀλλὰ παραμένετε καὶ φυλάττετε αὐτὴν μετὰ τῆς μνήμης τοῦ Κυρίου Ἰησοῦ Χριστοῦ·
αὐτὸ καὶ αὐτὸ πάντοτε, ἕως οὗ ἐμφυτευθῇ τὸ ὄνομα τοῦ Κυρίου ἔσω ἐν τῇ καρδίᾳ, καὶ
μηδὲν ἕτερον, ἄχρις οὗ μεγαλυνθῇ Χριστὸς ἐν ὑμῖν.
Παρακαλῶ οὖν ὑμᾶς τὸν κανόνα ταύτης τῆς προσευχῆς μηδέποτε καταπαύσητε.
Ἤκουσα γάρ ποτε τῶν ἁγίων πατέρων λεγόντων ὅτι ποῖος ἐκεῖνος μονάζων, ἐὰν
κανόνα καταπαύσῃ, ἀλλὰ ὀφείλει εἴτε ἐσθίει εἴτε πίνει εἴτε ὁδεύει εἴτε διακονεῖ
ἀδιαλείπτως κράζειν· Κύριε Ἰησοῦ Χριστέ, υἱὲ τοῦ Θεοῦ, ἐλέησον ἡμᾶς, ἵνα αὐτὴ ἡ
μνήμη τοῦ ὀνόματος τοῦ Κυρίου Ἰησοῦ Χριστοῦ ἐρε θίσῃ πρὸς πόλεμον τοῦ ἐχθροῦ.
Πάντα γὰρ διὰ τῆς μνήμης τοῦ Κυρίου ἔχει εὑρεῖν ἡ βιαζομένη ψυχὴ εἴτε πονηρὰ εἴτε
ἀγαθά· πρῶτον δὲ τὰ κακὰ ἔχει ἰδεῖν ἔσω ἐν τῇ καρδίᾳ καὶ τότε τὰ καλά. Ἡ γὰρ μνήμη
ἔχει κινῆσαι τὸν δράκοντα καὶ ἡ μνήμη ἔχει αὐτὸν ταπεινῶσαι. Ἡ μνήμη ἔχει ἐλέγξαι
τὴν ἐν ὑμῖν οἰκοῦσαν ἁμαρτίαν καὶ ἡ μνήμη ἔχει αὐτὴν δαπανῆσαι. Ἡ μνήμη ἔχει
κινῆσαι πᾶσαν τὴν δύναμιν τοῦ διαβόλου ἐν τῇ καρδίᾳ καὶ ἡ μνήμη ἔχει αὐτὴν νικῆσαι
καὶ ἐκριζῶσαι κατὰ μέρος, ἵνα τὸ ὄνομα τοῦ Κυρίου Ἰησοῦ Χριστοῦ κατερχόμενον εἰς
τὸν βυθὸν τῆς καρδίας τὸν μὲν δράκοντα τὸν κρατοῦντα τὰς νομὰς ταπεινώσῃ, τὴν δὲ
ψυχὴν σώσῃ καὶ ζωοποιήσῃ, ἀδιαλείπτως παραμένον τὸ ὄνομα τοῦ Κυρίου Ἰησοῦ
Χριστοῦ κράζον, ἵνα καταπίῃ ἡ καρδία τὸν Κύριον καὶ ὁ Κύριος τὴν καρδίαν καὶ
γενήσεται τὰ δύο ἕν. Ἀλλὰ τὸ ἔργον τοῦτο οὐκ ἔστι μιᾶς ἡμέρας ἢ δύο, ἀλλὰ χρόνου
πολλοῦ καὶ καιροῦ πολλοῦ· πολλοῦ γὰρ ἀγῶνος χρεία καὶ χρόνου, ἕως οὗ ἐκβληθῇ ὁ
ἐχθρὸς καὶ ἐνοικήσῃ ὁ Χρι στός· οὐκ ἔστι γὰρ ἡμῖν ἡ πάλη πρὸς αἷμα καὶ σάρκα, ἀλλὰ
πρὸς τὰ πνευματικὰ τῆς πονηρίας, λέγει ὁ ἀπόστολος. Οἱ οὖν ἀποτασσόμενοι καὶ
σπουδάζοντες εὐαρεστῆσαι τῷ Κυρίῳ ὀφείλουσιν ἑαυτοὺς εἰς πάντα ταπεινῶσαι καὶ
θλιβῆναι, ἵνα μόνον ἐπιτύχωσι τῶν μελλόντων ἀγαθῶν καὶ τῆς ἀτελευτήτου
ἀναπαύσεως. Παρακαλῶ οὖν ὑμᾶς προσέχετε, πῶς μεταλαμβάνετε τῆς τροφῆς. Λέγει
γὰρ ὁ προφήτης· δουλεύσατε τῷ Κυρίῳ ἐν φόβῳ· τοῦτο εἶπεν ἐν καιρῷ προσευχῆς. Καὶ
ἀγαλλιᾶσθε αὐτῷ ἐν τρόμῳ· καὶ τοῦτο εἶπεν ἐν καιρῷ τροφῆς. Οὐκ εἶπεν ἐν γέλωτι καὶ
ἀδιαφο ρίᾳ, ἀλλ' ἐν φόβῳ καὶ τρόμῳ. Τίς γὰρ τολμήσει ποτὲ βασιλέως παρόντος γελάσαι
καὶ ἀγανάκτησιν ἑαυτῷ ἐπενέγκασθαι καὶ τιμωρίαν. Καί, ὅταν ψάλλητε, νηφόντως
ψάλλετε ἐν αἰσθήσει καρδίας, ἵνα γεύσηται ἡ καρδία καὶ γινώσκῃ τὰ λεγόμενα ἐν
διακρίσει εἰς δόξαν Χριστοῦ, μὴ χαύνως, ἀλλὰ ζεόντως, ἵνα καὶ ὁ νοῦς διεγείρηται καὶ
τὸ σῶμα πρὸς τὴν ἀγρυπνίαν, ἵνα πανταχόθεν ἡ ψυχὴ τρέφηται καὶ προκόπτῃ. Καὶ μὴ
θελήσητέ ποτε τροπάρια πολυσύνθετα λέγειν, ἵνα μὴ πλάνην ὑπομείνητε τερπόμενοι τῇ
φωνῇ. Καὶ περὶ τῆς διακονίας τῆς εἰς τοὺς ἀδελφοὺς πρόσεχε ποῖον κελλάριον
προχειρίζεις· τὸν δυνάμενον συνείδησιν φυλάξαι τῷ Θεῷ καὶ τῇ συνοδίᾳ, μή ποτε εἰς
κρῖμα ἐμπέσῃ καὶ παγίδα τοῦ διαβόλου ἢ λαθροφάγον ἢ νοσφιζόμενόν τι καὶ ἀπολέσῃ
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

5

τὴν ψυχὴν αὑτοῦ. Ὁμοίως καὶ τὰς διακονίας ἑκάστῳ κατὰ τὴν συνείδησιν ἐγχείριζε· οἱ
γὰρ καλῶς διακονήσαντες παρὰ Κυρίου τὸν μισθὸν λήψονται. Ταῦτα πάντα μετὰ τῆς
μνήμης ποίει τοῦ Κυρίου καὶ παρακάλει Κύριον τὸν Θεόν, ἵνα αὐτός σου γένηται ὁδηγός,
ἐφ' οἷς ἂν πράττεις· ὅπου γὰρ Χριστὸς καλεῖται καὶ ὅπου Χριστὸς παροικήσει πάντα ἅγια
ἐκεῖ, πάντα εὐλογημένα. Ἐκεῖ Χριστὸς εὐφραίνεται καὶ μακάριοι οἱ αὐτὸν πεινῶντες καὶ
οἱ αὐτὸν διψῶντες καὶ οἱ αὐτὸν τρώγοντες καὶ οἱ αὐτὸν πίνοντες καὶ οἱ αὐτὸν
ἀγαπῶντες. Τίς γὰρ ἡμᾶς χωρίσει ἀπὸ τῆς ἀγάπης τοῦ Χριστοῦ; Θλῖψις ἢ στενοχωρία ἢ
διωγμὸς ἢ κίνδυνος ἢ μάχαιρα ἢ θάνατος; Ἡ γὰρ γλυκύτης τοῦ Χριστοῦ καὶ τὸ θεϊκὸν
αὐτοῦ φίλτρον πάντων ὁμοῦ περιγίνεται μετὰ τῆς μνήμης τοῦ Χριστοῦ.
Καθὼς καὶ οἱ ἅγιοι μάρτυρες κατεφρόνησαν θανάτου διὰ τὴν ἀγάπην καὶ τὴν
γλυκύτητα τοῦ Χριστοῦ, ἵνα αὐτὸν μόνον κερδήσωσιν, καθὼς καὶ ὁ ἅγιος Παῦλος
κηρύσσει· εἴ τις οὐκ ἀγαπᾷ τὸν Κύριον Ἰησοῦν, ἤτω ἀνάθεμα, μαραναθά, τὸ δὲ
μαραναθὰ ἑβραϊστί ἐστιν· εἶδον τὸν Κύριον. Καὶ εὐθέως ὤφθη αὐτῷ ὁ Κύριος καὶ λέγει
αὐτῷ· ἐπειδὴ ἀγαπᾷς με, Παῦλε, ἰδὲ ποῦ εἰμι. Ἀπὸ χαρᾶς οὖν λέγει ὁ Παῦλος τὸ
μαραναθά, εἶδον τὸν Κύριον, ἰδὲ ἦλθεν ὁ Κύριος, ἰδὲ ποῦ ἐστιν. Παρακαλῶ οὖν ὑμᾶς
μισήσατε τὸν καλλωπισμὸν τῶν ἱματίων καὶ ἀγαπήσατε ταπεινοφροσύνην καὶ
πτωχείαν καὶ πᾶσαν θλῖψιν ἀντὶ χαρᾶς, ἵνα ἀγαπήσῃ ὑμᾶς ὁ Χριστός. Φεύγετε τὰς
συντυχίας τῶν γυναικῶν εἴτε κοσμικῶν εἴτε κανονικῶν· χαλεπὸν γὰρ καὶ ταῖς
μοναζούσαις ἀλλήλαις συντυγχάνειν. Ὁ Κύριος τῆς δόξης ἐξελεῖται ἡμᾶς ἐκ τῆς
τοιαύτης ἀγάπης. Παρακαλῶ οὖν ὑμᾶς, τέκνα, ἐν τῷ ἐσχάτῳ μου τούτῳ καὶ τελευταίῳ
κεφαλαίῳ, ἵνα ἕκαστος ὑμῶν πρὸ ὀφθαλμῶν ἔχῃ τὴν ἔξοδον αὑτοῦ τῆς ψυχῆς καθ'
ἑκάστην ἡμέραν καὶ νύκτα καὶ ὥραν· οἶδε γὰρ τὸ μέρος τοῦτο παιδεῦσαι τὴν ψυχὴν
πρὸς ἀσφάλειαν καὶ ὁδηγῆσαι πρὸς σωτηρίαν, ἵνα μὴ τὰ τῆς αὔριον μεριμνᾷ καὶ
φαντάζηται–ἀρκετὸν γὰρ τῇ ἡμέρᾳ ἡ κακία αὐτῆς–, ἵνα, ὅταν ἔλθῃ ὁ Κύριος ἐπὶ τὴν
ψυχὴν ἡμῶν ἐν ὥρᾳ ᾗ οὐκ οἴδαμεν, ἑτοίμους ἡμᾶς εὕρῃ πρὸς τὴν αἰώνιον ἐκείνην καὶ
ἀπέραντον ὁδόν. Ταῦτα ἔγραψα, τέκνον μου καὶ ἀδελφέ, ἐν πολλῇ ὀδύνῃ τῆς καρδίας
μου, οὐκ ἀπὸ σκέψεως δὲ ἢ μελέτης–μαρτυρεῖ μοι ὁ Χριστός. Ἀλλ', εἴ τι αὐτὸς
ὑπηγόρευσε διὰ τῆς πονηρᾶς μου καρδίας, τοῦτο καὶ ἔγραψα· πρᾶγμα οὐκ ἔχω. Αὐτὸς
γάρ ἐστιν, ὁ καὶ ἐν ἀλόγοις ζῴοις ἐν καιρῷ εὐθέτῳ ἀνθρωπίνην φωνὴν χαρισάμενος, ὡς
καὶ ἐν Ῥώμῃ τῷ κυνὶ ἐπὶ Πέτρου τοῦ ἀποστόλου, οὐ διὰ τὴν ἁγιότητα τοῦ κυνός, ἀλλὰ
διὰ τὴν πίστιν τῶν ἀκουόντων. Ὁ δὲ Κύριος τῆς δόξης ὁ διδάσκων ἄνθρωπον γνῶσιν
ὁδηγήσει ὑμᾶς εἰς τὸ αὑτοῦ θέλημα. Ὁ Κύριος τῆς δόξης στηρίξει τὰς ψυχὰς ὑμῶν ἐν τῇ
πίστει αὑτοῦ ἕως τέλους. Ὁ Κύριος τῆς δόξης τὴν αὑτοῦ εἰρήνην καὶ τὴν αὑτοῦ ἀγάπην
χαρίσεται ὑμῖν διὰ παντός. Ὁ Κύριος τῆς δόξης φυλάξει τὰς ψυχὰς ὑμῶν. Ὁ Κύριος τῆς
δόξης ἔσται μεθ' ὑμῶν. Ὁ βασιλεύων πρὸ τῶν αἰώνων καὶ νῦν καὶ ἀεὶ καὶ εἰς τοὺς
αἰῶνας τῶν αἰώνων. Ἀμήν.

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

6

