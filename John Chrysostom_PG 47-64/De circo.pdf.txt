De circo
Εἰς τὸ ἱπποδρόμιον λόγος.
59.567
Στάδιον ἡμῖν σήμερον πνευματικὸν καὶ δρόμος οὐράνιος ἀνέῳγε τοῖς
ἑτοίμως πρὸς τὴν θεωρίαν τοῦ πνεύματος ἔχουσιν, οὐ κατὰ τὸν ἐκ γῆς χοϊκὸν, ἀλλὰ
κατὰ τὸν τοῦ πνεύματος ἐπουράνιον ἄνθρωπον· ἐφ' ᾧ δεῖ τρέχειν μὲν κατὰ τὴν γῆν,
σκοπεῖν δὲ κατὰ τοῦ οὐρανοῦ. Τοῦτο προήχθην ἱστορῆσαι πρὸς ἔλεγχον τῶν
φιλοθεαμόνων, τῶν τὸ πνευματικὸν τῆς σωτηρίας θεώρημα παρορώντων, καὶ τὸ
σωματικὸν τῆς πονηρίας ἱππο 59.568 δρόμιον, μᾶλλον δὲ σατανοδρόμιον
προτιμούντων. Θεώρει δὲ σπουδαίως, ἀγαπητὲ, μᾶλλον τὸ τῆς Ἐκκλησίας
ἀπερίεργον ἱπποδρόμιον, ἔνθα Παῦλος ἀγωνίζεται, οὐκ εὐρίπου καὶ κάμπων
κύκλους τρέχων, ἀλλ' ἀπὸ Ἱερουσαλὴμ κύκλῳ μέχρι τοῦ Ἰλλυρικοῦ ἐκπληρῶν τὸ
Εὐαγγέλιον τοῦ Χριστοῦ· ἔνθα ὁ Ἡλίας ἡνιοχεῖ, περὶ οὗ εἴρηται· Ἅρμα, ἅρμα, ἡνίοχε
τοῦ Ἰσραὴλ, καὶ ἱππεὺς αὐτοῦ· οὐκ ἀπὸ λευκῆς ἐπὶ σφενδόνα μιλιοδρομῶν, ἀλλ' ἀπὸ
γῆς 59.569 εἰς οὐρανὸν πυρίνου ἅρματος κατατολμῶν, ἔνθα λαὸς οὐρανοπολιτῶν
ἐπὶ τὴν θεωρίαν πάρεστι, καὶ κριτὴς πνευματικὸς καθέζεται, καὶ βασιλεὺς θεωρεῖ,
καὶ ἐπινίκιον οὐράνιον δῶρον τίθεται, καὶ μάστιγες φοβερᾶς κρίσεως πρόκεινται.
Βούλεταί σε ὁ Θεὸς δι' ἑαυτοῦ ἀγωνίζεσθαι.
∆ιὸ καὶ δέδωκέ σοι τὸ τῶν Εὐαγγελίων τετράθηρον ἅρμα, ἐν ᾧ ἅρματι ἵπποι
λευκοὶ κατὰ τὸν Ζαχαρίαν· ζυγαῖοι μὲν φῶς καὶ ἀθανασία, ἀκροτῆρες δικαιοσύνη
καὶ ἀλήθεια, οἱ πορευόμενοι κατόπισθεν τοῦ ἅρματος τοῦ πορευομένου εἰς γῆν
βοῤῥᾶ· περὶ οὗ λέγει ἡ Σοφία, Σκληρός τις ἄνεμος βοῤῥᾶς· σκληρὸν γὰρ ἀληθῶς
πνεῦμα καὶ πονηρὸν ὁ διάβολος. Ἐν ἅρματι αὐτοῦ ἵπποι μέλανες· ὑποζύγιοι μὲν ὁ
ᾅδης καὶ θάνατος, ἀκροτῆρες δὲ σκότος καὶ ἀπώλεια, οὓς δεῖ καθηνιοχεῖν τῷ ἅρματι
τοῦ Χριστοῦ, ὅτι Τὸ ἅρμα, φησὶ, τοῦ Θεοῦ, μυριοπλάσιον, χιλιάδες εὐθυνόντων, οὓς
δεῖ καθηνιοχεῖν ἐν ταῖς τρικυμίαις τοῦ βίου, ὡς ἐν θαλάσσῃ. Ἐπεβίβασεν γὰρ εἰς
θάλασσαν τοὺς ἵππους αὐτοῦ ταράττοντας ὕδατα πολλὰ, καὶ καταστρέφοντας τὰ
ἅρματα τοῦ νοητοῦ Φαραὼ, καὶ τὴν δύναμιν αὐτοῦ εἰς θάλασσαν Ἐρυθράν. Ἔχει δὲ
καὶ πάριππον ἄγγελον τὸν καταδιώκοντα καὶ παρασκοτοῦντα τοὺς ἵππους τοῦ
ἐχθροῦ σου. Γενηθήτω γὰρ, φησὶν, ἡ ὁδὸς αὐτῶν σκότος καὶ ὀλίσθημα, καὶ ἄγγελος
Κυρίου καταδιώκων αὐτούς. Ἔχεις ἐξ ἐναντίας τοὺς τὰς δόσεις αὐτοῦ
καταγγέλλοντας Χαναναίους, Φαρισαίους, Σαδουκαίους· ἔχεις καὶ σὺ Μωϋσέα,
Σαμουὴλ καὶ ∆αυῒδ, οἳ προκατήγγειλαν περὶ τῆς ἐλεύσεως τοῦ δικαίου. Ἔχει ἐκεῖνος
μεσαφέτας δαίμονας, Βὴλ καὶ ∆αγὼν καὶ Βαὰλ καὶ Χαμὼς, οἳ τὸν Ἰσραὴλ ἔβαλλον
ἀπὸ σκελῶν, καὶ τὰ κῶλα αὐτῶν ἔῤῥιψαν ἐν τῇ ἐρήμῳ· ἔχεις καὶ σὺ θυρανοίκτας
Παῦλον, καὶ Σιλουανὸν, καὶ Τιμόθεον, καθὼς αὐτοί φασιν, ὅτι Θύρα ἡμῖν ἀνέῳγε
μεγάλη καὶ ἐναργὴς, καὶ ἀντικείμενοι πολλοὶ, οἳ καὶ ἐζήτησαν ἵνα δοθῇ αὐτοῖς
λόγος ἐν ἀνοίξει τοῦ στόματος αὐτῶν. Ἔχει ἐκεῖνος Φίλητον, Ἰούδαν, ∆ήμαν καὶ
Ἑρμογένην, Ὑμεναῖον καὶ Ἀλέξανδρον τὸν χαλκέα ὑποκρίσεως γέμοντας· ἔχεις καὶ
σὺ θεωρητὰς τὸν τῶν ἀγγέλων δῆμον, τὰ τῶν δικαίων τάγματα ἄνωθεν καθορῶντα·
πῶς μὲν πάντες τρέχουσιν, εἷς δὲ λαμβάνει τὸ βραβεῖον.
Εἷς ὁ κατὰ τὴν ἀλήθειαν λαὸς τοῦ Χριστοῦ, εἷς ἐν Χριστῷ βαπτισθεὶς, καὶ
Χριστὸν ἐνδυσάμενος· καὶ μηκέτι Ἰουδαῖος μήθ' Ἕλλην, ἄρσεν καὶ θῆλυ, δοῦλος ἢ
ἐλεύθερος, Σκύθης, Ἄρειος, Εὐνόμιος, Μαραθώνιος, ἀλλ' εἷς ἐν Χριστῷ. Ἀξίως τοῦ
Θεοῦ τρέχε, καὶ πολιτεύου, λαὲ τοῦ Θεοῦ, ἐν τῷ θεωρίῳ τοῦ παναγίου Πνεύματος.
Μὴ τερπέτω σε τὰ προϊππικὰ κομβινεύματα, ἵνα μὴ καταλάβῃ σε τὰ πρὸ κρίσεως
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

κινδυνεύματα. Εἰ βούλει κομβινεύειν καὶ διατάσσεσθαι, δύνασαι τοῦτο ποιεῖν
μᾶλλον ἐν τῷ θεωρήματι πνευματικῷ. Ζεῦξον κατὰ τῶν θηλυμανῶν ἵππων τοῦ
διαβόλου τοὺς εὐσεβεῖς σου λογισμούς· ἀντιδιάταξαι κατ' ἰσχυρῶν μακροθυμίαν.
Κρεῖττον γὰρ, φησὶ, μακρόθυμος ἰσχυροῦ· κατὰ ἰουδαϊσμοῦ χριστιανισμὸν, καθ'
ἑλληνισμοῦ θεοσέβειαν, κατὰ πορνείας σωφροσύνην, κατὰ πλεονεξίας
ἐλεημοσύνην, κατὰ ψεύδους ἀλήθειαν, κατ' ὀργῆς πραότητα καὶ ἐπιείκειαν. Πάλιν
κατ' ὀξέων ζεῦξον καθυποτακτὰς, τουτέστι, κατὰ θυμοῦ καὶ λύπης πραϋθυμίαν (
Πραΰθυμος γὰρ, φησὶν, ἀνὴρ, καρδίας ἰατρός)· κατ' ὀξέων πραϋθυμίαν, κατ' ἔχθρας
φιλίαν, κατὰ μίσους ἀγάπην, κατὰ θλίψεως εὐχαριστίαν, καθ' ὑπερηφανίας
ταπεινοφροσύνην, κατὰ πονηρίας ἀγαθωσύνην, καὶ κατὰ παντὸς ἅπαξ ἐναντίου
πράγματος τὴν ὀρθόδοξον καὶ σωτήριον πίστιν ἔχε.
∆εῖ δέ σε ἡνιοχοῦντα καὶ τὴν τοῦ πνεύματος ἔχειν ἐγκράτειαν, ἵνα μὴ
ναυσιασμῷ περιπεσὼν ἀκηδιάσῃς. Ὁ γὰρ ἀγωνιζόμενος, φησὶ, παντὸς ἐγκρατεύεται·
ἐκεῖνοι μὲν οὖν ἵνα φθαρτὸν στέφανον λάβωσιν, ἡμεῖς δὲ ἄφθαρτον. Λάβε καὶ τὴν
κασσίδα τοῦ σωτηρίου, καὶ τὸ φραγέλλιον ἐν τῇ χειρί σου, ὃ Κύριος ἡμῶν Ἰησοῦς
ποιήσας ἐκ σχοινίου, ἤλαυνεν ἀπὸ τοῦ 59.570 ἱεροῦ τοὺς τοῦ διαβόλου ἐμπόρους.
Στῆκε ἑδραίως ἐπὶ τὸ σκεῦος τῆς ἐκλογῆς· ἀνάβηθι ἐπὶ τὸ ἅρμα τοῦ εὐνούχου, ὦ
Φίλιππε, τοῦ ἀπηλλαγμένου τῆς ἀκαθαρσίας πάσης· ἀνάβηθι ἐπὶ τὸ ἅρμα·
ἀμετεωρίστως ἀτένιζε τὸν τῷ ἀναγνώσματι τοῦ Εὐαγγελίου διακονοῦντα
μαμπάριον. Μήτις με δόξῃ γελοιάζοντα λέγειν· οὐ ψεύδομαι. Ὡς γὰρ ἐν τῷ
ἱπποδρομίῳ πάντων οἱ ὀφθαλμοὶ ἐπὶ τὸν μαμπάριον, πεπηγότος τοῦ σημάντρου,
ἀτενίζουσιν. ἐκδεχομένων ἐν τῇ βουλῇ ἐκείνου τὸ ἄνοιγμα· οὐδὲν ἧττον καὶ οἱ ἐπὶ
τὴν ἐκκλησιαστικὴν θεωρίαν εἰς τὸ αὐτὸ ἀθροιζόμενοι, τοῦ διακόνου ἀνοίγειν
μέλλοντος τὸ τοῦ Εὐαγγελίου τετράθυρον, πάντες αὐτῷ ἀτενίζομεν ἡσυχίαν
παρέχοντες, καὶ ἡνίκα τοῦ δρόμου τῆς ἀναγνώσεως ἄρξηται, εὐθέως διανιστάμεθα
ἡμεῖς, ἐπιφωνοῦντες· Ἀλλὰ δόξα σοι, Κύριε. Ἀνάβηθι ἐπὶ τὸ ἅρμα, θήρευσον,
ἀποδρόμησον, κατάστειλον, ἔκβαλε, ἀπόστρεψον, πίασον, καὶ ποίησον ἐκ τοῦ
στόματος τὴν καλὴν ὁμολογίαν, ἀποδρόμησον κατὰ τοῦ ἐναντίου· Ὁ γὰρ
πορευόμενος ἁπλῶς, πορεύεται πεποιθώς. Κατάστειλον αὐτὸν ὡς ὁ ∆αυῒδ τὸν
Γολιὰθ, ἔκβαλλε αὐτὸν ὡς ὁ Ἑλισσαῖος τὸν Γιεζὶ, πίασον αὐτὸν ὡς ὁ Ἡλίας τὸν
Ἀχαὰβ ἐν τῷ ἀμπελῶνι τοῦ Ναβουθαὶ, ἀπόστρεψον αὐτὸν εἰς τὸ σκότος τὸ ἐξώτερον,
ὅπου ὁ κλαυθμὸς καὶ ὁ βρυγμὸς τῶν ὀδόντων· πρόσεχε σεαυτῷ.
Προσέχετε γὰρ, φησὶν, ἑαυτοῖς, μήποτε βαρυνθῶσιν ὑμῶν αἱ καρδίαι ἐν
κραιπάλῃ καὶ μέθῃ. Πάντα βάστασον· Ἀλλήλων γὰρ, φησὶ, τὰ βάρη βαστάζετε.
Ἄνοδα ἔχων φυλάττου, Μή ποτε προσκόψῃς πρὸς λίθον τὸν πόδα σου· μὴ
ἀποστρεφόμενος εἰς τὰ ὀπίσω, μνημονεύων τῆς γυναικὸς Λώτ. Μέσον ἔχε ὁδὸν
δικαιοσύνης, μὴ ἐκκλίνῃς εἰς τὰ δεξιὰ, μηδ' εἰς ἀριστερά. Ἔσωθεν ζήτει ἐκ τοῦ
παραδείσου, ἔσωθεν ζήτει. Ἐκ γὰρ τῆς καρδίας ἐξέρχονται διαλογισμοὶ πονηροὶ,
φόνοι, μοιχεῖαι, κλοπαὶ, ψευδομαρτυρίαι, καταλαλιαὶ, βλασφημίαι, τὰ κοινοῦντα
τὸν ἄνθρωπον. Μὴ συναναβῇς αὐτῷ, ὡς οὐδὲ ὁ Θεὸς τῷ Ἰσραὴλ, ὅτι
σκληροτράχηλός ἐστι· μὴ συναναβῇς αὐτῷ· εἰς ἁμαρτίαν σε καλεῖ. Αὐτός σε κάμψει.
Καὶ γενοῦ πρὸ αὐτοῦ, λέγων αὐτῷ· Ὕπαγε ὀπίσω μου, Σατανᾶ, ὅτι οὐ φρονεῖς τὰ τοῦ
Θεοῦ, ἀλλὰ τὰ τῶν ἀνθρώπων. Ἔξω μεῖνον, ὡς Ἠλίας φεύγων τὴν Ἰεζαβέλ· σπεῦδε
εἰς τὸ βραβεῖον τῆς ἄνω κλήσεως. Μή σε καταλάβῃ, ὡς οὐδὲ Ἀβεσσαλὼμ παῖδες τοὺς
περὶ Ἰωναθὰν, καὶ Ἀχιμαὰς τοὺς ἱερεῖς τοῦ Κυρίου φεύγοντας· μή σε πιάσει, ὡς οὐδὲ
βασιλέως Ἱεριχὼ παῖδες τοὺς κατασκόπους· ἀλλ' εἰ δυνατὸν, ἀπάτησον αὐτὸν ὡς
Ῥαὰβ τοὺς ἐρευνῶντας ἡ πόρνη. Μή σε καθυποτάξῃ, ὡς οὐδὲ Φαραὼ τοὺς περὶ
Μωϋσέα καὶ Ἀαρών· ἀλλὰ περίκαμψον αὐτὸν, ὡς οἱ μάγοι Ἡρώδην. Μὴ ἀγωνιάσῃς·
ἔχεις γὰρ τὸν διατάκτην Θεὸν, καὶ ἰατρὸν τὸν Χριστόν. Μηδὲν πτοηθῇς· ἔχεις γὰρ
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

ἀλείπτην τὸ Πνεῦμα τὸ ἅγιον, τὸν τοῦ Σαμουῆλος μυστικῷ κέρατι χρίοντά σε τῷ
μύρῳ. Μηδὲ τὸ δίψος φοβηθῇς· ἔχεις γὰρ τὸ ὕδωρ ἐπὶ σὲ τῆς ἀναπαύσεως τοῦ
βαπτίσματος διὰ τῆς κολοκύνθης, ἵνα προερχόμενόν σοι, τουτέστι, διὰ τῆς κηρυκείας
τοῦ Ἰωνᾶ, ἀναλάβῃ σε. Στρέψον τὸ ἅρμα τῆς τυραννίδος αὐτοῦ· βάλε αὐτὸν εἰς
βόθρον, ὃν εἰργάσατο· βάλε αὐτὸν εἰς τὴν βαθεῖαν ἐκεῖ. Ψαλμικῶς ἀναβόησον, Ἐκ
βαθέων ἐκέκραξά σοι, Κύριε· Κύριε, εἰσάκουσον τῆς φωνῆς μου. Βάλε αὐτὸν εἰς τὴν
βαθεῖαν, κατακάμψας μονώτατος ἐκ τῆς ἀπωλείας αὐτοῦ. Ἆρον εἰς ὕψος ὁσίους
χεῖρας πρὸς τὸν βασιλέα, λαβὼν βραβεῖον τῶν φοινίκων κατὰ τοὺς παῖδας, καὶ
σείσας προσκύνησον αὐτῷ, ὅτι Ἔπεσε Βὴλ, συνετρίβη ∆αγών. Χάρις δὲ τῷ Θεῷ τῷ
διδόντι ἡμῖν τὸ νῖκος διὰ τοῦ Κυρίου ἡμῶν τοῦ Ἰησοῦ Χριστοῦ.
Ταύτῃ τῇ νίκῃ καὶ τὸν παράδεισον οἷα λοῦτρον πάνδημον ἀνοῖξαι δυνηθήσῃ,
ἔνθα ποταμοὶ τέσσαρες, ὕδωρ πολὺ, ὡς ἐν λούτρῳ, ἐν τόπῳ χλόης, ὅπως καὶ σὺ
εὐκαίρως λέγῃς· Εἰς τόπον χλόης ἐκεῖ με κατεσκήνωσεν, ἐφ' ὕδατος ἀναπαύσεως
ἐξέθρεψέ με· καὶ ἐπὶ πᾶσι τούτοις καυχώμενος λέγῃς, Τὸν καλὸν ἀγῶνα ἠγώνισμαι,
τὸν δρόμον τετέλεκα, τὴν πίστιν τετήρηκα· λοιπὸν ἀπόκειταί μοι ὁ τῆς δικαιοσύνης
στέφανος, ἐν Χριστῷ Ἰησοῦ τῷ Κυρίῳ ἡμῶν, ᾧ δόξα εἰς τοὺς αἰῶνας τῶν αἰώνων,
Ἀμήν.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

3

