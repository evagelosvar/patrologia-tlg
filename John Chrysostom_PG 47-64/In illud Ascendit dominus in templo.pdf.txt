In illud: Ascendit dominus in templo
Εἰς τὸ, Ὅτε ἀνέβη ὁ Κύριος εἰς τὸ ἱερὸν, μεσούσης τῆς ἑορτῆς καὶ εἰς τὸν
Μελχισεδέκ. Τῇ μεσοπεντηκοστῇ.
61.739
Ῥοαὶ καὶ μῆλον καὶ φοίνιξ καὶ οἱ λοιποὶ τῶν ἀκροδρύων καρποὶ τὸν τοῦ
σώματος ἡδύνουσι φάρυγγα· θεῖοι δὲ λόγοι ἁγίων διδασκάλων ἀγάπῃ κεκερασμένοι,
τῇ ἀκοῇ τῆς διδασκαλίας παραπεμπόμενοι, τὸν τῆς ψυχῆς γλυκαίνουσι λάρυγγα. Καὶ
ὥσπερ ὁ τοὺς καρποὺς τῶν δένδρων τρυγήσας, ἐν τῇ τῶν καρπῶν μεταλήψει
ἐπαίνους τοῖς δένδροις ἀποδίδωσιν· ὡσαύτως καὶ ὁ τὰ θεῖα λόγια τῶν διδασκάλων
ἐν τῷ καρτάλλῳ τῆς ἀκοῆς ἐντρυγήσας, εἶτα τῷ νῷ παραπέμψας, καὶ τὴν τοῦ λόγου
γλυκύτητα γευσάμενος, τῷ διδασκάλῳ ἐπαίνους ἐκπέμπων ἐκ χειλέων λέγει· Ὡς
γλυκέα τῷ λάρυγγί μου τὰ λόγιά σου, ὑπὲρ μέλι καὶ κηρίον τῷ στόματί μου! Καὶ ὁ
σοφώτατος δὲ Σαλομὼν τοὺς καλοὺς λόγους κηρίοις μέλιτος παρεικάσας φησί·
Κηρία μέλιτος λόγοι καλοί. Ὥσπερ γὰρ ὁ λάρυγξ τὰ βρώματα γευόμενος, οἶδε τὰ
καλὰ ἀπὸ τῶν φαύλων διακρίνειν· ὡσαύτως καὶ ὁ νοῦς τῆς ψυχῆς τὰ χρηστὰ ῥήματα
οἶδεν ἀπὸ τῶν φαύλων διακρίνειν, ὥσπερ φησὶν ἐν τῷ Ἰώβ· Λάρυγξ μὲν γὰρ σῖτα
γεύεται, νοῦς δὲ ῥήματα διακρίνει.
Καὶ μηδεὶς νομιζέτω ἔχειν μὲν τὸν οἰκοδεσπότην ἐν τῷ ἰδίῳ παραδείσῳ
δένδρα διάφορα, φοίνικας λέγω καὶ μῆλα καὶ ἐλαίας, τὸν δὲ Κύριον ἐν τῷ ἰδίῳ
παραδείσῳ τῆς Ἐκκλησίας μὴ ἔχειν διάφορα στελέχη τῶν πατέρων, ἕκαστον κατὰ
τὴν οἰκείαν χρηστότητα πρὸς τὴν τῶν εἰσπορευομένων ἀπόλαυσιν ἐκ βάθους τῆς
καρδίας τοὺς καρποὺς τῶν λόγων προσφέροντα. Ὁ μὲν γάρ τίς ἐστιν αὐτῶν, ὥσπερ
ἐλαία εὐθαλὴς, τοῖς φύλλοις τῆς πίστεως κατάκομος, καὶ τοῖς ἔργοις τῆς εὐλαβείας
κατάκαρπος· ἄλλος δέ ἐστιν ὥσπερ φοίνιξ τοῖς βαΐοις τῆς κατὰ διαβόλου νίκης
κατεστεμμένος, καὶ τὸν γλυκύτατον καρπὸν τῆς εὐποιίας τοῖς πένησι
προβαλλόμενος· περὶ οὗ φησιν ἡ Γραφὴ, ∆ίκαιος ὡς φοίνιξ ἀνθήσει. Ἕτερος δέ τίς
ἐστιν, ὥσπερ μῆλον, δύο τέρψεις παρέχων τοῖς ἀνθρώποις, τήν τε ὄσφρησίν φημι,
καὶ τὴν γλυκύτητα τῷ φάρυγγι· καὶ τὸν τοιοῦτον ἐγκωμιάζουσα ἐν τοῖς Ἄσμασι τῶν
ᾀσμάτων ἡ νύμφη Ἐκκλησία τί φησιν; Ὡς μῆλον ἐν τοῖς ξύλοις τοῦ δρυμοῦ, οὕτως
ἀδελφιδός μου ἐν μέσῳ τῶν υἱῶν Ἰσραήλ. Καί φησιν, Ἐν τῇ σκιᾷ αὐτοῦ ἐπεθύμησα
καὶ ἐκάθισα, καὶ καρπὸς αὐτοῦ γλυκὺς ἐν τῷ λάρυγγι. Τί δὲ ποιήσω, ἀγαπητοὶ, ἐγὼ, ὁ
ὀλιγοστὴν φυλλάδα λογίων καὶ καρπῶν κεκτημένος, μήτε τοὺς καυσαμένους
ἀναψύξαι ἰσχύων, μήτε τοὺς πεινωμένους τοῖς καρποῖς τῶν λόγων ἐκθρέψαι
δυνάμενος; Καλὸν γάρ μοι ἦν μεθ' ὑμῶν μανθάνειν τὰ τῶν ἁγίων πατέρων
διδάγματα. Ἀλλ' ἐπειδὴ φόβος Θεοῦ καὶ ὁ πόθος ὑμῶν πρὸς τὸ λέγειν ἡμᾶς διεγείρει,
δύο ἢ τρεῖς λόγους τοῦ γλυκυτάτου Εὐαγγελίου λαβόντες καθάπερ φοίνικας, καὶ
ἀπογεύσαντες, ὑμᾶς διεγείρωμεν· οὕτω γὰρ προθύμως ἐπὶ τὰ καλλίκαρπα δένδρα
τῶν ἁγίων πατέρων ζέοντες τῷ πνεύματι ἐπειχθήσεσθε.
Ἀρτίως ἠκούσαμεν τοῦ εὐαγγελιστοῦ λέγοντος, Μεσαζούσης τῆς ἑορτῆς
ἀνέβη ὁ Ἰησοῦς εἰς τὸ ἱερὸν, καὶ ἐδίδασκεν. Οὐ παρῆλθεν ὁ λόγος τοῦ Εὐαγγελίου,
ἐπειδὴ ἐνέστηκεν ὁ τῆς ἑορτῆς καιρὸς, πρέπουσα τοῖς παροῦσιν ἡ τοῦ Εὐαγγελίου
ἀνάγνωσις, ἁρμόζουσα τῇ ἡμέρᾳ ἡ διήγησις· ἀδελφὴ γάρ ἐστι τῆς ἐνεστώσης ἑορτῆς
ἡ τοῦ λόγου δύναμις. Μεσαζούσης τῆς ἑορτῆς ἀνέβη ὁ Ἰησοῦς εἰς τὸ ἱερόν·
τελεσθείσης δὲ τῆς ἑορτῆς, οὐκέτι ἀναβαίνει εἰς τὸ ἱερὸν, ἀλλ' εἰς τὸν οὐρανὸν
ἀνατρέχει. Μεσαζούσης δὲ τῆς ἑορτῆς ἀνέβη ὁ Ἰησοῦς εἰς τὸ ἱερόν. Ἑορτῆς δὲ λέγει,
οὐ τῆς παρ' Ἕλλησι καὶ Ἰουδαίοις γινομένης, ἔνθα μέθαι καὶ κῶμοι καὶ ᾄσματα
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

πορνικὰ, καὶ θέλγιστρα καλλιφωνίας, καὶ ὀρχήσεις πολύστροφοι, ἀλλ' ἑορτῆς ἁγίας
πρεπούσης Χριστιανοῖς, ἔνθα οὐ μέθαι καὶ κῶμοι, ἀλλ' εὐχαὶ καὶ ὑμνῳδίαι καὶ
ἱκεσίαι· ἔνθα οὐκ ᾄσματα πορνικὰ ἐκλύοντα σώματα, ἀλλ' ᾄσματα θεϊκὰ τὰς ψυχὰς
ἐνισχύοντα· ἔνθα οὐκ ὀρχήσεις πολύστροφοι, ἀλλὰ ψυχῶν ἁγίων ἐν ἀγαλλιάσει
σκιρτήματα. Εἰ δὲ θέλεις ἰδεῖν τοὺς παρ' ἡμῖν ᾄδοντας καὶ ὀρχουμένους καὶ
σκιρτῶντας οἷοί εἰσιν, ἴδε μοι τὸν Ἡσαΐαν ᾄδοντα καὶ λέγοντα, Ἄσω δὴ τῷ
ἠγαπημένῳ ᾆσμα τῷ ἀγαπητῷ μου, τῷ ἀμπελῶνί μου. Βλέπε δὲ καὶ χορὸν ἁγίων
γυναικῶν, Μαρίαν τὴν ἀδελφὴν Μωϋσέως, μετὰ καὶ ἄλλων γυναικῶν Ἰσραηλιτῶν,
ἐπινικίους ὕμνους τῷ Θεῷ ᾀδούσας καὶ λεγούσας· Ἄσωμεν τῷ Κυρίῳ· ἐνδόξως γὰρ
δεδόξασται. Οὗ μέμνηται καὶ ∆αυῒδ ἐν ὕμνοις λέγων, Ἐν μέσῳ νεανίδων 61.740
τυμπανιστριῶν ἐν Ἐκκλησίαις εὐλογεῖτε τὸν Θεόν. Καὶ ὁ αὐτὸς μακάριος ∆αυῒδ οὐκ
ἐπαισχύνεται ἔμπροσθεν τῆς θεοφόρου κιβωτοῦ ὀρχεῖσθαι, ὡς ἔστιν ἀκοῦσαι
λέγοντος· Καὶ παίξομαι, καὶ γελάσομαι, καὶ ὀρχήσομαι ἐναντίον Κυρίου. Τὸ γὰρ
βρέφος τῆς Ἐλισάβετ Ἰωάννης, θεωρήσας τὸν θεοφόρον βότρυν ἐν τῇ ἀγάμῳ μήτρᾳ
ἔτι κρυπτόμενον, ἐν τῇ λινῷ τῆς γαστρὸς χορεύων ἐσκίρτα. Μεσαζούσης δὲ τῆς
ἑορτῆς ἀνέβη ὁ Ἰησοῦς εἰς τὸ ἱερόν. Ἱερὸν δέ ἐστι Θεῷ μόνῳ οἰκητήριον. Λέγουσι
γὰρ καὶ Ἕλληνες, ἔχομεν ἱερὸν, ἀγνοοῦντες ὅτι ἐκεῖνο οὐκ ἔστιν ἱερὸν, ἀλλὰ
μιαρόν. Ὅπου γὰρ αἱμάτων ἔκχυσις καὶ σπονδῶν, καὶ κνίσσαι εἰδωλικαὶ καὶ τελεταὶ
δαιμόνων, ἐκεῖνο οὐκ ἂν εἴη ἱερὸν, ἀλλὰ μιαρὸν τῶν δαιμόνων. Μεσαζούσης δὲ τῆς
ἑορτῆς ἀνέβη ὁ Ἰησοῦς εἰς τὸ ἱερόν. Ἱερὸν δέ ἐστι μόνῳ Θεῷ οἰκητήριον
ἀνακείμενον, ἔνθα ἀγάπη καὶ εἰρήνη, ἔνθα πίστις καὶ σωφροσύνη αὐλίζεται. ∆ιὰ
τοῦτο καὶ τὸν ἱερέα τοῦ Θεοῦ τὸν ἱερουργοῦντα τῷ Θεῷ εἰρηναῖον δεῖ εἶναι καὶ
γαληνὸν, ἄκλοπον, ἄδολον, ἀφιλάργυρον, ἵνα προτέρως Θεὸς ἐν τῇ αὑτοῦ ψυχῇ
κατορθώσῃ, εἶθ' οὕτω τὸν λαὸν πρὸς τὴν ἑαυτοῦ εἰκόνα μεταῤῥυθμίσῃ. ∆ύναται
ἱερὸν λέγεσθαι καὶ ἑκάστου πιστοῦ ἀνθρώπου ψυχὴ κεκαθαρμένη· Οἰκήσω γὰρ ἐν
ὑμῖν, καὶ ἐμπεριπατήσω· ἱερὸν δὲ ὡς ἀληθῶς ἅγιον, καὶ ἱερεῖον, καὶ ἱερεὺς, αὐτός
ἐστιν ὁ Χριστὸς, ἐν ᾧ, κατὰ τὸν θεῖον Ἀπόστολον, Πᾶν τὸ πλήρωμα τῆς θεότητος
κατοικεῖ σωματικῶς· πρὸς ὃν εἴρηκεν ὁ Πατὴρ, Σὺ ἱερεὺς εἰς τὸν αἰῶνα κατὰ τὴν
τάξιν Μελχισεδέκ.
Νῦν ἐκπεσὼν ἐκ τοῦ ἀδοκήτου εἰς μέγιστον ζήτημα, ἀπορῶ ποῦ κλίνω τὸ
ὄμμα τῆς διανοίας ποῦ μετελεύσω τῷ πηδαλίῳ τῆς διδασκαλίας οὐκ οἶδα. Εἰ γὰρ ὁ
ἀπόστολος εἰς τὸ κεφάλαιον τὸ περὶ τὸν Μελχισεδὲκ ἐληλυθὼς, ὤκνησε λεπτομερῶς
διηγήσασθαι εἰρηκὼς, Περὶ οὗ πολὺς ἡμῖν ὁ λόγος καὶ δυσερμήνευτος, τί ἂν εἴποιμ'
ἐγὼ, τοσοῦτον ἐπέχων τοῦ ἀποστόλου, ὅσον ἀνθρώπου διάκρισις; Καὶ ὥσπερ
ὁδοιπόρος μέχρις ἄν τινα πρανῆ καὶ πεδιάσιμον ὁδὸν ἐπιβαίνῃ, ἡδέως τὴν πορείαν
ἀνύει· ἐπειδὰν δὲ ἔλθῃ πρὸς τόπον τινὰ τραχύτατον ἢ προσάντη καὶ δύσβατον, ἔνθα
ἐστὶν ὄρος οὐρανόμηκες, σταθεὶς καὶ τὸν κάματον τοῦ ἀναφόρου ἀναλογισάμενος,
ἠρέμα βαδίζειν ἄρχεται, ἵνα μὴ ταχεῖον ἀποκρίσῃ· ὡσαύτως καὶ ἡμεῖς, ἕως οὗ
εἴχομεν τὸ τοῦ Εὐαγγελίου πρανὲς καὶ πεδιάσιμον, τῷ βήματι τοῦ λόγου ἡδέως
ἐπορευόμεθα· ὅταν δὲ ἐληλύθαμεν εἰς οὐρανόμηκες νόημα, ἠρέμα ἐπιβαίνειν τῇ
οὐρανίῳ κλίμακι ἑρμηνείᾳ ἀρχόμεθα. Τί δέ ἐστι τὸ παρὰ πᾶσι Χριστιανοῖς
ζητούμενον θεώρημα; Τίνος ἕνεκεν, φησὶν, ὁ Ἀπόστολος μνημονεύσας Μελχισεδὲκ,
εἶπεν αὐτὸν ἀπάτορα καὶ ἀγενεαλόγητον, μήτε ἀρχὴν ἡμερῶν, μήτε ζωῆς τέλος
ἔχοντα; οὗτος οὐκ ἂν εἴη ἄνθρωπος, ἀλλὰ Θεός. Πολλοὶ δὲ μὴ νοήσαντες τὰ περὶ
αὐτοῦ εἰρημένα, μείζονα τοῦ Χριστοῦ εἶναι εἰρήκασι, καὶ συνεστήσαντο ἑαυτοῖς
αἵρεσιν, καὶ λέγονται ἐκεῖνοι Μελχισεδεκιανοί· καὶ φιλονεικοῦντες ἡμῖν καὶ
βουλόμενοι δεῖξαι, ὅτι μείζων ἐστὶ τοῦ Χριστοῦ ὁ Μελχισεδὲκ, προφέρουσιν ἡμῖν
μάρτυρα τὴν Γραφὴν λέγουσαν· Σὺ ἱερεὺς εἰς τὸν αἰῶνα κατὰ τὴν τάξιν Μελχισεδέκ.
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

Πῶς γὰρ, φησὶν, οὐκ ἂν εἴη μείζων ὁ Μελχισεδὲκ, οὗ κατὰ τὴν εἰκόνα καὶ τάξιν
ἱερατεύει ὁ Χριστός;
Ἄλλοι δὲ πλανηθέντες λέγουσιν αὐτὸν εἶναι τὸ Πνεῦμα τὸ ἅγιον. Ἡμεῖς δὲ
λέγομεν αὐτὸν εἶναι ἄνθρωπον ὁμοιοπαθῆ ἡμῖν, καὶ μήτε μείζονα αὐτὸν εἶναι
Χριστοῦ· τί δὲ λέγω τοῦ Χριστοῦ; ἀλλ' οὔτε Ἰωάννου τοῦ Βαπτιστοῦ. Ἀληθὴςγὰρ ὁ
λόγος τοῦ Κυρίου, Ἀμὴν λέγω ὑμῖν, οὐκ ἐγήγερται ἐν γεννητοῖς γυναικῶν μείζων
Ἰωάννου τοῦ Βαπτιστοῦ. Ἀλλ' οὔτε μὴν εἶναι αὐτὸν τὸ ἅγιον Πνεῦμα. Ὅτι δὲ οὐκ
ἔστι μείζων τοῦ Χριστοῦ ὁ Μελχισεδὲκ, εἰπάτωσαν ἡμῖν ἐκεῖνοι οἱ τοῦτο λέγοντες,
ποίου περιβόλου, ἢ ποίου χωρίου αὐτὸν εἶναι λέγουσιν. Ἆρα οὐρανίου; ἆρα ἐπιγείου;
ἆρα καταχθονίου; Ἐὰν οὖν εἴπωσιν ὅτι τῶν οὐρανίων ἐστὶν ὁ Μελχισεδὲκ ἢ ἄλλου
τινὸς χωρίου, ἀκουσάτωσαν ὅτι τὸ αὐτοῦ γόνυ κάμπτει τῷ Χριστῷ· λέγει γὰρ ὁ
Ἀπόστολος ὅτι Αὐτῷ κάμψει πᾶν γόνυ, καὶ πᾶσα γλῶσσα ἐξομολογήσεται,
ἐπουρανίων, ἐπιγείων καὶ καταχθονίων.
Εἰ οὖν κάμπτει γόνυ ὁ Μελχισεδὲκ τῷ Χριστῷ, ἐλάττων εἴη προσκυνῶν
Μελχισεδὲκ τοῦ προσκυνουμένου ὑπ' αὐτοῦ 61.741 Χριστοῦ. Τί δὲ οἱ τάλανες καὶ τὸν
ἑξῆς στίχον εἰρημένον παρὰ τοῦ Ἀποστόλου οὐκ ἀκούουσιν, ἵνα μάθωσιν αὐτὸν
ἐλάττω εἶναι τοῦ Χριστοῦ; ἐπιφέρει γὰρ λέγων, Ἀφωμοιωμένος τῷ Υἱῷ τοῦ Θεοῦ,
ὥσπερ καὶ ἡμεῖς κατ' εἰκόνα καὶ ὁμοίωσιν τοῦ Χριστοῦ ἐγενόμεθα. Ἀλλ' ἴσως τις τῶν
ἀκροατῶν ἐπερωτᾷ καὶ λέγει· Τί οὖν ἐστιν ἀπάτορα λέγεσθαι καὶ ἀμήτορα καὶ
ἀγενεαλόγητον τὸν Μελχισεδέκ; Πρὸς ὃν ἐροῦμεν, ὅτι Ἰουδαῖοι μὲν εἰρήκασιν αὐτὸν
ἐκ πορνείας γεννηθέντα, καὶ διὰ τοῦτο μὴ γενεαλογεῖσθαι ὑπὸ τῆς Γραφῆς. Πρὸς οὓς
ἐροῦμεν, ὅτι, Κακῶς εἴπατε· εὑρίσκομεν γὰρ καὶ ἄλλους ἀπὸ πορνείας γεννηθέντας
καὶ γενεαλογηθέντας. Εὐθὺς Σολομὼν ἐκ τῆς τοῦ Οὐρίου γυναικὸς γεννηθεὶς
ἐγενεαλογήθη, καὶ Ἀβιμέλεχ υἱὸς Γεδεὼν, ὃς ἐκ πορνείας γεννηθεὶς ἐγενεαλογήθη.
Τίνος οὖν ἕνεκεν οὐ γενεαλογεῖται ὁ Μελχισεδέκ; Ἀλλ' ἐπειδὴ τύπος ἦν τοῦ Χριστοῦ
ὁ Μελχισεδὲκ, καὶ εἰκόνα ἔφερε τοῦ Χριστοῦ. ∆ιὰ τοῦτο ἡ Γραφὴ παρέλιπε τὸν
πατέρα καὶ τὴν μητέρα, καὶ ἀγενεαλόγητον αὐτὸν διηγήσατο, ἵνα ἐν ἐκείνῳ,
καθάπερ εἰκόνι, τὸν ἀληθῶς ἀπάτορα καὶ ἀμήτορα καὶ ἀγενεαλόγητον τὸν Χριστὸν
ἐνοπτρισώμεθα. Ἀπάτωρ γὰρ Χριστὸς ἐπὶ γῆς κατὰ σάρκα, ἀμήτωρ ἐν οὐρανοῖς κατὰ
Πνεῦμα, κατὰ τὴν θεότητα. Τοιοῦτός ἐστι καὶ ὁ Μήτε ἀρχὴν ἡμερῶν, μήτε ζωῆς
τέλος ἔχων. Οὐ γὰρ ἐγενεαλόγησε τὸν Μελχισεδὲκ ἡ Γραφὴ, ἀλλὰ παρέλιπεν αὐτοῦ
καὶ τὴν γέννησιν καὶ τὴν τελευτὴν, ἵνα, καθὼς προείπαμεν, ἐν ἐκείνῳ τοῦτον
θεωρήσωμεν τὸν ἀληθινὸν Μελχισεδέκ. Βασιλεὺς γὰρ δικαιοσύνης ἑρμηνεύεται ὁ
Μελχισεδὲκ, ὅς ἐστι Χριστὸς, 61.742 μήτε ἀρχὴν ἡμερῶν, μήτε ζωῆς τέλος ἔχων.
Οὔτε γὰρ ἀρχὴν ἡμερῶν ὁ Θεὸς Λόγος εἶχεν, οὔτε ἔληξέ ποτε ἢ ἐτελεύτησεν· εἰς ἀεὶ
γὰρ ὁ Χριστὸς διαμένει. Ἀντιλέγουσι γὰρ ἡμῖν οἱ Μελχισεδεκιανοὶ λέγοντες, Τί οὖν
ἐστιν ὃ λέγει πρὸς αὐτὸν ὁ Πατὴρ, Σὺ ἱερεὺς εἰς τὸν αἰῶνα κατὰ τὴν τάξιν
Μελχισεδέκ;
Πρὸς οὓς λέγομεν, ὅτι οὗτος ὁ Μελχισεδὲκ ἀνὴρ δίκαιος γέγονε, καὶ ἀληθῶς
εἰκόνα Θεοῦ φέρων ἐν ἑαυτῷ. Προφητικῷ τοίνυν Πνεύματι κινούμενος, τὴν
μέλλουσαν ὑπὸ Χριστοῦ προσφορὰν τῷ Θεῷ προσκομίζεσθαι νῦν νοήσας, ἄρτῳ καὶ
οἴνῳ τὸν Θεὸν ἐγέραιρε, μιμούμενος τὸν μέλλοντα Χριστόν. Ἐπειδὴ οὖν ἐν τῇ τῶν
Ἰουδαίων συναγωγῇ ἡ τάξις τοῦ Ἀαρὼν θυσίαν προσέφερεν, οὐκ ἄρτον καὶ οἶνον,
ἀλλὰ μόσχους καὶ ἀμνοὺς καὶ ἐναίμοις θυσίαις ἐδόξαζε τὸν Θεὸν, ὁ Θεὸς πρὸς τὸν
μέλλοντα γεννᾶσθαι ἐκ τῆς παρθένου Μαρίας Ἰησοῦν βοᾷ καὶ λέγει· Σὺ ἱερεὺς εἰς
τὸν αἰῶνα κατὰ τὴν τάξιν Μελχισεδὲκ, οὐ κατὰ τὴν τάξιν Ἀαρὼν, τοῦ ἐν μόσχοις καὶ
ἀρνάσι καὶ ἐναίμοις θυσίαις λατρεύοντος, ἀλλὰ, Σὺ ἱερεὺς εἰς τὸν αἰῶνα κατὰ τὴν
τάξιν Μελχισεδὲκ, ἐν ἄρτῳ καὶ οἴνῳ τὴν τῶν ἐθνῶν προσκομίζων προσφορὰν εἰς τὸ
διηνεκές. Ἀλλ' ἵνα μὴ δόξωμεν, ἀγαπητοὶ, ἐρεσχελεῖν μηκύνοντες τὸν λόγον,
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

3

ἐνταῦθα στήσαντες τὸν λόγον, τῇ τῶν πατέρων πηγῇ τῆς διδασκαλίας
παραπέμψομεν ὑμᾶς, οἵτινες καὶ τὴν δίψαν τῆς ψυχῆς ὑμῶν καταπαύσωσι, καὶ τὰς
καρδίας ὑμῶν εὐφράνωσι, Πνεύματος ἁγίου πληροῦντες, ἐν Χριστῷ Ἰησοῦ τῷ Κυρίῳ
ἡμῶν, ᾧ ἡ δόξα εἰς τοὺς αἰῶνας τῶν αἰώνων. Ἀμήν.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

4

